//
//  TabBarVC.swift
//  zigzagUser
//
//  Created by Webmobril on 02/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController ,UITabBarControllerDelegate{
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        
//        self.tabBarController?.tabBar.items?[2].badgeColor = .baseOrange
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)], for: .selected)
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
        
        
    }
    
    
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = 60
        tabFrame.origin.y = self.view.frame.size.height - 60
        self.tabBar.frame = tabFrame
    }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        if tabBarController.selectedIndex == 3{
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            
//            if UserDefaults.standard.getUserId() == ""{
//                appDelegate.gotoLoginVC()
//            }else{
//                
//            }
//            
//        }
    }
    
    
    
}


