//
//  CustomTextField.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 13/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import SkyFloatingLabelTextField

class CustomTextField: SkyFloatingLabelTextField {
    private var _titleFormatter: (String) -> String = { (text: String) -> String in
        return text
    }
    override var titleFormatter: ((String) -> String) {
        get {
            return _titleFormatter
        }
        set {
            
        }
    }
}
