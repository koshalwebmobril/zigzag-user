//
//  LanguageVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 15/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class LanguageVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "Language".localizableString(loc: UserDefaults.standard.getLanguage())
        
    }
    

    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func arabicLanguage(_ sender: Any) {
        
        UserDefaults.standard.setLanguage(lang: "ar")
        viewDidLoad()
    }
    
    @IBAction func englishAction(_ sender: Any) {
        
         UserDefaults.standard.setLanguage(lang: "en")
        viewDidLoad()
        
        
    }
    
}



