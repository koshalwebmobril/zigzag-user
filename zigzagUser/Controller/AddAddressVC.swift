//
//  AddAddressVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 04/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import IQKeyboardManager
import JHTAlertController
import CountryPickerView
import GoogleMaps

class AddAddressVC: UIViewController ,UITextFieldDelegate,CountryPickerViewDelegate, CountryPickerViewDataSource,SelectAddressDelegate{
    
    
    func updateAddress(address: String, addressFirst: String, city: String, state: String, country: String, countryCode: String, zip: String, latitude: Double, longitude: Double) {
        //        txtAddress.text = address + " " + addressFirst //+ " " + city + " " + state + " " + country + " " + zip
        txtAddress.text = address + " " + addressFirst
        self.countryName = country
        if state == ""{
            self.state = addressFirst
        }else{
            self.state = state
        }
        txtCity.text = city
        txtState.text = state
        txtCountry.text = country
        self.city = city
        self.pincode = zip
        self.address1 = address
        self.address2 = addressFirst
        latValue = latitude
        longValue = longitude
        
        
        
    }
    //
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        
        return .navigationBar
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
    }
    
    
    
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var txtCompleteAddress: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var flagImgView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var countryListView: UIView!
    var countryView : CountryPickerView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    var addressDict = JSON()
    var latValue = Double()
    var longValue = Double()
    var strAddressType :String = "1"
    var countryCode :String = "+1"
    var state = String()
    var city = String()
    var pincode = String()
    var address1 = String()
    var address2 = String()
    var countryName = String()
    var isFromExpress = false
    var isFromMap = false
    var geocoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        self.view.backgroundColor = UIColor.patternColor
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        
        
        setupUI()
        
        if addressDict.count > 0{
            
            txtAddress.text = addressDict["address"].stringValue
            
            latValue = addressDict["lattitude"].doubleValue
            longValue = addressDict["longitude"].doubleValue
            txtPhone.text = addressDict["phone"].stringValue
            txtName.text = addressDict["name"].stringValue
            txtCity.text = addressDict["city"].stringValue
            txtState.text = addressDict["state"].stringValue
            txtCountry.text = addressDict["country"].stringValue
            txtCompleteAddress.text = addressDict["line_2"].stringValue
            txtLandmark.text = addressDict["landmark"].stringValue
            pincode = addressDict["zip"].stringValue
            //            address1 = addressDict["location"].stringValue
            
            saveBtn.setTitle("Edit Address", for: .normal)
            
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }  else {
            return true
        }
    }
    
    @IBAction func locationAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        vc.delegate = self
        self.isFromMap = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func setupUI(){
        
        txtAddress.delegate = self
        txtName.delegate = self
        txtPhone.delegate = self
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius  = 5.0
        
        countryView = CountryPickerView()
        
        countryView.delegate = self
        countryView.dataSource = self
        countryView?.isHidden = true
        let country = countryView.selectedCountry
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
        countryName = country.name
        
        
        countryView.countryDetailsLabel.textColor = .white
        
    }
    
    @IBAction func dropDownAction(_ sender: Any) {
        txtPhone.resignFirstResponder()
        countryView.showCountriesList(from: self)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !isFromExpress{
            if textField == txtAddress{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
                vc.delegate = self
                
                self.navigationController?.pushViewController(vc, animated: true)
                return false
            }
        }
        return true
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func isValidTextField() -> Bool{
        
        if !(txtName.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter your name", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
        else if !(txtPhone.text?.isValidTextField())! || !(txtPhone.text?.isValidPhoneNumber())!{
            MBProgressHUD.showToast(message: "Please enter valid phone number", name: "Zag-Zag", PPView: self.view)
            return false
            
        }else if !(txtAddress.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter Address line 1", name: "Zag-Zag", PPView: self.view)
            return false
            
        }else if !(txtCompleteAddress.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter Address line 2", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
            //        else if !(txtLandmark.text?.isValidTextField())! {
            //            MBProgressHUD.showToast(message: "Please enter nearby Landmark", name: "Zag-Zag", PPView: self.view)
            //            return false
            //
            //        }
        else if !(txtCity.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter City", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
            //        else if !(txtState.text?.isValidTextField())! {
            //            MBProgressHUD.showToast(message: "Please enter State", name: "Zag-Zag", PPView: self.view)
            //            return false
            //
            //        }
        else if !(txtCountry.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter Country", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
        
        return true
    }
    
    
    
    @IBAction func saveAddressAction(_ sender: UIButton) {
        if !isFromMap{
            var address = String()
            address.append(String(format: "%@ ", txtAddress.text ?? "") )
            address.append(String(format: "%@ ", txtCompleteAddress.text ?? ""))
            address.append(String(format: "%@ ", txtCity.text ?? ""))
            address.append(String(format: "%@ ", txtCountry.text ?? ""))
//            DispatchQueue.main.sync {
                geocoder.geocodeAddressString(address) { placemarks, error in
                    let placemark = placemarks?.first
                    self.latValue = placemark?.location?.coordinate.latitude ?? 0.0
                    self.longValue = placemark?.location?.coordinate.longitude ?? 0.0
                    if self.isValidTextField(){
                        if sender.titleLabel!.text == "Edit Address"{
                            self.editAddressMethod()
                            
                        }else{
                            self.saveAddressMethod()
                        }
                    }
                }
//            }
            
        }else{
            if isValidTextField(){
                if sender.titleLabel!.text == "Edit Address"{
                    editAddressMethod()
                    
                }else{
                    saveAddressMethod()
                }
            }
        }
        
    }
    
    func saveAddressMethod(){
        
        if Reachability.isConnectedToNetwork(){
            
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.addAddressURL)"
            
            /*roles_id    name
             user_id    state
             location    city
             lattitude    zip
             longitude    phone
             address_type=1 for home, address_type=2 for office    */
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","location":"\(txtAddress.text ?? "" )","name":"\(txtName.text ?? "")","phone":"\(txtPhone.text ?? "")","lattitude":"\(latValue)","longitude":"\(longValue)","address_type":"\(strAddressType)","country":txtCountry.text ?? "","state":txtState.text ?? "","city":txtCity.text ?? "","zip":pincode,"landmark":txtLandmark.text ?? "","line_2":txtCompleteAddress.text ?? ""]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        
                        
                        let alert = self.createAlert(title: "", message: "Address has been added successfully.", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }else{
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    func editAddressMethod(){
        
        if Reachability.isConnectedToNetwork(){
            
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.editAddressURL)"
            
            /*roles_id    user_id
             address_id    name
             location    zip
             lattitude    phone
             longitude    state
             address_type    city*/
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","address_id":"\(addressDict["id"].stringValue)","user_id":"\(userId)","location":"\(txtAddress.text ?? "" )","name":"\(txtName.text ?? "")","phone":"\(txtPhone.text ?? "")","lattitude":"\(latValue)","longitude":"\(longValue)","address_type":"\(strAddressType)","country":txtCountry.text ?? "","state":txtState.text ?? "","city":txtCity.text ?? "","zip":pincode,"landmark":txtLandmark.text ?? "","line_2":txtCompleteAddress.text ?? ""]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        let alert = self.createAlert(title: "", message: "Address has been updated successfully.", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }else{
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
}
