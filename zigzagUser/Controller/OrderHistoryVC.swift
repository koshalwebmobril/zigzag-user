//
//  OrderHistoryVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 26/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController

class OrderHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blankImgView: UIImageView!
    
    var orderHistoryArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
self.view.backgroundColor = UIColor.patternColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(orderReloadAction), name: NSNotification.Name("orderHistory"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getOrderHistory()
    }
    @objc func orderReloadAction(){
        getOrderHistory()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    

    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath) as! MyOrderCell
        

        cell.bgView.layer.cornerRadius = 5.0
        cell.bgView.layer.shadowColor = UIColor.gray.cgColor
        cell.bgView.layer.shadowOpacity = 1
        cell.bgView.layer.shadowOffset = CGSize.zero
        cell.bgView.layer.shadowRadius = 2.0
        
        cell.cancelBtn.layer.masksToBounds = true
        cell.cancelBtn.layer.cornerRadius = 5.0
        cell.trackBtn.layer.masksToBounds = true
        cell.trackBtn.layer.cornerRadius = 5.0
        cell.rateBtn.layer.masksToBounds = true
        cell.rateBtn.layer.cornerRadius = 5.0
        cell.rateBtn.isHidden = false
        
        cell.logoImgView.layer.masksToBounds = true
        cell.logoImgView.layer.cornerRadius = 5
        
        let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(self.orderHistoryArray[indexPath.row]["provider"]["user_image"].stringValue)")
        
        cell.logoImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        cell.nameLabel.text = orderHistoryArray[indexPath.row]["provider"]["name"].stringValue
        
        cell.addressLabel.text = orderHistoryArray[indexPath.row]["provider"]["address"].stringValue
        
        let addStr = NSMutableString()
        
        for item in orderHistoryArray[indexPath.row]["order_detail"].arrayValue{
            
            addStr.appendFormat("%@ * %@\n", item["quantity"].stringValue,item["product_name"].stringValue)
            
        }
        cell.itemslabel.text = addStr as String
        
        cell.amountLabel.text = "$\(orderHistoryArray[indexPath.row]["final_order_amount"].stringValue)"
        cell.discountLabel.text = "$\(orderHistoryArray[indexPath.row]["discount_amount"].stringValue)"
        
        let dateStr = orderHistoryArray[indexPath.row]["created_at"].stringValue
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date : Date = dateFormatterGet.date(from: dateStr)!
        
        let orderDate = Date.getFormattedDate(date: date, format: "MMM d, yyyy, h:mm a")
        cell.dateLabel.text = "\(String(describing: orderDate))"
        
        cell.trackBtn.setTitleColor(.darkGray, for: .normal)
        
//        1=New order,2=Assign driver,3=out for delivery,4=Delivered,5=Canceled,6=Refunded
        
        if orderHistoryArray[indexPath.row]["order_status"].intValue == 1{
            cell.trackBtn.setTitle("New order", for: .normal)
        }else if orderHistoryArray[indexPath.row]["order_status"].intValue == 2{
            cell.trackBtn.setTitle("Assigned", for: .normal)
        }else if orderHistoryArray[indexPath.row]["order_status"].intValue == 3{
            cell.trackBtn.setTitle("Out for delivery", for: .normal)
        }else if orderHistoryArray[indexPath.row]["order_status"].intValue == 4{
            cell.trackBtn.setTitle("Delivered", for: .normal)
            cell.trackBtn.setTitleColor(.green, for: .normal)
        }else if orderHistoryArray[indexPath.row]["order_status"].intValue == 5{
            cell.trackBtn.setTitle("Cancelled", for: .normal)
            cell.trackBtn.setTitleColor(.red, for: .normal)
            cell.rateBtn.isHidden = true
        }else{
            cell.trackBtn.setTitle("Refunded", for: .normal)
            cell.rateBtn.isHidden = true
        }
        
        if self.orderHistoryArray[indexPath.row]["is_provider_rated"].intValue == 1{
            cell.rateBtn.setTitle("Rated", for: .normal)
        }else{
            cell.rateBtn.setTitle("Rate Now", for: .normal)
        }
        
        return cell
    }
    
    
    @IBAction func orderAgainAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
               let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
               
               
        let alert = self.createAlert(title: "", message:"Do you want to order again?", icon: "checkmark3")
        
        let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
            
            vc.restaurantId = self.orderHistoryArray[indexPath.row]["provider_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
        
    }
    
    @IBAction func ratenowAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DishRatingVC") as! DishRatingVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.providerId = self.orderHistoryArray[indexPath.row]["provider_id"].stringValue
        vc.is_provider_rated = self.orderHistoryArray[indexPath.row]["is_provider_rated"].intValue
        vc.providerRating = self.orderHistoryArray[indexPath.row]["provider_rated"].doubleValue
        vc.driverRating = self.orderHistoryArray[indexPath.row]["driver_rated"].doubleValue
        vc.driverId = self.orderHistoryArray[indexPath.row]["assign_driver_id"].stringValue

        var dataArray = [String]()
        for item in orderHistoryArray[indexPath.row]["order_detail"].arrayValue{
            dataArray.append(item["dish_id"].stringValue)
        }
        vc.dishId = dataArray.joined(separator: ",")
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    func getOrderHistory() {
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.orderHistory)"
        
        /*roles_id=2&user_id=5&card_id=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_type":"2"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.orderHistoryArray = swiftyVarJson["orders"].arrayValue
                    self.orderHistoryArray = self.orderHistoryArray.filter({ (data) -> Bool in
                        data["order_type"].intValue == 1
                    })
                    self.tableView.isHidden = false
                    self.blankImgView.isHidden = true
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
                    
//                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    

}
