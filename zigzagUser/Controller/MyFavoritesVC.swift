//
//  MyFavoritesVC.swift
//  zigzagUser
//
//  Created by Webmobril on 06/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class MyFavoritesVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titlelabel: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var blankImgView: UIImageView!
    var favoriteArrray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.patternColor
        
        titlelabel.text = "My Favorites"
        tableView.delegate = self
        tableView.dataSource = self
        
        addBtn.layer.masksToBounds = true
        addBtn.layer.cornerRadius = 5.0
        
        if #available(iOS 13.0, *) {
                      let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                      
                      statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
                      UIApplication.shared.keyWindow?.addSubview(statusBar)
                      } else {
                          
                          if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                              // my stuff
                              statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
                          }

                      }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserId() == ""{
            
//            self.customAlert(title: "", message: "Please login first to get your favorite list.", icon: "checkmark3")
            blankImgView.isHidden = false
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            vc.isFromRoot = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
             MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getFavoriteList()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.tabBarController?.popoverPresentationController
        
    }
    
    
    
    @IBAction func addFavoriteAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "AllShopList") as! AllShopList
       
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteArrray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! TableViewCell
        
        self.blankImgView.isHidden = true
        
        cell.nearbyview.layer.shadowColor = UIColor.gray.cgColor
        cell.nearbyview.layer.shadowOpacity = 0.3
        cell.nearbyview.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.nearbyview.layer.shadowRadius = 6
        cell.nearbyview.layer.cornerRadius = 10
        
        cell.nearbydistance.layer.cornerRadius = 10
        cell.nearbydistance.layer.masksToBounds = true
        
        let logoURL = URL(string:"\(GlobalURL.imagebaseURL)\(favoriteArrray[indexPath.row]["user_image"].stringValue)")
        
        cell.nearbyimg?.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "burger"),  completed: nil)
        
        cell.nearbyname.text = favoriteArrray[indexPath.row]["name"].stringValue
        cell.ratingLabel.text = favoriteArrray[indexPath.row]["address"].stringValue
        
        cell.ratingView.rating = favoriteArrray[indexPath.row]["avg_rating"].doubleValue
        cell.nearbydistance.text =  String(format: "%.2f km", favoriteArrray[indexPath.row]["distance"].floatValue)
        
        cell.nearbyprice.text = String(format: "$%.2f", favoriteArrray[indexPath.row]["startint_at"].floatValue) 
        cell.removeBtn.layer.masksToBounds = true
        cell.removeBtn.layer.cornerRadius = 5.0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        VC.restaurantId = self.favoriteArrray[indexPath.row]["provider_id"].stringValue
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    @IBAction func removeBtnAction(_ sender: UIButton) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        
        let providerId = self.favoriteArrray[indexPath.row]["provider_id"].stringValue
        
        
        let alert = self.createAlert(title: "", message: "Do you want to remove from favorite?", icon: "checkmark3")
        
        let okAction = JHTAlertAction(title: "Yes", style: .default) {
            _ in self.removeFavoriteMethod(providerId: providerId)}
        
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    func getFavoriteList(){
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getFavoriteList)"
        
        /*roles_id
        user_id
        */
        
        let userId = UserDefaults.standard.getUserId()
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
//        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.tableView.isHidden = false
                    
                    self.favoriteArrray = swiftyVarJson["providers"].arrayValue
                    
                    self.blankImgView.isHidden = true
//                    self.addBtn.isHidden = true
                    self.tableView.isHidden = false

                    self.tableView.reloadData()
                    
                    
                }else{
                    
                    self.blankImgView.isHidden = false
                    self.tableView.isHidden = true

                    
                }
                
                
                
                
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    
    func removeFavoriteMethod(providerId : String){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.removeFavorite)"
        
        /*roles_id
        user_id
        provider_id
        */
        
        let userId = UserDefaults.standard.getUserId()
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(providerId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    let alert = self.createAlert(title: "", message: "Removed from favorites.", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {
                        _ in self.getFavoriteList()}
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                    
                    
                }else{
                    
                    self.blankImgView.isHidden = false

                }
                
                
                
                
            }else{
            MBProgressHUD.hide(for: self.view, animated: true)
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
}
