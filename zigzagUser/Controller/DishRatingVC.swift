//
//  DishRatingVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 28/05/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import FloatRatingView
import Alamofire
import SwiftyJSON
import JHTAlertController

class DishRatingVC: UIViewController,FloatRatingViewDelegate {
    
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var frontView: UIView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var driverRatingView: FloatRatingView!
    var providerId = String()
    var driverId = String()
    var rating = Double()
    var dishId = String()
    var driverRating = Double()
    var is_provider_rated = Int()
    var providerRating = Double()
//    var driverRating = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitBtn.layer.masksToBounds = true
        submitBtn.layer.cornerRadius = 5
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
        colorView.addGestureRecognizer(tapgesture)
        
        if is_provider_rated == 1{
            ratingView.isUserInteractionEnabled = false
            driverRatingView.isUserInteractionEnabled = false
            submitBtn.setTitle("Rated", for: .normal)
        }else{
            ratingView.isUserInteractionEnabled = true
            driverRatingView.isUserInteractionEnabled = true
            submitBtn.setTitle("Rating", for: .normal)
        }
        
        ratingView.delegate = self
        ratingView.rating = providerRating
        
        driverRatingView.delegate = self
        driverRatingView.rating = driverRating
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        if ratingView == self.driverRatingView{
            self.driverRating = rating
        }else{
            self.rating = rating
        }
    }
    
    @objc func dismissView(_ sender:UITapGestureRecognizer){
//        NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
              self.dismiss(animated: true, completion: nil)
          }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if is_provider_rated == 1{
            self.customAlert(title: "", message: "You have already rated.", icon: "checkmark3")
            
        
        }else{
        if self.rating == 0.0{
            self.customAlert(title: "", message: "Please give minimum one rating.", icon: "checkmark3")
            
        }else{
            rateRestaurant()
        }
        }
    }
    
   
    func rateRestaurant(){
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.ratingRestaurantURL)"
        
        /*roles_id
        user_id
        provider_id(restaurant)
        rating
        */
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(providerId)","rating":"\(self.rating)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.rateDriver()
                                        
                }else{
                    
                  
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func rateDriver(){
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.rateDriver)"
        
        /*roles_id
        user_id
        provider_id(restaurant)
        rating
        */
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","driver_id":"\(driverId)","rating":"\(self.driverRating)"]
        print(params)
//        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
//                    NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)
//
//                    let alert = self.createAlert(title: "", message: "Rated successfully.", icon: "checkmark3")
//
//                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
//
//                        self.dismiss(animated: true, completion: nil)
//                    }
//
//
//                    alert.addAction(okAction)
//
//                    self.present(alert, animated: true, completion: nil)
                    
                    self.rateDish()
                    
                 
                                        
                }else{
                    
                  
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }
    
    func rateDish(){
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.rateDishURL)"

            /*roles_id
            user_id
            provider_id(restaurant)
            rating
            */

            let userId = UserDefaults.standard.getUserId()

            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(dishId)","rating":"\(self.driverRating)"]
            print(params)
    //        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)

            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)

                if  response.result.value != nil{

                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{

                        NotificationCenter.default.post(name: NSNotification.Name("orderHistory"), object: nil)

                        let alert = self.createAlert(title: "", message: "Rated successfully.", icon: "checkmark3")

                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in

                            self.dismiss(animated: true, completion: nil)
                        }


                        alert.addAction(okAction)

                        self.present(alert, animated: true, completion: nil)



                    }else{



                    }




                }else{
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)

                }

            }
        }
    
}
