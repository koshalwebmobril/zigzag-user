//
//  SelectAddressVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 29/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

protocol SelectAddressDelegate {
    func updateAddress(address: String, addressFirst: String, city: String, state: String, country: String, countryCode: String, zip: String, latitude: Double, longitude: Double)
}

class SelectAddressVC: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,SearchViewControllerDelegate  {
    func updatedLocationAddress(address: String, addressFirst: String, city: String, state: String, country: String, countryCode: String, zip: String, latitude: Double, longitude: Double) {
        self.getAddress = address
        self.addressLabel.text = address
        self.sourceLocation.latitude = latitude
        self.sourceLocation.longitude = longitude
        self.delegate.updateAddress(address: address, addressFirst: addressFirst, city: city, state:state, country: country, countryCode: countryCode, zip: zip, latitude: latitude, longitude: longitude)
        setupMapView()
    }
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    
//    @IBOutlet weak var addressTxt: UITextView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var currentLocBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var mapAddress: UILabel!
    
    @IBOutlet weak var selectBtn: UIButton!
    
    var newLocationAddress = String()
    
    var locationManager = CLLocationManager()
    var sourceLocation = CLLocationCoordinate2D()
    
    var geocoder = CLGeocoder()
    
    
    var getAddress = String()
    var delegate : SelectAddressDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
//        mapView.delegate = self
        
        topView.layer.shadowColor = UIColor.gray.cgColor
        topView.layer.shadowOpacity = 0.3
        topView.layer.shadowOffset = CGSize(width: 2, height: 2)
        topView.layer.shadowRadius = 6
        topView.layer.cornerRadius = 10
        
        addressView.layer.shadowColor = UIColor.gray.cgColor
        addressView.layer.shadowOpacity = 0.3
        addressView.layer.shadowOffset = CGSize(width: 2, height: 2)
        addressView.layer.shadowRadius = 6
        addressView.layer.cornerRadius = 10
        
        selectBtn.layer.masksToBounds = true
        selectBtn.layer.cornerRadius = 5.0
    }
    
    func setupMapView(){
        mapView.camera = GMSCameraPosition(latitude: sourceLocation.latitude, longitude: sourceLocation.longitude, zoom: 18.0)
        addressView.isHidden = false
        mapView.delegate = self
        mapView.clear()
        
    }
    
    
    func getAddressFromLatLong(latitude: Double, longitude : Double) {
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&key=AIzaSyDJl11mAojAFGlCjDrzrZx1LhXozqtA78Y"
        let params:[String: Any] = [:]
        
        Alamofire.request(url, method: .post, parameters: params , encoding: JSONEncoding.default)
            .responseJSON { response in
                
                
               
                switch response.result {
                case .success(let value):
                    
                    var addressVal = String()
                    var pincodeVal = String()
                    var cityVal = String()
                    var stateVal = String()
                    var countryVal = String()
                    var localtyVal = String()
                    var sublocalityVal = String()

                    
                    let responseJson = value as? [String: Any]
                    
                    if let results = responseJson?["results"]! as? [NSDictionary] {
                        if results.count > 0 {
                            if let addressComponents = results[0]["address_components"]! as? [NSDictionary] {
                                self.newLocationAddress = results[0]["formatted_address"] as? String ?? ""// Full Address
                                for component in addressComponents {
                                    if let temp = component.object(forKey: "types") as? [String] {
                                        if (temp[0] == "postal_code") {
                                            pincodeVal = component["long_name"] as? String ?? ""
                                        }
                                        if (temp[0] == "locality") {
                                            cityVal = component["long_name"] as? String ?? ""
                                        }
                                        if (temp[0] == "administrative_area_level_1") {
                                            stateVal = component["long_name"] as? String ?? ""
                                        }
                                        if (temp[0] == "country") {
                                            countryVal = component["long_name"] as? String ?? ""
                                        }
                                        
                                        for item in temp{
                                        if item == "sublocality_level_2" {
                                            sublocalityVal = component["long_name"] as? String ?? ""
                                        }else if item == "street_number"{
                                        localtyVal = component["long_name"] as? String ?? ""
                                        }
                                        }
                                        for item in temp{
                                        if item == "sublocality_level_1" {
                                            localtyVal = component["long_name"] as? String ?? ""
                                        }else if item == "route"{
                                            localtyVal = component["long_name"] as? String ?? ""
                                            }
                                        }
                                        
                                        
                                    }
                                    
                                    self.getAddress = String(format:"%@ ",self.newLocationAddress)
                                    
                                    self.delegate?.updateAddress(address: sublocalityVal, addressFirst: localtyVal, city: cityVal , state: stateVal , country: countryVal , countryCode: pincodeVal, zip: pincodeVal , latitude: latitude, longitude: longitude)
                                    
                                    self.mapAddress.text = self.getAddress
                                    self.addressLabel.text = self.getAddress
                                    self.setupMapView()
                                    
                                }
                            }
                        }
                        
                        
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
        
        sourceLocation = CLLocationCoordinate2DMake(cameraPosition.target.latitude, cameraPosition.target.longitude)
        
        self.getAddressFromLatLong(latitude: sourceLocation.latitude, longitude:  sourceLocation.longitude)

        
    }
    
    
    
    
    
    
    @IBAction func selectAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLoc = locations.last
        sourceLocation = CLLocationCoordinate2DMake((currentLoc?.coordinate.latitude)!, (currentLoc?.coordinate.longitude)!)
        
        self.getAddressFromLatLong(latitude: sourceLocation.latitude, longitude:  sourceLocation.longitude)

        locationManager.stopUpdatingLocation()
        
        
        
    }
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        vc.delegate = self
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func getCurrentLocationAction(_ sender: Any) {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
    }
}
