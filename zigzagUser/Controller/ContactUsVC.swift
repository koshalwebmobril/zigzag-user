//
//  ContactUsVC.swift
//  zigzagUser
//
//  Created by Webmobril on 09/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class ContactUsVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var titlemsg: UITextField!
    @IBOutlet weak var msg: UITextView!
    @IBOutlet weak var contactview: UIView!
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var msgView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        msg.delegate = self
        contactview.layer.cornerRadius = 10
        
        msgView.layer.masksToBounds = true
        msgView.layer.cornerRadius = 10
        send.layer.cornerRadius = 10
        msg.text = "Message"
        msg.textColor = UIColor.lightGray
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: titlemsg.frame.height))
        titlemsg.leftView = paddingView
        titlemsg.leftViewMode = .always
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func send(_ sender: Any) {
        
        contactMethod()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if msg.textColor == UIColor.lightGray {
            msg.text = nil
            msg.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if msg.text.isEmpty {
            msg.text = "Message"
            msg.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func isValidTextField()->Bool{
        
        if !(titlemsg.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter title", name: "Zig-Zag", PPView: self.view)
            return false
        }else if !(msg.text?.isValidTextField())! || msg.text == "Message"{
            MBProgressHUD.showToast(message: "Please enter your message", name: "Zig-Zag", PPView: self.view)
            return false
        }
        return true
    }
    
    func contactMethod(){
        
         if Reachability.isConnectedToNetwork(){
        
        if isValidTextField(){
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.contactAdminURL)"
        
        /*roles_id
        subject
        message*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","subject":"\(titlemsg.text ?? "")","message":"\(msg.text ?? "")"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                       
                        self.navigationController?.popViewController(animated: true)
                    }

                    
                    alert.addAction(okAction)

                    self.present(alert, animated: true, completion: nil)
                    
                   
                                        
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }

}
