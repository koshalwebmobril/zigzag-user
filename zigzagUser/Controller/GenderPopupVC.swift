//
//  GenderPopupVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 29/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

protocol GenderDelegate {
    func setGender(type:String,id:String)
}

class GenderPopupVC: UIViewController {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var frontView: UIView!
    
    var delegate : GenderDelegate!
    

    override func viewDidLoad() {
        super.viewDidLoad()

    let tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
           colorView.addGestureRecognizer(tapgesture)
           
           
       }
       
      
       
       @objc func dismissView(_ sender:UITapGestureRecognizer){
           self.dismiss(animated: true, completion: nil)
       }
    @IBAction func maleAction(_ sender: Any) {
        delegate.setGender(type: "Male",id:"1")
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func femaleAction(_ sender: Any) {
        delegate.setGender(type: "Female",id:"2")
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
