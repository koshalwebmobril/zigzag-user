//
//  CustomizationVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 30/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

class CustomizationVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countView2: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var discount_label: UILabel!
    
    var productId = String()
    var restaurantId = String()
    var providerName = String()
    var finalAmount = Float()
    
    var menuHeaderArray = [JSON]()
    var menuOptionArray = [JSON]()
    var productDetails = JSON()
    var customizationId = String()
    var subtypesId = String()
    var discountValue = 0.0
    var subMenuArray = [[String:[Int]]]()
    //    var selectedIndex = IndexPath()
    
    var radioSelectedIndex = [IndexPath]()
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBtn.layer.masksToBounds = true
        addBtn.layer.cornerRadius = 5.0
        
        countView2.layer.masksToBounds = true
        countView2.layer.cornerRadius = 5.0
        
        
        if discountValue != 0.0 {
            discount_label.text = "(\(discountValue)% OFF)"
        }
        
        
        getProductDetails()
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if menuHeaderArray.count > 0{
            return menuHeaderArray.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return menuHeaderArray[section-1]["all_sub_types"].arrayValue.count
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var superCell = UITableViewCell()
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! OrderDetailCell
            
            
            cell.productImView.layer.masksToBounds = true
            cell.productImView.layer.cornerRadius = 5.0
            let imgURL:URL = URL(string: "\(GlobalURL.imagebaseURL)\(productDetails["img"].stringValue)")!
            
            cell.productImView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
            
            cell.productName.text = productDetails["name"].stringValue
            let desc = productDetails["description"].stringValue
            cell.productDesc.text = desc.htmlToString
            
            superCell = cell
        }else{
            
            if menuHeaderArray[indexPath.section-1]["customisation_type"].intValue == 1{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "checkList", for: indexPath) as! OrderDetailCell
                cell.checkMenuLabel.text = menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["type"].stringValue
                cell.checkPriceLabel.text = "$\( menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["prices"].stringValue)"
                //                if selectedCheckIndex.count > 0{
                //                    if indexPath.row == selectedCheckIndex[indexPath.section].row{
                //                    cell.checkBtn.isHidden = false
                //                    cell.uncheckBtn.isHidden = true
                //                }else{
                //                    cell.checkBtn.isHidden = true
                //                    cell.uncheckBtn.isHidden = false
                //                }
                //                }
                
                superCell = cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "list", for: indexPath) as! OrderDetailCell
                cell.itemName.text = menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["type"].stringValue
                cell.radioMenuPrice.text = "$\( menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["prices"].stringValue)"
                
                for (_,item) in radioSelectedIndex.enumerated(){
                    if indexPath.section == item.section{
                        if indexPath.row == item.row{
                            cell.radioBtn.isSelected = true
                        }else{
                            cell.radioBtn.isSelected = false
                        }
                    }
                }
                
                
                
                superCell = cell
            }
        }
        return superCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 50.0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section > 0 {
            return menuHeaderArray[section-1]["name"].stringValue
        }
        return ""
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        let cell = self.tableView.cellForRow(at: indexPath) as! OrderDetailCell
        sender.isSelected = !sender.isSelected
        
        let keyData = subMenuArray.flatMap({$0.filter({$0.key == menuHeaderArray[indexPath.section-1]["id"].stringValue})})
        var addData = keyData[0].value
        let getValue = [menuHeaderArray[indexPath.section-1]["id"].stringValue : addData]
        let position = self.subMenuArray.index(of: getValue)
        
        let fullName = self.priceLabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        var getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        
        
        
        if sender.isSelected{
            cell.checkBtn.setImage(UIImage(named: "check_select"), for: .normal)
            let addValue = menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["prices"].floatValue
            let count: Int = Int(self.countLabel.text ?? "") ?? 0
            getPrevAmount = getPrevAmount + (addValue * Float(count))
            
            addData.append(menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["id"].intValue)
            
            
            
        }else{
            cell.checkBtn.setImage(UIImage(named: "check_blank"), for: .normal)
            let addValue = menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["prices"].floatValue
            let count: Int = Int(self.countLabel.text ?? "") ?? 0
            getPrevAmount = getPrevAmount - (addValue * Float(count))
            
            
            for (index,item) in addData.enumerated() {
                if item == menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["id"].intValue {
                    addData.remove(at: index)
                }
            }
            
            
            
        }
        
        
        subMenuArray[position!] = ([menuHeaderArray[indexPath.section-1]["id"].stringValue : addData])
        
        self.priceLabel.text =  String(format: "$%.2f", getPrevAmount)
        
        
        print(subMenuArray)
        
        
    }
    
    
    
    
    
    @IBAction func radioAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        
        sender.isSelected = !sender.isSelected
        
        let keyData = subMenuArray.flatMap({$0.filter({$0.key == menuHeaderArray[indexPath.section-1]["id"].stringValue})})
        var addData = keyData[0].value
        let getValue = [menuHeaderArray[indexPath.section-1]["id"].stringValue : addData]
        let position = self.subMenuArray.index(of: getValue)
        
        let fullName = self.priceLabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        var getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        let count: Int = Int(self.countLabel.text ?? "") ?? 0
        
        for (point,data) in radioSelectedIndex.enumerated(){
            if data.section == indexPath.section{
                let addValue = menuHeaderArray[data.section-1]["all_sub_types"][data.row]["prices"].floatValue
                
                getPrevAmount = (getPrevAmount - (addValue * Float(count)))
                
                radioSelectedIndex.remove(at: point)
                
                if radioSelectedIndex.count == 0{
                    getPrevAmount = getPrevAmount +  self.productDetails["price"].floatValue * Float(count)
                    
                }
                
            }
        }
        
        if sender.isSelected{
            
            let addValue = menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["prices"].floatValue
            
            addData = [menuHeaderArray[indexPath.section-1]["all_sub_types"][indexPath.row]["id"].intValue]
            
            if radioSelectedIndex.count == 0{
                getPrevAmount = (getPrevAmount + (addValue * Float(count))) - self.productDetails["price"].floatValue * Float(count)
                
            }else{
                getPrevAmount = (getPrevAmount + (addValue * Float(count)))
            }
            
            radioSelectedIndex.append(indexPath)
            
        }
        
        
        subMenuArray[position!] = ([menuHeaderArray[indexPath.section-1]["id"].stringValue : addData])
        
        
        
        self.priceLabel.text = String(format: "$%.2f", getPrevAmount)
        
        self.tableView.reloadData()
        print(subMenuArray)
        
        print("count:\(radioSelectedIndex.count)")
        
        
    }
    
    
    @IBAction func minusAction(_ sender: Any) {
        
        var count: Int = Int(self.countLabel.text ?? "") ?? 0
        
        let fullName = self.priceLabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        let getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        
        let finalAmount = getPrevAmount / Float(count)
        
        
        
        if count > 1{
            count = count - 1
        }
        
        
        self.countLabel.text = "\(count)"
        
        
        
        self.finalAmount = finalAmount*Float(count)
        
        self.priceLabel.text  = String(format: "$%.2f", self.finalAmount)
        
        
        
        
        
    }
    
    @IBAction func plusAction(_ sender: Any) {
        
        var count: Int = Int(self.countLabel.text ?? "") ?? 0
        
        if count <= 99{
            
            let fullName = self.priceLabel.text
            let fullStr = fullName!.split{$0 == "$"}.map(String.init)
            
            let getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
            
            let baseAmount = getPrevAmount/Float(count)
            
            count = count  + 1
            
            self.countLabel.text = "\(count)"
            
            
            
            
            self.finalAmount = baseAmount * Float(count)
            
            self.priceLabel.text = String(format: "$%.2f", self.finalAmount)
            
            
        }else{
            self.customAlert(title: "", message: "You can not order more than 99 Quantity", icon: "checkmark3")
        }
    }
    
    @IBAction func addToCart(_ sender: Any) {
        if UserDefaults.standard.getUserId() == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
            var strSet = [String]()
            var subStrArray = [String]()
            
            for (index,value) in subMenuArray.enumerated(){
                print(value)
                print(index)
                var subStr = String()
                for (_,data) in value.enumerated(){
                    
                    print(data)
                    if data.value.count > 0 {
                        subStr = (data.value.map{String($0)}).joined(separator: "@")
                        strSet.append(data.key)
                        subStrArray.append(subStr)
                    }
                    
                }
                
                
                
            }
            if radioSelectedIndex.count == 0{
                subStrArray.removeAll()
            }
            
            let customizationStr = strSet.map({$0}).joined(separator: ",")
            
            let submenStr = subStrArray.map({$0}).joined(separator: ",")
            
            
            addToCartApi(customization: customizationStr, subType: submenStr)
            
        }
        
    }
    
    
    
    func getProductDetails(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.dishDetailsURL)"
            
            /*roles_id
             dish_id
             provider_id*/
            
            let params :[String:Any] = ["roles_id":"2","provider_id":"\(self.restaurantId)","dish_id":"\(self.productId)"]
            //        let params :[String:Any] = ["roles_id":"2","provider_id":"62","dish_id":"4"]
            print(baseURL)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        //dish
                        
                        self.productDetails = swiftyVarJson["dish"]
                        self.menuHeaderArray = swiftyVarJson["customization"].arrayValue
                        
                        for value in self.menuHeaderArray{
                            //                        print("This is val \(value)")
                            let newValue: [String: [Int]] = [value["id"].stringValue : []]
                            self.subMenuArray.append(newValue)
                        }
                        //                        self.menuHeaderArray.forEach { (data) in
                        //                            if data["customisation_type"].intValue == 2{
                        //                                self.menuOptionArray = data["all_sub_types"].arrayValue
                        //                            }
                        //                        }
                        
                        self.priceLabel.text = String(format: "$%.2f", self.productDetails["price"].floatValue)
                        //                        self.finalAmount = self.menuOptionArray[0]["prices"].floatValue
                        //                        self.customizationId = self.menuHeaderArray[0]["id"].stringValue
                        //                        self.subtypesId = self.menuOptionArray[0]["id"].stringValue
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                        //                       self.updateBottomView()
                        
                        
                    }else{
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    func addToCartApi(customization: String, subType: String)  {
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.addToCart)"
            
            /*roles_id
             user_id
             dish_id
             quantity
             amount
             provider_id customisation=1,2,3&sub_types=2@3,4@6@7,9@12    */
            
            var strCustomization = String()
            
            if customization.count > 0{
                strCustomization = "1"
            }else{
                strCustomization = "0"
            }
            let userId = UserDefaults.standard.getUserId()
            
            
            let totalStr = self.priceLabel.text!.split{$0 == "$"}.map(String.init)
            let totalAmount: Float = Float(totalStr[0] ) ?? 0.0
            let discountAmount = String(format: "%.2f", (totalAmount*Float(discountValue))/100) 
            if subType.isEmpty{
                self.customAlert(title: "", message: "Please select your customization type.", icon: "checkmark3")
                return
            }
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","amount":"\(totalAmount)","quantity":countLabel.text!,"provider_id":"\(self.restaurantId)","dish_id":"\(self.productId)","is_customizable":"\(strCustomization)","sub_types":"\(subType)","customisations":"\(customization)","discount_price":"\(discountAmount)"]
            
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        NotificationCenter.default.post(name: NSNotification.Name("updateCartCount"), object: nil)
                        //                         self.delegate?.updateCell(isAdded: true, amount: swiftyVarJson["data"]["amount"].floatValue)
                        
                        UserDefaults.standard.setProviderId(providerId: Int(self.restaurantId)!)
                        
                        UserDefaults.standard.setProviderName(name: self.providerName)
                        
                        
                        let alert = self.createAlert(title: "", message: "Added to shopping cart.", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            self.dismiss(animated: true, completion: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                        }
                        
                        
                        
                    }else{
                        //                         self.delegate?.updateCell(isAdded: false, amount: 0.0)
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer.invalidate()
        //        self.timer. = nil
    }
    
    
}
