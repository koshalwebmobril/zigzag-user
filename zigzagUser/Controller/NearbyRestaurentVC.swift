//
//  NearbyRestaurentVC.swift
//  zigzagUser
//
//  Created by Webmobril on 29/01/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreLocation
import JHTAlertController

class NearbyRestaurentVC: UIViewController ,CLLocationManagerDelegate{

    @IBOutlet weak var nearbyTableView: UITableView!
    
    var restaurantArray = [JSON]()
    
    var locManager = CLLocationManager()
    var latitudeVal = Double()
    var longitudeVal = Double()
    var isApiRunning : Bool = false
    var contactNumber = String()
    
    @IBOutlet weak var blankImgView: UIImageView!
    
    var indexValue = UInt()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor

        nearbyTableView.delegate = self
        nearbyTableView.dataSource = self
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        isApiRunning = false
         
        locationManagerMethod()
    }
    fileprivate func locationManagerMethod() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.distanceFilter = 100
        locManager.startUpdatingLocation()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways){
            
            
            print("location authorized......")
            
        }else{
            
            let alert = UIAlertController(title: "Allow Location Access", message: "Zig-Zag needs access to your location to show the order List. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
            
            
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
        
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print(error)
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            print(locations)
            
    //        let location = locations.last!

               guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

               latitudeVal = locValue.latitude
               longitudeVal = locValue.longitude
            self.getRestaurantList(restaurantType: indexValue + 1)
        }
        
    
    
   
    @IBAction func callAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: nearbyTableView)
        let indexPath = nearbyTableView.indexPathForRow(at: position)!
        
        contactNumber = restaurantArray[indexPath.row]["mobile"].stringValue
        
        if contactNumber.isEmpty{
            
            self.customAlert(title: "", message: "Phone number not available", icon: "checkmark3")
            
            
            
            
        }else{
            let tempCall = String(format:"%@",contactNumber)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
    }
    

}

extension NearbyRestaurentVC:UITableViewDelegate,UITableViewDataSource{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nearbyCell") as! TableViewCell
        
        blankImgView.isHidden = true
        
        cell.nearbyview.layer.shadowColor = UIColor.gray.cgColor
        cell.nearbyview.layer.shadowOpacity = 0.3
        cell.nearbyview.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.nearbyview.layer.shadowRadius = 6
        cell.nearbyview.layer.cornerRadius = 10
        
//        cell.nearbydistance.layer.cornerRadius = 10
//        cell.nearbydistance.layer.masksToBounds = true
        
        let logoURL = URL(string:"\(GlobalURL.imagebaseURL)\(restaurantArray[indexPath.row]["user_image"].stringValue)")
        
        cell.nearbyimg?.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "burger"),  completed: nil)
        
        cell.nearbyname.text = restaurantArray[indexPath.row]["name"].stringValue
        cell.address_label.text = restaurantArray[indexPath.row]["address"].stringValue
        cell.desc_label.text = restaurantArray[indexPath.row]["description"].stringValue
//        cell.ratingLabel.text = String(format: "%.1f", restaurantArray[indexPath.row]["avg_rating"].floatValue)
        
//        cell.ratingView.rating = restaurantArray[indexPath.row]["avg_rating"].doubleValue
        cell.nearbydistance.text =  restaurantArray[indexPath.row]["distance"].stringValue
//        if restaurantArray[indexPath.row]["startint_at"].stringValue == ""{
//
//            cell.nearbyprice.text = "$0.00"
//
//        }else{
//        cell.nearbyprice.text = "$\(restaurantArray[indexPath.row]["startint_at"].stringValue)"
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 160
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        VC.restaurantId = self.restaurantArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    
   
}





extension NearbyRestaurentVC{
    
    
    func getRestaurantList(restaurantType : UInt){
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getRestaurantListURL)"
            
            /*roles_id
            restaurant_type,lattitude, longitude*/
   
        let params :[String:Any] = ["roles_id":"2","restaurant_type":"\(restaurantType)","lattitude":"\(latitudeVal)","longitude":"\(longitudeVal)"]
            print(params)
        
        if !isApiRunning{
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            isApiRunning = true
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        
                        self.restaurantArray = swiftyVarJson["restaurants"].arrayValue
                        
                        if self.restaurantArray.count > 0{
                        
                        DispatchQueue.main.async {
                            self.nearbyTableView.reloadData()
                           
                        }
                        }else{
                            self.blankImgView.isHidden = false
                        }
                        
                       
                        
                    }else{
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                        
                    }
                    
                    
                    
                    
                }else{
                  MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                }
                
            }
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
