//
//  AddressListVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 04/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

protocol setExpressAddressDelegate {
    func setExpressAddress(address: String,lattitude: Double,longitude:Double,name:String,mobile:String,addressline2:String,landmark:String,city:String,state:String,country:String)
}

class AddressListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var isFromExpress = false
    @IBOutlet weak var tableView: UITableView!
    var delegate:setExpressAddressDelegate?
    
    var addressArray = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor

        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .patternColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserDefaults.standard.getUserId() == ""{
            
            self.customAlert(title: "", message: "You are not registered user. Please register", icon: "checkmark3")
            
            
        }else{
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getAllAddress()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
        
        cell.bgView.layer.shadowColor = UIColor.gray.cgColor
        cell.bgView.layer.shadowOpacity = 0.3
        cell.bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.bgView.layer.shadowRadius = 6
        cell.bgView.layer.cornerRadius = 5.0
        
        cell.pointBtn.layer.masksToBounds = true
        cell.pointBtn.layer.cornerRadius = cell.pointBtn.frame.height/2
        
        cell.editBtn.layer.masksToBounds = true
        cell.editBtn.layer.cornerRadius = 5.0
        
        cell.defaultBtn.layer.masksToBounds = true
        cell.defaultBtn.layer.cornerRadius = 5.0
        
        cell.lblAddressNumber.text = "Address \(indexPath.row+1)"
        
        if self.isFromExpress{
            cell.defaultBtn.isUserInteractionEnabled = true
            cell.defaultBtn.setTitle("Select", for: .normal)
        }else{
        
        if addressArray[indexPath.row]["primary_status"].intValue == 1{
            cell.pointBtn.backgroundColor = .green
            cell.defaultBtn.isUserInteractionEnabled = true
            cell.defaultBtn.setTitle("Default", for: .normal)
        }else{
            
            cell.pointBtn.backgroundColor = .lightGray
            cell.defaultBtn.isUserInteractionEnabled = true
            cell.defaultBtn.setTitle("Make Default", for: .normal)
            
        }
        }
       
        cell.nameLabel.text = (addressArray[indexPath.row]["name"].stringValue)
        cell.lbladdress.text =  addressArray[indexPath.row]["address"].stringValue + " \(addressArray[indexPath.row]["line_2"].stringValue)" + " \(addressArray[indexPath.row]["city"].stringValue)" + " \(addressArray[indexPath.row]["country"].stringValue)" + " \(addressArray[indexPath.row]["zip"].stringValue)"
//            + "," + addressArray[indexPath.row]["city"].stringValue + "," + addressArray[indexPath.row]["state"].stringValue + "," + addressArray[indexPath.row]["country"].stringValue + "," + addressArray[indexPath.row]["zip"].stringValue
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 70))
        footerView.backgroundColor = .patternColor
        
        let view = UIView(frame: CGRect(x: 10, y: 5, width: footerView.frame.width-20, height: 60))
        view.backgroundColor = .white
        
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 2, height: 2)
        view.layer.shadowRadius = 6
        view.layer.cornerRadius = 5.0
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addAddressAction(_ :)))
        view.addGestureRecognizer(gesture)
        
        let button = UIButton(frame: CGRect(x: 20, y: 22.5, width: 15, height: 15))
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = button.frame.height/2
        button.layer.masksToBounds = true
        view.addSubview(button)
        
        let label = UILabel(frame: CGRect(x: 60, y: 20, width: view.frame.width - 80, height: 20))
        
        label.text = "Add New Address"
        label.textColor = UIColor.black
        view.addSubview(label)
        
        footerView.addSubview(view)
        
        return footerView
        
    }
    
    @objc func addAddressAction(_ sender: UITapGestureRecognizer){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        vc.isFromExpress = self.isFromExpress
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func editBtnAction(_ sender: Any) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        vc.addressDict = self.addressArray[indexPath.row]
        vc.isFromExpress = self.isFromExpress
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    

    @IBAction func defaultBtnAction(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        if sender.currentTitle ?? "" == "Select"{
            delegate?.setExpressAddress(address: addressArray[indexPath.row]["address"].stringValue, lattitude: addressArray[indexPath.row]["lattitude"].doubleValue, longitude: addressArray[indexPath.row]["longitude"].doubleValue, name: addressArray[indexPath.row]["name"].stringValue, mobile: addressArray[indexPath.row]["phone"].stringValue, addressline2: addressArray[indexPath.row]["line_2"].stringValue, landmark: addressArray[indexPath.row]["line_2"].stringValue, city: addressArray[indexPath.row]["city"].stringValue, state: addressArray[indexPath.row]["state"].stringValue, country: addressArray[indexPath.row]["country"].stringValue)
            self.navigationController?.popViewController(animated: true)
        }else{
        let addressId = self.addressArray[indexPath.row]["id"].stringValue
        if  addressArray[indexPath.row]["primary_status"].intValue == 1{
            self.customAlert(title: "", message: "This address is already default.", icon: "checkmark3")
        }else{
            self.makeDefaultAddress(addressId: addressId)
        }
        }
        
        
    }
    
    
    
    @IBAction func deleteAddressAction(_ sender: Any) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        
        let addressId = self.addressArray[indexPath.row]["id"].stringValue
        
        let alert = self.createAlert(title: "", message: "Do you want to delete this address?", icon: "checkmark3")
        
        let okAction = JHTAlertAction(title: "Yes", style: .default) {
            _ in self.deleteAddress(addressId: addressId)}
        
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAllAddress()  {
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getAllAddressURL)"
        
        /*roles_id
        user_id*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
        
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    self.addressArray = swiftyVarJson["address"].arrayValue

                    
                    self.tableView.reloadData()
                                        
                }else{
                    
                    self.tableView.reloadData()

                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func makeDefaultAddress(addressId :String){
        
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.makeDefualtAddressURL)"
        
        /*roles_id
        address_id
        user_id*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","address_id":"\(addressId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
//            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    let alert = self.createAlert(title: "", message: "Default address updated successfully.", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                       
                        self.getAllAddress()
                    }

                    
                    alert.addAction(okAction)

                    self.present(alert, animated: true, completion: nil)
                    
                   
                    
                                        
                }else{
                     MBProgressHUD.hide(for: self.view, animated: true)
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                   
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func deleteAddress(addressId : String)   {
        
        if Reachability.isConnectedToNetwork(){
            
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.deleteAddressURL)"
            
            /*roles_id
            address_id
            user_id*/
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","address_id":"\(addressId)"]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
    //            MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        let alert = self.createAlert(title: "", message: "Address deleted successfully.", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                           
                            self.getAllAddress()
                        }

                        
                        alert.addAction(okAction)

                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                                            
                    }else{
                        
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        }
    
    
    
    
}
