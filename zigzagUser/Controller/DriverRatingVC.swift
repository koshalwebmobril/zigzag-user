//
//  DriverRatingVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 28/05/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import FloatRatingView
import Alamofire
import SwiftyJSON
import JHTAlertController

class DriverRatingVC: UIViewController,FloatRatingViewDelegate {
    
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var submitBtn: UIButton!
    
    var popupDict = [String:Any]()
    
    var driverId = String()
    
    var rating = Double()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
        colorView.addGestureRecognizer(tapgesture)
        
        ratingView.delegate = self
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        self.rating = rating
    }
    
    @objc func dismissView(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if self.rating == 0.0{
            self.customAlert(title: "", message: "Please give minimum one rating.", icon: "checkmark3")
            
        }else{
            rateDriver()
        }
        
    }
    
    
    func rateDriver(){
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.rateDriver)"
        
     
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","driver_id":"\(driverId)","rating":"\(self.rating)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    let alert = self.createAlert(title: "", message:"Rated successfully.", icon: "checkmark3")
                     
                     let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                         self.dismiss(animated: true, completion: nil)
                     }
                    alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
                     alert.addAction(okAction)
                   
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
