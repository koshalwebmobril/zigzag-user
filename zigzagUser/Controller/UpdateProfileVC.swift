//
//  UpdateProfileVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 04/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

class UpdateProfileVC: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,GenderDelegate {
    func setGender(type: String, id: String) {
        self.txtGender.text = type
        self.genderValue = id
    }
    
    
    
    @IBOutlet weak var maleBtn: UIButton!
    
    @IBOutlet weak var femaleBtn: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var datePicker = UIDatePicker()
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtGender: SkyFloatingLabelTextField!
    @IBOutlet weak var txtDob: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtname: SkyFloatingLabelTextField!
    var imagePicker = UIImagePickerController()
    var isuploadImage : Bool = false
    
    var isFromEdit : Bool = false
    var userId = String()
    
    var genderValue = "0"
    
    var updateData = JSON()
    var isFromRoot = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        if isFromEdit {
            isuploadImage = true
        }else{
            isuploadImage = false
        }
        
        imagePicker.delegate = self
        
        setupUI()
    }
    
    func setupUI(){
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor  = UIColor.baseOrange.cgColor
        
        btnSubmit.layer.masksToBounds = true
        btnSubmit.layer.cornerRadius = 5.0
        
        txtname.delegate = self
        txtEmail.delegate = self
        txtDob.delegate = self
        txtGender.delegate = self
        
        maleBtn.layer.masksToBounds = true
        maleBtn.layer.cornerRadius = 5.0
        maleBtn.layer.borderColor = UIColor.lightGray.cgColor
        maleBtn.layer.borderWidth = 2.0
        
        femaleBtn.layer.masksToBounds = true
        femaleBtn.layer.cornerRadius = 5.0
        femaleBtn.layer.borderColor = UIColor.lightGray.cgColor
        femaleBtn.layer.borderWidth = 2.0
        
        
        showDatePicker()
        
        if updateData.count > 0{
            
            let imgURL : URL = URL(string: "\(GlobalURL.imagebaseURL)\(updateData["user_image"].stringValue)")!
            
            imageView.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
            
            imageView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "profile-3"), completed: nil)
            
            txtname.text = "\(updateData["name"].stringValue)"
            txtEmail.text = "\(updateData["email"].stringValue)"
            txtDob.text = "\(updateData["dob"].stringValue)"
            
            if updateData["gender"].stringValue == "1"{
                maleBtn.layer.borderColor = UIColor.baseOrange.cgColor
                
                self.genderValue = "1"
                
            }else if updateData["gender"].stringValue == "2"{
                femaleBtn.layer.borderColor = UIColor.baseOrange.cgColor
                
                self.genderValue = "2"
            }
            
            
        }
        
    }
    
    func showDatePicker(){
        //Formate Date
        //        datePicker.minimumDate =
        
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date
        
        
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDob.inputAccessoryView = toolbar
        txtDob.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd"
        txtDob.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        txtDob.text = ""
        self.view.endEditing(true)
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnAction(_ sender: Any) {
        
        editProfileMethod()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtGender{
            genderPopup()
            return false
            
        }
        return true
    }
    
    private func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func maleAction(_ sender: UIButton) {
        maleBtn.layer.borderColor = UIColor.baseOrange.cgColor
        femaleBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        self.genderValue = "1"
        
    }
    
    
    @IBAction func femaleAction(_ sender: UIButton) {
        
        femaleBtn.layer.borderColor = UIColor.baseOrange.cgColor
        maleBtn.layer.borderColor = UIColor.lightGray.cgColor
        
        self.genderValue = "2"
    }
    
    
    func genderPopup()  {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GenderPopupVC") as! GenderPopupVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
        
        
        
    }
    
    
    @IBAction func editImageAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.customAlert(title: "", message: "You don't have camera", icon: "checkmark3")
            
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imageView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        
        self.isuploadImage = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    func isValidParameters() -> Bool {
        if !(txtname.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter name", name: "Zig-Zag", PPView: self.view)
            
            return false
            
        }else if  (txtEmail.text?.isValidTextField())! && !(txtEmail.text?.isValidEmail())!{
           
            MBProgressHUD.showToast(message: "Please enter valid email Id", name: "Zig-Zag", PPView: self.view)
            
            return false
           
        }
        else if self.genderValue == "0"{
            MBProgressHUD.showToast(message: "Please select your gender", name: "Zig-Zag", PPView: self.view)
            
            return false
            
            
        }else{
            return true
            
        }
    }
    
    
    func editProfileMethod() {
        
        if Reachability.isConnectedToNetwork(){
            
            if isValidParameters(){
                
                let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.updateProfileURL)"
                
                print(baseURL)
                /*user_id
                 roles_id
                 name
                 email
                 profile_image
                 dob
                 gender*/
                
                var customerId = String()
                if self.isFromEdit {
                    customerId = UserDefaults.standard.getUserId()
                }else{
                    customerId = self.userId
                }
                
                let params = ["roles_id":"2","user_id":"\(customerId)","name":"\(txtname.text ?? "")","email":"\(txtEmail.text ?? "")","dob":"\(txtDob.text ?? "")","gender":"\(genderValue)"]
                
                let imgData = self.imageView.image!.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    //                DispatchQueue.main.async {
                    multipartFormData.append(imgData!, withName: "profile_image",fileName: "profile_image.jpg", mimeType: "image/jpg")
                    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                
                                
                                if swiftyJsonVar["error"] == false {
                                    if self.isFromEdit {
                                        let alert = self.createAlert(title: "", message: "Profile updated successfully.", icon: "checkmark3")
                                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        alert.addAction(okAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else{
                                        UserDefaults.standard.setUserId(userId: self.userId)
                                        if self.isFromRoot{
                                            let alert = self.createAlert(title: "", message:"Your profile has been created successfully.", icon: "checkmark3")
                                            let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                                                appdelegate.gotoHomeVC()
                                            }
                                            alert.addAction(okAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }else{
                                        let viewControllers = self.navigationController?.viewControllers
                                        for nav in viewControllers!{
                                            if nav is CustomizationVC{
                                                self.navigationController?.popToViewController(nav, animated: true)
                                            }
                                        }
                                        
                                    }
                                    }
                                    
                                    //
                                    
                                } else {
                                    
                                    self.customAlert(title: "", message: swiftyJsonVar["message"].stringValue, icon: "checkmark3")
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            }
            
            
        }
        else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
}
