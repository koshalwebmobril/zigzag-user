//
//  MyOrdersVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 16/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController

class MyOrdersVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var blankImgView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var orderArray = [JSON]()
    
    var index = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.patternColor
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getOrderHistoryApi()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath) as! MyOrderCell
        
//"order_type"
        cell.bgView.layer.cornerRadius = 5.0
        cell.bgView.layer.shadowColor = UIColor.gray.cgColor
        cell.bgView.layer.shadowOpacity = 1
        cell.bgView.layer.shadowOffset = CGSize.zero
        cell.bgView.layer.shadowRadius = 2.0
        
        cell.cancelBtn.layer.masksToBounds = true
        cell.cancelBtn.layer.cornerRadius = 5.0
        cell.trackBtn.layer.masksToBounds = true
        cell.trackBtn.layer.cornerRadius = 5.0
        
        cell.logoImgView.layer.masksToBounds = true
        cell.logoImgView.layer.cornerRadius = 5
        
        let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(self.orderArray[indexPath.row]["provider"]["user_image"].stringValue)")
        
        cell.logoImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        cell.nameLabel.text = orderArray[indexPath.row]["provider"]["name"].stringValue
        
        cell.addressLabel.text = orderArray[indexPath.row]["provider"]["address"].stringValue
        
        let addStr = NSMutableString()
        
        for item in orderArray[indexPath.row]["order_detail"].arrayValue{
            
            addStr.appendFormat("%@ * %@\n", item["quantity"].stringValue,item["product_name"].stringValue)
            
        }
        cell.itemslabel.text = addStr as String
        
        cell.amountLabel.text = "$\(orderArray[indexPath.row]["final_order_amount"].stringValue)"
        
        let dateStr = orderArray[indexPath.row]["created_at"].stringValue
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date : Date = dateFormatterGet.date(from: dateStr)!
        
        let orderDate = Date.getFormattedDate(date: date, format: "MMM d, yyyy h:mm a")
        cell.dateLabel.text = "\(String(describing: orderDate))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        vc.productDetails = self.orderArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func trackOrderAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: position)!
        
    
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewTrackOrderVC") as! NewTrackOrderVC
        vc.orderId = orderArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func cancelOrderAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        
        let id = self.orderArray[indexPath!.row]["id"].stringValue
        let status = self.orderArray[indexPath!.row]["order_status"].intValue
        
        self.index = indexPath!
        
        if status >= 2{
            
            self.customAlert(title: "", message: "You can not cancel this order.", icon: "checkmark3")
            
            
        }else{
        
            let alert = self.createAlert(title: "", message:"Do you want to cancel this order?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
             self.cancelOrderApi(orderId: id)
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
        
        }
        
    }
    
    func getOrderHistoryApi(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.orderHistory)"
        
        /*roles_id=2&user_id=5&card_id=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_type":"1"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.orderArray = swiftyVarJson["orders"].arrayValue
                    
                    self.orderArray = self.orderArray.filter({ (data) -> Bool in
                        data["order_type"].intValue == 1
                    })
                    if self.orderArray.count > 0{
                        self.tableView.isHidden = false
                        self.blankImgView.isHidden = true
                    }else{
                        self.tableView.isHidden = true
                        self.blankImgView.isHidden = false
                    }
                    
                    self.tableView.reloadData()
                    
                }else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
//                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    
    
    func cancelOrderApi(orderId:String){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.cancelOrder)"
        
        /*roles_id=2&user_id=5&card_id=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_id":"\(orderId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    

                    let alert = self.createAlert(title: "", message:"Order has been cancelled successfully.", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        self.orderArray.remove(at: self.index.row)
                        if self.orderArray.count > 0{
                            self.tableView.isHidden = false
                            self.blankImgView.isHidden = true
                            self.tableView.reloadData()
                        }else{
                            self.tableView.isHidden = true
                            self.blankImgView.isHidden = false
                            self.tableView.reloadData()
                        }
                    }
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                    
                }else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
}
