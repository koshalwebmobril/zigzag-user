//
//  TrackCourierVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 06/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TrackCourierVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blankImgView: UIImageView!
    
    var courierList = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getOrderHistoryApi()
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courierList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CourierCell", for: indexPath) as! CourierCell
        cell.bgView.layer.masksToBounds = true
        cell.bgView.layer.cornerRadius = 10.0
        cell.bgView.layer.borderColor = UIColor.lightGray.cgColor
        cell.bgView.layer.borderWidth = 2.0
        cell.topView.layer.masksToBounds = true
        cell.topView.layer.cornerRadius = cell.topView.frame.height/2
        
        cell.bottomView.layer.masksToBounds = true
        cell.bottomView.layer.cornerRadius = cell.topView.frame.height/2
        
        cell.trackId.text = courierList[indexPath.row]["id"].stringValue
        cell.priceLabel.text = String(format: "$%.2f", courierList[indexPath.row]["final_order_amount"].floatValue)
        
        cell.billingAddress.text = courierList[indexPath.row]["billing_address"].stringValue
        cell.DestinationAddress.text = courierList[indexPath.row]["destination_address"].stringValue
        
        if courierList[indexPath.row]["order_status"].intValue == 1{
            cell.statusLabel.text = "ORDER PLACED"
        }else if courierList[indexPath.row]["order_status"].intValue == 2{
            cell.statusLabel.text = "ASSIGNED"
            
        }else if courierList[indexPath.row]["order_status"].intValue == 3{
            cell.statusLabel.text = "OUT FOR DELIVERY"
            
        }else if courierList[indexPath.row]["order_status"].intValue == 4{
            cell.statusLabel.text = "DELIVERED"
            
        }else if courierList[indexPath.row]["order_status"].intValue == 5{
            cell.statusLabel.text = "CANCELLED"
           
        }else{
            cell.statusLabel.text = "REFUNDED"
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CourierDetailVC") as! CourierDetailVC
        vc.data = courierList[indexPath.row]
       
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func getOrderHistoryApi(){
            if Reachability.isConnectedToNetwork(){
                
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.expressServiceHistoryURL)"
            
            /*roles_id=2&user_id=5&card_id=2*/
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        self.courierList = swiftyVarJson["orders"].arrayValue
                       
                        self.tableView.isHidden = false
                        self.blankImgView.isHidden = true
                        self.tableView.reloadData()
                        
                    }else{
                        self.tableView.isHidden = true
                        self.blankImgView.isHidden = false
   
                        
                    }
                    
                    
                    
                    
                }else{
                 MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        }
}
