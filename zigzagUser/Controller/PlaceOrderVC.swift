//
//  PlaceOrderVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 20/04/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Braintree
import IQKeyboardManager
import BraintreeDropIn
import JHTAlertController

class PlaceOrderVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var blankImgView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var cartItemArray = [JSON]()
    var subtotalAmount = String()
    var braintreeClient: BTAPIClient!
    
    var grandTotal = Float()
    
    var addressArray = [JSON]()
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var checkOutBtn: UIButton!
    
    var strNote = String()
    var deliveryCharge = Float()
    var discountValue = 0.0
    var totalBillingAmount = String()
    
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var addressType: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var changeAddressBtn: UIButton!
    
    @IBOutlet weak var addAdressBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.view.backgroundColor = UIColor.patternColor
        
        
        
        
        
        
    }
    
    fileprivate func cartUI() {
        checkOutBtn.layer.masksToBounds = true
        checkOutBtn.layer.cornerRadius = 10.0
        
        
        tableView.isHidden = true
        bottomView.isHidden = true
        bottomHeight.constant = 0.0
        
        
        circleView.layer.masksToBounds = true
        circleView.layer.cornerRadius = circleView.frame.height/2
        
        addAdressBtn.layer.masksToBounds = true
        addAdressBtn.layer.cornerRadius = addAdressBtn.frame.height/2
        
        
        
        
    }
    
    
    
    
    
    fileprivate func checkOutWithBraintree() {
        braintreeClient = BTAPIClient(authorization: "sandbox_nd7n7y7v_rv8zpf3xvzq3zsry")
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self
        
        let request = BTPayPalRequest(amount: subtotalAmount)
        request.currencyCode = "USD"
        
        
        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
                
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
                
                VC.oderAmount = "\(self.subtotalAmount)"
                VC.deliveryCharge = "0.00"
                VC.providerId = self.cartItemArray[0]["provider_id"].stringValue
                VC.clientNotes = cell.txtClientNote.text ?? ""
                VC.modalPresentationStyle = .fullScreen
                self.present(VC, animated: true)
                
            } else if error != nil {
                print(error ?? "No error")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    func setDataToAddressView(){
        
        for (index,_) in addressArray.enumerated(){
            
            if addressArray[index]["primary_status"].intValue == 1{
                
                nameLabel.text = addressArray[index]["name"].stringValue
                //            if addressArray[index]["primary_status"].intValue == 1{
                //            addressType.text = "Delivery Address"
                //            }else{
                //                addressType.text = "Office (Default)"
                //            }
                
                addressLabel.text = addressArray[index]["address"].stringValue + " " + addressArray[index]["city"].stringValue + " " + addressArray[index]["state"].stringValue + " " + addressArray[index]["country"].stringValue + " " + addressArray[index]["zip"].stringValue
            }
        }
    }
    
    
    @IBAction func changeAddressAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        cartUI()
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        cartList()
        getAllAddress()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
    }
    
    
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let text = textField.text as? String{
            strNote = text
        }
        tableView.reloadData()
        return true
    }
    
    @IBAction func checkoutAction(_ sender: Any) {
        
        if addressArray.count > 0{
            
            let alert = self.createAlert(title: "", message:"Do you want to checkout?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
                
                VC.oderAmount = "\(self.subtotalAmount)"
                VC.deliveryCharge = "\(self.deliveryCharge)"
                VC.restaurantId = self.cartItemArray[0]["provider_id"].stringValue
                VC.clientNotes = self.strNote
                VC.finalAmount = self.grandTotal
                VC.finalAmount = (self.grandTotal - Float(self.discountValue))
                VC.discount = self.discountValue
                VC.modalPresentationStyle = .fullScreen
                self.present(VC, animated: true)
                
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            let alert = self.createAlert(title: "", message: "Please add your delivery address.", icon: "cart-1")
            
            let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                
                self.dismiss(animated: true, completion: nil)
            }
            
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        //        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
        //        let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
        //
        //        VC.oderAmount = "\(self.subtotalAmount)"
        //        VC.deliveryCharge = "0.00"
        //        VC.providerId = self.cartItemArray[0]["provider_id"].stringValue
        //        VC.clientNotes = cell.txtClientNote.text ?? ""
        //        VC.modalPresentationStyle = .fullScreen
        //        self.present(VC, animated: true)
        
        //        checkOutWithBraintree()
        
    }
    
    @IBAction func removeAllAction(_ sender: Any) {
        
        if cartItemArray.count > 0{
            
            let alert = self.createAlert(title: "", message:"Do you want to remove all items from cart?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
                self.emptyCart()
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            let alert = self.createAlert(title: "", message: "Cart is Empty.", icon: "cart-1")
            
            let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                
                self.dismiss(animated: true, completion: nil)
            }
            
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    
    func cartList(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getCartItemList)"
            
            print(baseURL)
            /*roles_id
             user_id*/
            
            let providerId = UserDefaults.standard.getProviderId()
            let userId = UserDefaults.standard.getUserId()
            let params:[String:Any] = ["roles_id":"2","user_id":userId,"provider_id":"\(providerId)"]
            
            print("Login Parameters:\(params)")
            
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    
                    if swiftyVarJson["error"] == false{
                        self.cartItemArray.removeAll()
                        self.cartItemArray = swiftyVarJson["data"].arrayValue
                        
                        self.deliveryCharge = swiftyVarJson["delivery_charge"].floatValue
                        self.tableView.isHidden = false
                        self.bottomHeight.constant = 140.0
                        self.bottomView.isHidden = false
                        self.blankImgView.isHidden = true
                        
                        self.tableView.reloadData()
                        
                        
                    }else{
                        
                        self.cartItemArray.removeAll()
                        self.tableView.isHidden = true
                        self.bottomView.isHidden = true
                        self.bottomHeight.constant = 0.0
                        self.blankImgView.isHidden = false
                        
                    }
                    
                }else{
                    MBProgressHUD.showToast(message: "The request timeout.", name: "Zag-Zag", PPView: self.view)
                    
                    
                    
                }
                
                
            }
            
            
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    
    func getAllAddress()  {
        
        if Reachability.isConnectedToNetwork(){
            
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getAllAddressURL)"
            
            /*roles_id
             user_id*/
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
            print(params)
            
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        self.addressArray = swiftyVarJson["address"].arrayValue
                        
                        self.addAdressBtn.isHidden = true
                        self.circleView.isHidden = false
                        self.addressType.isHidden  = false
                        self.changeAddressBtn.isHidden = false
                        self.nameLabel.isHidden = false
                        
                        self.addressLabel.isHidden = false
                        self.setDataToAddressView()
                        
                    }else{
                        
                        self.addAdressBtn.isHidden = false
                        self.circleView.isHidden = true
                        self.addressType.isHidden  = true
                        self.changeAddressBtn.isHidden = true
                        self.nameLabel.isHidden = true
                        
                        self.addressLabel.isHidden = true
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                }else{
                    //                MBProgressHUD.showToast(message: "No Internet", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    func emptyCart(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.emptyCart)"
            
            /*roles_id
             user_id*/
            
            
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
            print(params)
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                //                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        if let tabItems = self.tabBarController?.tabBar.items {
                            let tabItem = tabItems[2]
                            tabItem.badgeColor = .baseOrange
                            tabItem.badgeValue = "0"
                        }
                        UserDefaults.standard.setValue(0, forKey: "cart_count")
//                        NotificationCenter.default.post(name: NSNotification.Name("deleteCartCount"), object: nil)
                        let alert = self.createAlert(title: "", message:swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                            self.cartList()
                        }
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }else{
                        
                        
                        
                    }
                    
                    
                    
                    
                }else{
                    //                MBProgressHUD.showToast(message: "No Internet", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
}


extension PlaceOrderVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItemArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as!  CartCell
        
        
        let imgStr = self.cartItemArray[indexPath.row]["product_img"].stringValue
        let imgURL :URL = URL(string:"\(GlobalURL.imagebaseURL)\(imgStr)")!
        
        cell.logoImg.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        
        cell.lblName.text = cartItemArray[indexPath.row]["product_name"].stringValue
        cell.lblAmount.text = "$\(cartItemArray[indexPath.row]["amount"].stringValue)"
        cell.quantityLabel.text = "QTY : \(cartItemArray[indexPath.row]["quantity"].stringValue)"
        let customArray = cartItemArray[indexPath.row]["selected_customisations"].arrayValue
        let descriptionStr = NSMutableString()
        if customArray.count > 0{
            for data in customArray{
                descriptionStr.append(String(format: "%@ - %@ - $%.2f \n",  data["customisation"].stringValue,data["subtypes"].stringValue,data["prices"].floatValue))
//                descriptionStr.appendFormat("* %@", data["subtypes"].stringValue)
            }
        }else{
            descriptionStr.append(cartItemArray[indexPath.row]["description"].stringValue)
        }
        cell.lblDesc.text = "\(descriptionStr)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        if cartItemArray.count > 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
            var totalAmount = Float()
            var discountAmount = Float()
            for item in cartItemArray{
                discountAmount = discountAmount + item["discount_price"].floatValue
                totalAmount = totalAmount + item["amount"].floatValue
            }
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: cell.txtClientNote.frame.height))
            let image = UIImageView(frame: CGRect(x: 15, y: (paddingView.frame.height/2) - 10, width: 20, height: 20))
            image.image = UIImage(named: "text_message")
            paddingView.addSubview(image)
            cell.txtClientNote.leftViewMode = .always
            cell.txtClientNote.leftView = paddingView
            cell.txtClientNote.delegate = self
            cell.txtClientNote.text = strNote
            cell.lblFee.text = String(format: "$%.2f", deliveryCharge)
            
            grandTotal = totalAmount + deliveryCharge
            self.discountValue = Double(discountAmount)
            self.subtotalAmount = String(format: "%.2f", totalAmount)
            cell.lblSubtotal.text = String(format: "$%.2f", totalAmount)
            cell.discount_label.text = String(format: "$%.2f", discountAmount)
            cell.grandTotal.text = String(format: "$%.2f", (grandTotal - discountAmount))
            self.totalBillingAmount = "\(grandTotal)"
            return cell
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        
        if cartItemArray.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header") as!  CartCell
            
            cell.lblItems.text = "\(cartItemArray.count) ITEM(S)"
            
            return cell
            
        }
        
        return view
        
    }
}


extension PlaceOrderVC : BTViewControllerPresentingDelegate{
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
    }
    
    
}


extension PlaceOrderVC:BTAppSwitchDelegate{
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
    
    
}
