//
//  SetAddressVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 02/07/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import CountryPickerView

protocol SetAddressDelegate {
    func setAddress(address:String,lattitude:Double,longitude:Double,name:String,mobile:String,addressline2:String,landmark:String,city:String,state:String,country:String)
}

class SetAddressVC: UIViewController,UITextFieldDelegate,CountryPickerViewDelegate, CountryPickerViewDataSource,SelectAddressDelegate {
    

    func updateAddress(address: String, addressFirst: String, city: String, state: String, country: String, countryCode: String, zip: String, latitude: Double, longitude: Double) {
        txtAddress.text = address + " " + addressFirst
      lat = latitude
        long = longitude
        
        txtCity.text = city
        txtState.text = state
        txtCountry.text = country
    }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        
        return .navigationBar
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
       
    }
    
    
   
    
    @IBOutlet weak var addressLine2: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var flagImgView: UIImageView!
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    
    var countryView : CountryPickerView!
    var addressType = String()
    var delegate : SetAddressDelegate!
    
    var lat = Double()
    var long = Double()
    
    var data = [String:String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setupUI()
    }
    func setupUI(){
        
        
        txtAddress.delegate = self
        txtName.delegate = self
        txtPhone.delegate = self
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius  = 5.0
        
        countryView = CountryPickerView()
        
        countryView.delegate = self
        countryView.dataSource = self
        countryView?.isHidden = true
        let country = countryView.selectedCountry
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
        
        
        
        
        
        countryView.countryDetailsLabel.textColor = .white
        
        if data.count > 0{
            txtName.text = data["name"]
            txtPhone.text = data["phone"]
            txtAddress.text = data["address"]
            addressLine2.text = data["line2"]
            txtLandmark.text = data["landmark"]
            txtCity.text = data["city"]
            txtState.text = data["state"]
            txtCountry.text = data["country"]
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtPhone {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }  else {
            return true
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dropDownAction(_ sender: Any) {
        txtPhone.resignFirstResponder()
        countryView.showCountriesList(from: self)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if addressType != ""{
        if textField == txtAddress{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            return false
        }
        }
        return true
    }
    
    func isValidText() -> Bool{
        
        if !(txtName.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter your name", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
        else if !(txtPhone.text?.isValidTextField())! || !(txtPhone.text?.isValidPhoneNumber())!{
            MBProgressHUD.showToast(message: "Please enter valid phone number", name: "Zag-Zag", PPView: self.view)
            return false
            
        }else if !(txtAddress.text?.isValidTextField())!{
            MBProgressHUD.showToast(message: "Please enter Address line 1", name: "Zag-Zag", PPView: self.view)
            return false
            
        }else if !(addressLine2.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter Address line 2", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
//        else if !(txtLandmark.text?.isValidTextField())! {
//            MBProgressHUD.showToast(message: "Please enter nearby Landmark", name: "Zag-Zag", PPView: self.view)
//            return false
//
//        }
        else if !(txtCity.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter City", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
//        else if !(txtState.text?.isValidTextField())! {
//            MBProgressHUD.showToast(message: "Please enter State", name: "Zag-Zag", PPView: self.view)
//            return false
//
//        }
        else if !(txtCountry.text?.isValidTextField())! {
            MBProgressHUD.showToast(message: "Please enter Country", name: "Zag-Zag", PPView: self.view)
            return false
            
        }
        
        return true
    }
    
    
    
    @IBAction func locationAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectAddressVC") as! SelectAddressVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func setAddressAction(_ sender: Any) {
        if isValidText(){
            delegate.setAddress(address: txtAddress.text!,lattitude: self.lat ,longitude: self.long,name: txtName.text ?? "",mobile: txtPhone.text ?? "",addressline2:addressLine2.text ?? "",landmark:txtLandmark.text ?? "",city:txtCity.text ?? "",state:txtState.text ?? "",country:txtCountry.text ?? "")
            self.navigationController?.popViewController(animated: true)
        }
    }
    

}
