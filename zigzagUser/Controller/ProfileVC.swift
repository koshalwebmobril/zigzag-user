//
//  ProfileVC.swift
//  zigzagUser
//
//  Created by Webmobril on 06/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

class ProfileVC: UIViewController {
    
    @IBOutlet weak var logout_btn: UIButton!
    @IBOutlet weak var profilepic: UIImageView!
    @IBOutlet weak var editbtn: UIButton!
    @IBOutlet weak var profilename: UILabel!
    
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var phoneNumber: UILabel!
    
    var profileData = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        //        self.view.backgroundColor = UIColor.patternColor
        logout_btn.layer.cornerRadius = 10
        logout_btn.layer.masksToBounds = false
        
        profilepic.layer.cornerRadius = profilepic.frame.width/2
        profilepic.layer.masksToBounds = false
        profilepic.clipsToBounds = true
        profilepic.layer.borderWidth = 2
        profilepic.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.3529411765, blue: 0.137254902, alpha: 1)
        
        editbtn.layer.cornerRadius = editbtn.frame.width/2
        editbtn.layer.masksToBounds = false
        editbtn.clipsToBounds = true
        
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserId() == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            vc.isFromRoot = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            getProfileDetails()
        }
    }
    
    func setProfileData(){
        
        
        
        let imgURL : URL = URL(string: "\(GlobalURL.imagebaseURL)\(profileData["user_image"].stringValue)")!
        
        
        //        profilepic.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        
        self.profilepic.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "profile-3"), completed: nil)
        
        profilename.text = "\(profileData["name"].stringValue)"
        
        phoneNumber.text = UserDefaults.standard.getPhone()
        
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func myaddress(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func myorders(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersVC") as! MyOrdersVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func myhistory(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func settings2(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editProfileAction(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        vc.updateData = self.profileData
        vc.isFromEdit = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func trackAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrackCourierVC") as! TrackCourierVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = self.createAlert(title: "", message:"Are you sure! Do you want to logout?", icon: "checkmark3")
        
        let okAction = JHTAlertAction(title: "Yes", style: .default) {(UIAction) in
            
            UserDefaults.standard.removeUserdefaultData()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.gotoLoginVC()
            
        }
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    func logout(){
        
        if Reachability.isConnectedToNetwork(){
            
            let logoutURL = "\(GlobalURL.baseURL)\(GlobalURL.logoutURL)"
            
            let userId = UserDefaults.standard.getUserId()
            
            let param :[String:Any] = ["user_id":"\(userId)"]
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(logoutURL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hideHUD()
                
                if response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    
                    if swiftyVarJson["error"] == false{
                        
                        let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "OK", style: .default) {(UIAction) in
                            UserDefaults.standard.removeUserdefaultData()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoLoginVC()
                            
                        }
                        
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }else{
                        
                        UserDefaults.standard.removeUserdefaultData()
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.gotoLoginVC()
                        
                    }
                    
                }else{
                    //                print(response.result.value!)
                }
                
            }
            
            
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    func getProfileDetails(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getProfileData)"
            
            
            
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
            
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        
                        self.profileData = swiftyVarJson["data"]
                        
                        self.headerView.isHidden = false
                        self.optionView.isHidden = false
                        
                        self.setProfileData()
                        
                        
                    }else{
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                        
                        
                    }
                    
                    
                    
                    
                }else{
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
}
