//
//  PaymentVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 13/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class PaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cardBtn: UIButton!
    @IBOutlet weak var payPalBtn: UIButton!
    var selectedIndexPath = IndexPath()
    var CardArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        tableView.delegate = self
        tableView.dataSource = self

        
    }
    
    @IBAction func cardBtnAction(_ sender: Any) {
        
        tableView.isHidden = false
        payPalBtn.isSelected = false
        cardBtn.isSelected = true
        getSavedCard()
    }
    
    @IBAction func payPalAction(_ sender: Any) {
        
        tableView.isHidden = true
        cardBtn.isSelected = false
        payPalBtn.isSelected = true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
        return CardArray.count 
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{

        if selectedIndexPath == indexPath {
          return 200
        }else{
        return 80
            }
        }else{
            if selectedIndexPath == indexPath {
              return 350
            }else{
            return 50
                }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if indexPath.section == 0{
        let cardCell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! PaymentCell
            
            cell = cardCell
        }else{
            
            let footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterCell", for: indexPath) as! PaymentCell
            
            cell = footerCell
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        selectedIndexPath = indexPath
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    
    func getSavedCard(){
        
        
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.savedCardList)"
        
        /*user_id
        roles_id*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.CardArray = swiftyVarJson["dishes"].arrayValue
                    
                    self.tableView.reloadData()
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }
    
    func saveNewCard(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.saveNewCard)"
        
        /*roles_id=2&user_id=5&name_on_card=sfsf&card_number=435436456546&expire_month=06&expire_year=22&card_cvv=456&card_type=1*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","card_number":"","expire_month":"","expire_year":"","card_cvv":"","card_type":"","name_on_card":""]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    
                }else{
                    
                   self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }
    
    
    func deleteCard(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.removeCard)"
        
        /*roles_id=2&user_id=5&card_id=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","card_id":""]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }
    


}
