//
//  AlertVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 09/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class AlertVC: UIViewController {
    
    
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    var msgStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        frontView.layer.cornerRadius = 5
        frontView.layer.shadowColor = UIColor.lightGray.cgColor
        frontView.layer.shadowOpacity = 1
        frontView.layer.shadowOffset = CGSize.zero
        frontView.layer.shadowRadius = 1.5

        messageLabel.text = msgStr
        
    }
    

    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.navigationController?.parent?.dismiss(animated: true, completion: nil)
        }
    }
    
}
