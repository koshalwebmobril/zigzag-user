//
//  OtpVC.swift
//  VitesEats
//
//  Created by Sandeep Kumar on 09/11/2020.
//  Copyright © 2020 Sandeep Kumar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class OtpVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var resend_label: UILabel!
    @IBOutlet weak var resend_btn: UIButton!
    @IBOutlet weak var phone_lable: UILabel!
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var verifynow: UIButton!
    @IBOutlet weak var one: UITextField!
    @IBOutlet weak var two: UITextField!
    @IBOutlet weak var three: UITextField!
    @IBOutlet weak var four: UITextField!
    var userId : String?
    var otpStr : String?
    var codeNumber :String?
    var mobileNumber :String?
    var timer = Timer()
    var count = 60
    var isFromRoot = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.customAlert(title: "", message: "Verification Code: \(otpStr ?? "")", icon: "checkmark3")
        phone_lable.text = codeNumber! + mobileNumber!
//        var count = 0
//        for item in otpStr!{
//            if count == 0{
//                one.text = "\(item)"
//                count = count + 1
//            }else if count == 1{
//                two.text = "\(item)"
//                count = count + 1
//            }else if count == 2{
//                three.text = "\(item)"
//                count = count + 1
//            }else{
//                four.text = "\(item)"
//            }
//        }
        setUIView()
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUIView(){
        one.delegate = self
        two.delegate = self
        three.delegate = self
        four.delegate = self
        one.textAlignment = .center
        two.textAlignment = .center
        three.textAlignment = .center
        four.textAlignment = .center
        
        one.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        two.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        three.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        four.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerDidFire(_:)), userInfo: nil, repeats: true)
        
//        one.layer.borderColor = #colorLiteral(red: 1, green: 0.6, blue: 0, alpha: 1)
//        one.layer.borderWidth = 2.0
//        one.layer.cornerRadius = 5.0
//        one.clipsToBounds = true
//
//        two.layer.borderColor = #colorLiteral(red: 1, green: 0.6, blue: 0, alpha: 1)
//        two.layer.borderWidth = 2.0
//        two.layer.cornerRadius = 5.0
//        two.clipsToBounds = true
//
//        three.layer.borderColor = #colorLiteral(red: 1, green: 0.6, blue: 0, alpha: 1)
//        three.layer.borderWidth = 2.0
//        three.layer.cornerRadius = 5.0
//        three.clipsToBounds = true
//
//        four.layer.borderColor = #colorLiteral(red: 1, green: 0.6, blue: 0, alpha: 1)
//        four.layer.borderWidth = 2.0
//        four.layer.cornerRadius = 5.0
//        four.clipsToBounds = true
    }
    
    @objc func timerDidFire(_ timer: Timer) {
        count = count - 1
        resend_label.isHidden = false
        resend_btn.isHidden = true
        resend_label.text = "Resend Code: 00:\(count)"
        
        if count == 0{
            count = 60
            self.timer.invalidate()
            resend_label.isHidden = true
            resend_btn.isHidden = false
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text
        if text?.count == 1 {
            switch textField {
            case one:
                two.becomeFirstResponder()
            case two:
                three.becomeFirstResponder()
            case three:
                four.becomeFirstResponder()
            case four:
                four.resignFirstResponder()
            default:
                break
            }
        }else {
            switch textField {
                
            case one:
                one.resignFirstResponder()
            case two:
                one.becomeFirstResponder()
            case three:
                two.becomeFirstResponder()
            case four:
                three.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    
    
    @IBAction func resendOtpAction(_ sender: Any) {
        resendOTP()
    }
    
    
    @IBAction func verifyaction(_ sender: Any) {
        if (one!.text == "") {
            self.customAlert(title: "", message: "Please enter four digit Verification Code.", icon: "checkmark3")
        }
        else if (two!.text == "") {
            self.customAlert(title: "", message: "Please enter four digit Verification Code.", icon: "checkmark3")
        }
        else if (three!.text == "") {
            self.customAlert(title: "", message: "Please enter four digit Verification Code.", icon: "checkmark3")
        }
        else if (four!.text == "") {
            self.customAlert(title: "", message: "Please enter four digit Verification Code.", icon: "checkmark3")
        }
        else{
//            let enteredOtp = "\(one.text ?? "")\(two.text ?? "")\(three.text ?? "")\(four.text ?? "")"
//            if self.otpStr == enteredOtp{
                otpValidationMethod()
//            }else{
//                self.customAlert(title: "", message: "Please enter valid Verification Code.", icon: "checkmark3")
//            }
        }
    }
    
    
    func otpValidationMethod(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.verifyOtpURL)"
            
            print(baseURL)
            /*user_id
             otp
             roles_id
             login_flag*/
            var otpStr = String()
            otpStr.append(one.text ?? "")
             otpStr.append(two.text ?? "")
             otpStr.append(three.text ?? "")
             otpStr.append(four.text ?? "")
            let login_flag = UserDefaults.standard.getLoginFlag()
            
            let token = UserDefaults.standard.getFCMToken()
            let params:[String:Any] = ["roles_id":"2","otp":otpStr,"login_flag":login_flag,"user_id":userId ?? "","device_type":"2" ,"device_token":"\(token)"]
            
            print("Login Parameters:\(params)")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hideHUD()
                
                if response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    
                    if swiftyVarJson["error"] == false{
                        UserDefaults.standard.setUserId(userId: self.userId ?? "")
                        if self.isFromRoot {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoHomeVC()
                        }else{
                            let viewControllers = self.navigationController?.viewControllers
                            for nav in viewControllers!{
                                if nav is CustomizationVC{
                                    self.navigationController?.popToViewController(nav, animated: true)
                                }
                            }
                        }
                        
                    }else{
                        let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        let okAction = JHTAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }else{
                    
                    MBProgressHUD.showToast(message: "The request timed out.", name: "", PPView: self.view)
                    
                    
                }
                
                
            }
            
            
            
            
            
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    func resendOTP(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.loginURL)"
            
            print(baseURL)
            /*roles_id
             device_type
             device_token
             mobile*/
            
            let token = UserDefaults.standard.getFCMToken()
            let params = ["roles_id":"2","device_type":"2","device_token":token,"mobile":mobileNumber ?? "","country_code":
                "\(codeNumber ?? "")"]
            
            print("Login Parameters:\(params)")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hideHUD()
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    
                    if swiftyVarJson["error"] == false{
                        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerDidFire(_:)), userInfo: nil, repeats: true)
                        self.otpStr = swiftyVarJson["result"]["otp"].stringValue
//                        var count = 0
//                        for item in self.otpStr!{
//                            if count == 0{
//                                self.one.text = "\(item)"
//                                count = count + 1
//                            }else if count == 1{
//                                self.two.text = "\(item)"
//                                count = count + 1
//                            }else if count == 2{
//                                self.three.text = "\(item)"
//                                count = count + 1
//                            }else{
//                                self.four.text = "\(item)"
//                            }
//                        }
//                        let alert = self.createAlert(title: "", message: "\(swiftyVarJson["message"].stringValue) : \(self.otpStr ?? "")", icon: "checkmark3")
//                            let okAction = JHTAlertAction(title: "Ok", style: .default, handler: nil)
//                            alert.addAction(okAction)
//                            self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        
                        let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        let okAction = JHTAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }else{
                    
                    MBProgressHUD.showToast(message: "The request timed out.", name: "", PPView: self.view)
                    
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        
    }
    
}
