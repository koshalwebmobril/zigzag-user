//
//  SearchViewController.swift
//  Shopfeur Users
//
//  Created by deepkohli on 11/11/19.
//  Copyright © 2019 ShopfeurUsers.com. All rights reserved.
//

import UIKit
import GooglePlaces

protocol SearchViewControllerDelegate {
    func updatedLocationAddress(address: String, addressFirst: String, city: String, state: String, country: String, countryCode: String, zip: String, latitude: Double, longitude: Double)
}

class SearchViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let geoCoder = CLGeocoder()
    
    // Variable
    let placesClient = GMSPlacesClient.shared()
    var locationName:String = String()
    var locationLatitude = Double()
    var locationLongititude = Double()
    var newLocationAddress: String = String()
    var isFromSelectedClass: String = String()
    
    var latitude = Double()
    var longitude = Double()
    
    var arrayAddress = [GMSAutocompletePrediction]()
    lazy var filter: GMSAutocompleteFilter = {
        let filter = GMSAutocompleteFilter()
        return filter
    }()
    
     var delegate: SearchViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        self.view.backgroundColor = UIColor.patternColor
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .blue
        
        self.navigationController?.isNavigationBarHidden = true
        searchBar.delegate = self
        searchBar.barTintColor = UIColor.white
        searchBar.setBackgroundImage(UIImage.init(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha:1.0)
        
        //        searchBar.dropSearchBarShadowColor()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapOutside(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    // dismiss keyboard on tap outside
    @objc func tapOutside(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    func convertLatitudeLongitudetoAddress() {
        
        geocode(latitude: locationLatitude, longitude: locationLongititude) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
                self.newLocationAddress = String(format:"%@ %@ %@ %@ %@ %@ %@",(placemark.subLocality ?? ""), (placemark.subAdministrativeArea ?? ""), (placemark.locality ?? ""),(placemark.administrativeArea ?? ""), (placemark.country ?? ""), (placemark.isoCountryCode ?? ""), (placemark.postalCode ?? ""))
                
                print("Complete Address Format - \(self.newLocationAddress)")
                
                self.delegate?.updatedLocationAddress(address: placemark.subLocality ?? "", addressFirst: placemark.subAdministrativeArea ?? "", city: placemark.locality ?? "", state: placemark.administrativeArea ?? "", country: placemark.country ?? "", countryCode: placemark.isoCountryCode ?? "", zip: placemark.postalCode ?? "", latitude: self.locationLatitude, longitude: self.locationLongititude)
                
                self.searchBar.text = self.newLocationAddress
                self.locationName =   self.newLocationAddress
            }
        }
        
    }
    
    //MARK:- UISearch Bar Delegates
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //            tableView.isHidden = false
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.tableView.isHidden = true
        }
    }
    
    
    //MARK:- Search Bar Delegate
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let searchStr = (searchBar.text! as NSString).replacingCharacters(in: range, with: text)
        let whiteSpaces = CharacterSet.whitespacesAndNewlines
        if searchStr.isEmpty || searchStr.trimmingCharacters(in: whiteSpaces).isEmpty {
            self.tableView.isHidden = true
            
        } else {
            self.tableView.isHidden = false
        }
        if searchStr == "" {
            self.arrayAddress = [GMSAutocompletePrediction]()
        } else {
//            MBProgressHUD.showHUDMessage(message: "", PPview: self.tableView)
            GMSPlacesClient.shared().autocompleteQuery(searchStr, bounds: nil, filter: filter, callback:{
                (result, error) in
//                MBProgressHUD.hide(for: self.tableView, animated: true)
                if error == nil && result != nil {
                    
                    self.arrayAddress = result!
                    self.tableView.reloadData()
                    self.tableView.layoutIfNeeded()
                    
                }
            })
        }
        self.tableView.reloadData()
        return true
    }
    
    // MARK: - Back button tapped
    @IBAction func btnBackAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchViewCell", for: indexPath) as! SearchViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.lblSearch.attributedText = arrayAddress[indexPath.row].attributedFullText
        return cell
        
    }
    
    // MARK: - UITableViewDelegate methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected Row")
        searchBar.text = arrayAddress[indexPath.row].attributedFullText.string
        locationName = searchBar.text!
        self.view.endEditing(true)
        
        
        let placeId = arrayAddress[indexPath.row].placeID
        self.GetPlaceDataByPlaceID(placeID: placeId)
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Get place data by place ID.
    func GetPlaceDataByPlaceID(placeID: String) {
        self.placesClient.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            if let place = place {
                //                self.moveAtLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                self.locationLatitude = place.coordinate.latitude
                self.locationLongititude = place.coordinate.longitude
                print(self.locationLatitude, self.locationLongititude )
                self.convertLatitudeLongitudetoAddress()
                
            } else {
                print("No place details for: \(placeID)")
            }
        })
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
//        self.dismiss(animated: true, completion: nil)
    }
    
}

