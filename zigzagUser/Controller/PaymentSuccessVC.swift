//
//  PaymentSuccessVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 20/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import SwiftyJSON
import Alamofire
import JHTAlertController

class PaymentSuccessVC: UIViewController {
    
    
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var gifView: UIImageView!
    @IBOutlet weak var continueBtn: UIButton!
    
    @IBOutlet weak var tickImage: UIImageView!
    
    @IBOutlet weak var orderLabel: UILabel!
    var clientNotes = String()
    
    var oderAmount = String()
    var providerId = String()
    var deliveryCharge = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.view.backgroundColor = UIColor.patternColor
        continueBtn.layer.masksToBounds = true
        continueBtn.layer.cornerRadius = 5.0
        
        let str = UserDefaults.standard.value(forKey: "order") as? String ?? ""
        if   str == "" {
//        paymentSuccessApi()
        }else{
            UserDefaults.standard.set("", forKey: "order")
           self.gifView.image = UIImage(named: "express_delivery")
            self.orderLabel.text = "Your Shipment request placed successfully."
        }
        
    }
    
    func setUI(){
        gifView.layer.cornerRadius = 5.0
        gifView.clipsToBounds = true
        gifView.loadGif(name: "5215-loading-checkmark")
        gifView.animationDuration = 5
        gifView.animationRepeatCount = 0
        
        
    }
    
    
    
    @IBAction func continueBtnAction(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoHomeVC()
    }
    
    

    
    func paymentSuccessApi(){
        
        
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.paymentApi)"
        
        /*roles_id=2&user_id=14&provider_id=62&order_amt=553.00&delivery_charge=0.0*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(self.providerId)","order_amt":"\(self.oderAmount)","delivery_charge":"\(self.deliveryCharge)","client_note":"\(clientNotes)","payment_type":"1"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    
                    

                    self.bgView.isHidden = false
                        UserDefaults.standard.setProviderId(providerId: 0)
                        UserDefaults.standard.setProviderName(name: "")

                    
                }else{
                    
                    let alert = self.createAlert(title: "", message:swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                   
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }
    
    
    func expPayment(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.expressPaymentURL)"
        
        /*roles_id=2&user_id=14&provider_id=62&order_amt=553.00&delivery_charge=0.0*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","express_id":"\(self.providerId)","order_amount":self.oderAmount]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    
                    UserDefaults.standard.set("", forKey: "order")

                    self.bgView.isHidden = false
                    self.gifView.image = UIImage(named: "express_delivery")
                    self.orderLabel.text = "Your Shipment request placed successfully."
                        

                    
                }else{
                    
                    let alert = self.createAlert(title: "", message:swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                   
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }
    
}
