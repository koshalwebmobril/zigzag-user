//
//  RegisterVC.swift
//  zigzagUser
//
//  Created by Webmobril on 02/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import CountryPickerView
import JHTAlertController

class RegisterVC: UIViewController, UITextFieldDelegate ,CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        
        return .navigationBar
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
    }
    
    
    
    @IBOutlet weak var phn: SkyFloatingLabelTextField!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var phone_label: UILabel!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var countryListView: UIView!
    var countryView : CountryPickerView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var dropdwonBtn: UIButton!
    @IBOutlet weak var flagImgView: UIImageView!
    
    var idStr = String()
    var countryCode :String = "+1"
    var otpString = String()
    
    var userDetails = JSON()
    var isFromRoot = false
    
    @IBOutlet weak var skipButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.patternColor
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        countryView = CountryPickerView()
        countryView.delegate = self
        countryView.dataSource = self
        countryView?.isHidden = true
        let country = countryView.selectedCountry
        
        flagImgView.image = country.flag
        
        codeLabel.text = "\((country.code))  \(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
        
        //               countryListView.countryDetailsLabel.font = UIFont.systemFont(ofSize: 15.0)
        countryView.countryDetailsLabel.textColor = .white
        
        skipButton.layer.masksToBounds = true
        skipButton.layer.cornerRadius = skipButton.frame.height/2
        skipButton.layer.borderColor = UIColor.baseOrange.cgColor
        skipButton.layer.borderWidth = 2
        
        verifyBtn.layer.cornerRadius = 5
        self.verificationView.layer.masksToBounds = true
        self.verificationView.layer.cornerRadius = 5.0
        //        one.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //
        //        two.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //
        //        three.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //
        //        four.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        //
        //        one.delegate = self
        //        one.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.3568627451, blue: 0.1882352941, alpha: 1)
        //        one.layer.borderWidth = 2.0
        //        one.layer.cornerRadius = 5.0
        //        one.clipsToBounds = true
        //
        //        two.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.3568627451, blue: 0.1882352941, alpha: 1)
        //        two.layer.borderWidth = 2.0
        //        two.layer.cornerRadius = 5.0
        //        two.clipsToBounds = true
        //
        //        three.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.3568627451, blue: 0.1882352941, alpha: 1)
        //        three.layer.borderWidth = 2.0
        //        three.layer.cornerRadius = 5.0
        //        three.clipsToBounds = true
        //
        //        four.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.3568627451, blue: 0.1882352941, alpha: 1)
        //        four.layer.borderWidth = 2.0
        //        four.layer.cornerRadius = 5.0
        //        four.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func gotoBackAction(_ sender: Any) {
        if isFromRoot{
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.gotoHomeVC()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.verificationView.isHidden = true
        self.transparentView.isHidden = true
    }
    
    @IBAction func skipButtonAction(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        vc.selectedIndex = 0
        self.navigationController?.pushViewController(vc, animated: true)
        //       loginServiceMetod(mobileNumber: "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
     @objc func textFieldDidChange(_ textField: UITextField) {
     
     let text = textField.text
     if text?.utf16.count == 1 {
     switch textField {
     case one:
     //first.isHidden = true
     two.becomeFirstResponder()
     case two:
     //sec.isHidden = true
     three.becomeFirstResponder()
     case three:
     // thrd.isHidden = true
     four.becomeFirstResponder()
     case four:
     //fourth.isHidden = true
     four.resignFirstResponder()
     default:
     break
     }
     } else {
     switch textField {
     case four:
     //fourth.isHidden = false
     three.becomeFirstResponder()
     case three:
     //thrd.isHidden = false
     two.becomeFirstResponder()
     case two:
     //sec.isHidden = false
     one.becomeFirstResponder()
     case one:
     //first.isHidden = false
     one.resignFirstResponder()
     
     default:
     break
     }
     }
     }
     */
    
    @IBAction func dropDownAction(_ sender: Any) {
        phn.resignFirstResponder()
        countryView.showCountriesList(from: self)
    }
    
    
    @IBAction func verifyNum(_ sender: Any) {
        if (phn!.text == "") {
            self.customAlert(title: "", message: "Please enter Phone Number.", icon: "checkmark3")
            
        }
        else if !(phn.text?.isValidPhoneNumber())!{
            self.customAlert(title: "", message: "Number should be 7-15 digit long.", icon: "checkmark3")
            
        }
        else{
            phn.resignFirstResponder()
            self.transparentView.isHidden = false
            self.verificationView.isHidden = false
            phone_label.text = countryCode + phn.text!
        }
    }
    /*
     @IBAction func verifyaction(_ sender: Any) {
     if (one!.text == "") {
     self.customAlert(title: "", message: "Please enter four digit OTP.", icon: "checkmark3")
     }
     else if (two!.text == "") {
     self.customAlert(title: "", message: "Please enter four digit OTP.", icon: "checkmark3")
     }
     else if (three!.text == "") {
     self.customAlert(title: "", message: "Please enter four digit OTP.", icon: "checkmark3")
     }
     else if (four!.text == "") {
     
     self.customAlert(title: "", message: "Please enter four digit OTP.", icon: "checkmark3")
     
     
     }
     else{
     otpValidationMethod()
     
     }
     }
     */
    
    
    @IBAction func yesAction(_ sender: Any) {
        self.transparentView.isHidden = true
        self.verificationView.isHidden = true
        loginServiceMetod(mobileNumber: phn.text!)
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.transparentView.isHidden = true
        self.verificationView.isHidden = true
        phn.becomeFirstResponder()
        
    }
}

extension RegisterVC {
    
    
    func loginServiceMetod(mobileNumber:String) {
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.loginURL)"
            
            print(baseURL)
            /*roles_id
             device_type
             device_token
             mobile*/
            
            let token = UserDefaults.standard.getFCMToken()
            let params = ["roles_id":"2","device_type":"2","device_token":token,"mobile":mobileNumber,"country_code":
                "\(countryCode)"]
            
            print("Login Parameters:\(params)")
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hideHUD()
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    
                    if swiftyVarJson["error"] == false{
                        
                        UserDefaults.standard.setPhone(token: mobileNumber)
                        
                        self.userDetails = swiftyVarJson["result"]
                        UserDefaults.standard.setLoginFlag(loginFlag: swiftyVarJson["login_flag"].intValue)
                        
                        self.otpString = self.userDetails["otp"].stringValue
                        
                        if swiftyVarJson["login_flag"].intValue == 0 {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoHomeVC()
                            
                        }else if swiftyVarJson["login_flag"].intValue == 1{
                            
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
                                vc.mobileNumber = self.phn.text!
                                vc.otpStr = self.otpString
                                vc.userId = self.userDetails["id"].stringValue
                                vc.codeNumber = self.countryCode
                            vc.isFromRoot = self.isFromRoot
                                self.navigationController?.pushViewController(vc, animated: true)
                           
                        }else if swiftyVarJson["login_flag"].intValue == 2{
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
                            vc.userId = self.userDetails["id"].stringValue
                            vc.isFromRoot = self.isFromRoot
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        
                        let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        let okAction = JHTAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }else{
                    
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        
    }
    
    /*
     func otpValidationMethod(){
     
     if Reachability.isConnectedToNetwork(){
     
     let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.verifyOtpURL)"
     
     print(baseURL)
     /*user_id
     otp
     roles_id
     login_flag*/
     var otpStr = String()
     otpStr.append(one.text ?? "")
     otpStr.append(two.text ?? "")
     otpStr.append(three.text ?? "")
     otpStr.append(four.text ?? "")
     
     let login_flag = UserDefaults.standard.getLoginFlag()
     let userId = self.userDetails["id"].stringValue
     
     let token = UserDefaults.standard.getFCMToken()
     let params:[String:Any] = ["roles_id":"2","otp":otpStr,"login_flag":login_flag,"user_id":userId,"device_type":"2" ,"device_token":"\(token)"]
     
     print("Login Parameters:\(params)")
     MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
     
     Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
     
     MBProgressHUD.hideHUD()
     
     if response.result.value != nil{
     
     let swiftyVarJson = JSON(response.result.value!)
     print(swiftyVarJson)
     
     if swiftyVarJson["error"] == false{
     
     
     
     if swiftyVarJson["profile_flag"].intValue == 1{
     
     UserDefaults.standard.setUserId(userId: self.userDetails["id"].stringValue)
     
     let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
     let vc = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
     vc.selectedIndex = 0
     self.navigationController?.pushViewController(vc, animated: true)
     
     }else if  swiftyVarJson["profile_flag"].intValue == 0{
     
     let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
     let vc = mainStoryBoard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
     vc.userId = self.userDetails["id"].stringValue
     self.navigationController?.pushViewController(vc, animated: true)
     
     }
     
     
     
     
     
     
     }else{
     let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
     
     let okAction = JHTAlertAction(title: "Ok", style: .default, handler: nil)
     
     
     alert.addAction(okAction)
     
     self.present(alert, animated: true, completion: nil)
     
     }
     
     }else{
     
     MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
     
     
     }
     
     
     }
     
     
     
     
     
     }else{
     
     self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
     
     
     }
     }
     
     */
    
    
    
    
}
