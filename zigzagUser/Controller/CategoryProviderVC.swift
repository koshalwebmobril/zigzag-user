//
//  CategoryProviderVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 21/04/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

class CategoryProviderVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var blankImgView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var categoryId = String()
    var restaurantArray = [JSON]()
    
    var tabIndex = Int()
    
    var titleName = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor

        if #available(iOS 13.0, *) {
                      let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                      
                      statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
                      UIApplication.shared.keyWindow?.addSubview(statusBar)
                      } else {
                          
                          if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                              // my stuff
                              statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
                          }

                      }
        
        print(tabIndex)
        
        if tabIndex == 0{
            blankImgView.image = UIImage(named: "1-food")
        }else if tabIndex == 1{
            blankImgView.image = UIImage(named: "2-cake")
        }else if tabIndex == 2{
            blankImgView.image = UIImage(named: "3-grocery")
        }else if tabIndex == 3{
            blankImgView.image = UIImage(named: "4-gifts")
        }else if tabIndex == 4{
            blankImgView.image = UIImage(named: "5-nuts")
        }else if tabIndex == 5{
            blankImgView.image = UIImage(named: "6-water")
        }else if tabIndex == 6{
            blankImgView.image = UIImage(named: "7-delivery")
        }else {
            blankImgView.image = UIImage(named: "8-other")
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        titleLabel.text = "\(titleName)"
        
        categoryProviderList()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nearbyCell") as! TableViewCell
        
        
        
        cell.nearbyview.layer.shadowColor = UIColor.gray.cgColor
        cell.nearbyview.layer.shadowOpacity = 0.3
        cell.nearbyview.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.nearbyview.layer.shadowRadius = 6
        cell.nearbyview.layer.cornerRadius = 10
        

//        cell.nearbyimg.layer.masksToBounds = true
//        cell.nearbyimg.layer.cornerRadius = cell.nearbyimg.frame.height/2
        
        let imgURL:URL = URL(string: "\(GlobalURL.imagebaseURL)\(restaurantArray[indexPath.row]["user_image"].stringValue)")!
       cell.nearbyimg.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)

        
        cell.nearbyname.text = restaurantArray[indexPath.row]["name"].stringValue
        
        cell.desc_label.text = restaurantArray[indexPath.row]["address"].stringValue
        
        cell.address_label.text = restaurantArray[indexPath.row]["description"].stringValue
//        cell.ratingLabel.text = String(format: "%.1f", restaurantArray[indexPath.row]["avg_rating"].floatValue)
        
//        cell.ratingView.rating = restaurantArray[indexPath.row]["avg_rating"].doubleValue

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 160
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        VC.restaurantId = self.restaurantArray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
   func categoryProviderList(){
       
       if Reachability.isConnectedToNetwork(){
       
       let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.categoryProvider)"
       
       
       let params :[String:Any] = ["category_id":"\(categoryId)"]
       print(params)
      
   MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
       
       Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
     
           MBProgressHUD.hide(for: self.view, animated: true)
           
           if  response.result.value != nil{
               
               let swiftyVarJson = JSON(response.result.value!)
               print(swiftyVarJson)
               if swiftyVarJson["error"] == false{
                   
                   
                   self.restaurantArray = swiftyVarJson["data"].arrayValue
                
                if self.restaurantArray.count > 0{
                    self.tableView.isHidden  = false
                    self.blankImgView.isHidden = true
                }else{
                    self.tableView.isHidden  = true
                    self.blankImgView.isHidden = false
                }
                
                   DispatchQueue.main.async {
                        self.tableView.reloadData()
                   }
                  
                  
                   
               }else{
                
                self.tableView.isHidden = true
                self.blankImgView.isHidden = false
                   

               }
               
               
               
               
           }else{
               
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
           }
           
       }
   }else{
       
       self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
       
       
   }
   }

}
