//
//  CartVC.swift
//  zigzagUser
//
//  Created by Webmobril on 06/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManager
import Braintree
import JHTAlertController

class CartVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var checkOutBtn: UIButton!
    @IBOutlet weak var blankImgView: UIImageView!
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var addItemBtn: UIButton!
    
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var addressType: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var changeAddressBtn: UIButton!
    
    @IBOutlet weak var addAdressBtn: UIButton!
    
    var totalBillingAmount = String()
    
    var cartItemArray = [JSON]()
    var subtotalAmount = String()
    var providerId = String()
    var addressArray = [JSON]()
    var strNote = String()
    var deliveryCharge = Float()
    var grandTotal = Float()
    var discountValue = 0.0
    
    var braintreeClient: BTAPIClient!
    var payPalDriver : BTPayPalDriver!
    
    
    fileprivate func checkOutWithBraintree() {
        
        
        
        let request = BTPayPalRequest(amount: totalBillingAmount)
        request.currencyCode = "USD"
        
        
        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
                
                VC.oderAmount = "\(self.subtotalAmount)"
                VC.deliveryCharge = "0.00"
                VC.providerId = self.cartItemArray[0]["provider_id"].stringValue
                VC.clientNotes = cell.txtClientNote.text ?? ""
                VC.modalPresentationStyle = .fullScreen
                self.present(VC, animated: true)
                
                
                
                
                
            } else if error != nil {
                print(error ?? "No error")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func cartUI() {
        checkOutBtn.layer.masksToBounds = true
        checkOutBtn.layer.cornerRadius = 10.0
        
        addItemBtn.layer.masksToBounds = true
        addItemBtn.layer.cornerRadius = 10.0
        tableView.isHidden = true
        bottomView.isHidden = true
        blankImgView.isHidden = true
        bottomHeight.constant = 0.0
        
        
        circleView.layer.masksToBounds = true
        circleView.layer.cornerRadius = circleView.frame.height/2
        
        addAdressBtn.layer.masksToBounds = true
        addAdressBtn.layer.cornerRadius = addAdressBtn.frame.height/2
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.patternColor
        
        braintreeClient = BTAPIClient(authorization: "sandbox_nd7n7y7v_rv8zpf3xvzq3zsry")
        payPalDriver = BTPayPalDriver(apiClient: braintreeClient)
        payPalDriver.viewControllerPresentingDelegate = self
        
        payPalDriver.appSwitchDelegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        self.navigationController?.isNavigationBarHidden = true
        
        
        
    }
    
    @objc func dismissKeyboard() {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
        cell.txtClientNote.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        cartUI()
        
        
        
        if UserDefaults.standard.getUserId() == ""{
//            self.customAlert(title: "", message: "Please login first to get your cart list.", icon: "checkmark3")
            
            blankImgView.isHidden = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            vc.isFromRoot = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            cartItemList()
            getAllAddress()
            
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
    }
    
    
    func setDataToAddressView(){
        
        for (index,_) in addressArray.enumerated(){
            
            if addressArray[index]["primary_status"].intValue == 1{
                
                nameLabel.text = addressArray[index]["name"].stringValue
//                if addressArray[index]["primary_status"].intValue == 1{
//                    addressType.text = "Home (Default)"
//                }else{
//                    addressType.text = "Office (Default)"
//                }
                
                addressLabel.text = addressArray[index]["address"].stringValue + " \(addressArray[index]["line_2"].stringValue)" + " \(addressArray[index]["city"].stringValue)" + " \(addressArray[index]["country"].stringValue)" + " \(addressArray[index]["zip"].stringValue)"
            }
        }
    }
    
    @IBAction func changeAddressAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let text = textField.text as? String{
            strNote = text
        }
        
        tableView.reloadData()
        return true
    }
    
    
    
    @IBAction func minusBtnAction(_ sender: Any) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! CartCell
        var count: Int = Int(cell.lblQty.text ?? "") ?? 0
        
        let prevAmount = cartItemArray[indexPath!.row]["amount"].floatValue / Float(count)
        
        if count > 1{
            count = count - 1
            cell.lblQty.text = "\(count)"
        }else{
            count = count - 1
        }
        
        
        
        
        let amount = prevAmount * Float(count)
        let dishID = cartItemArray[indexPath!.row]["dish_id"].stringValue
        let discount = (amount * cartItemArray[indexPath!.row]["discount"].floatValue)/100
        updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)",discount:"\(discount)")
        
    }
    
    
    @IBAction func plusBtnAction(_ sender: Any) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! CartCell
        var count: Int = Int(cell.lblQty.text ?? "") ?? 0
        
        if count <= 99 {
        let prevAmount = cartItemArray[indexPath!.row]["amount"].floatValue / Float(count)
        count = count  + 1
        cell.lblQty.text = "\(count)"
        let amount = prevAmount * Float(count)
        let dishID = cartItemArray[indexPath!.row]["dish_id"].stringValue
            let discount = (amount * cartItemArray[indexPath!.row]["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
        }else{
            self.customAlert(title: "", message: "You can not order more than 99 Quantity", icon: "checkmark3")
        }
    }
    
    
    @IBAction func deleteItem(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        let dishId = cartItemArray[indexPath.row]["dish_id"].stringValue
        let alert = self.createAlert(title: "", message: "Do you want to delete?", icon: "checkmark3")
        let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
            self.updateCartAPi(amount: "0", quantity: "0", dishId: "\(dishId)", discount: "0")
        }
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func checkoutBtnAction(_ sender: Any) {
        
        if addressArray.count > 0{
            
            let alert = self.createAlert(title: "", message: "Do you want to checkout?", icon: "checkmark3")
                let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
                VC.oderAmount = "\(self.subtotalAmount)"
                VC.deliveryCharge = "\(self.deliveryCharge)"
                VC.restaurantId = self.cartItemArray[0]["provider_id"].stringValue
                VC.clientNotes = self.strNote
                VC.finalAmount = (self.grandTotal - Float(self.discountValue))
                VC.discount = self.discountValue
                VC.modalPresentationStyle = .fullScreen
                self.present(VC, animated: true)
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            let alert = self.createAlert(title: "", message: "Please add your delivery address.", icon: "cart-1")
            let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                    self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        }
        
        
        //        checkOutWithBraintree()
    }
    
    
    @IBAction func removeAllAction(_ sender: Any) {
        
        if cartItemArray.count > 0{
            
            let alert = self.createAlert(title: "", message: "Do you want to remove all items from cart?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
                self.emptyCart()
            }
            
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            let alert = self.createAlert(title: "", message: "Cart is Empty.", icon: "cart-1")
            let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func addItemAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        vc.restaurantId = self.providerId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension CartVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItemArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as!  CartCell
        
        self.blankImgView.isHidden = true
//        cell.steperView.layer.masksToBounds = true
//        cell.steperView.layer.borderColor = UIColor.lightGray.cgColor
//        cell.steperView.layer.cornerRadius = 10.0
//        cell.steperView.layer.borderWidth = 2.0
        
        cell.logoImg.layer.masksToBounds = true
        cell.logoImg.layer.cornerRadius = 5.0
        
        let imgStr = self.cartItemArray[indexPath.row]["product_img"].stringValue
        let imgURL :URL = URL(string:"\(GlobalURL.imagebaseURL)\(imgStr)")!
        
        cell.logoImg.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        
        cell.lblName.text = cartItemArray[indexPath.row]["product_name"].stringValue
        cell.lblAmount.text = "$\(cartItemArray[indexPath.row]["amount"].stringValue)"
        cell.lblQty.text = cartItemArray[indexPath.row]["quantity"].stringValue
        let customArray = cartItemArray[indexPath.row]["selected_customisations"].arrayValue
        let descriptionStr = NSMutableString()
        if customArray.count > 0{
            for data in customArray{
                descriptionStr.append(String(format: "%@ - %@ - $%.2f \n",  data["customisation"].stringValue,data["subtypes"].stringValue,data["prices"].floatValue))
//                descriptionStr.appendFormat("* %@", data["subtypes"].stringValue)
            }
        }else{
            descriptionStr.append(cartItemArray[indexPath.row]["description"].stringValue)
        }
        cell.lblDesc.text = "\(descriptionStr)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        if cartItemArray.count > 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Footer") as!  CartCell
            var totalAmount = Float()
            var discountAmount = Float()
            for item in cartItemArray{
                totalAmount = totalAmount + item["amount"].floatValue
                discountAmount = discountAmount + item["discount_price"].floatValue
            }
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height:cell.txtClientNote.frame.height))
            let image = UIImageView(frame: CGRect(x: 15, y: (paddingView.frame.height/2) - 10, width: 20, height: 20))
            image.image = UIImage(named: "text_message")
            paddingView.addSubview(image)
            cell.txtClientNote.leftViewMode = .always
            cell.txtClientNote.leftView = paddingView
            cell.txtClientNote.delegate = self
            cell.txtClientNote.text = strNote
            cell.lblFee.text =  String(format: "$%.2f", deliveryCharge)
            grandTotal = totalAmount + deliveryCharge
            self.subtotalAmount = String(format: "%.2f", totalAmount)
            self.discountValue = Double(discountAmount)
            cell.lblSubtotal.text = String(format: "$%.2f", totalAmount)
            cell.discount_label.text = String(format: "$%.2f", discountAmount)
            cell.grandTotal.text = String(format: "$%.2f", (grandTotal - discountAmount))
            self.totalBillingAmount = "\(grandTotal )"
            return cell
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        
        if cartItemArray.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header") as!  CartCell
            
            
                cell.lblItems.text = "\(cartItemArray.count) ITEM(S)"
            
            
            
            
            return cell
            
        }
        
        return view
        
    }
    
}

extension CartVC {
    
    func cartItemList(){
        
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getCartItemList)"
        
        print(baseURL)
        /*roles_id
         user_id*/
        
        let providerId = UserDefaults.standard.getProviderId()
        let userId = UserDefaults.standard.getUserId()
        let params:[String:Any] = ["roles_id":"2","user_id":userId,"provider_id":"\(providerId)"]
        
        print("Login Parameters:\(params)")
        
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                
                if swiftyVarJson["error"] == false{
                    
                    self.cartItemArray = swiftyVarJson["data"].arrayValue
                    
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeColor = .baseOrange
                        tabItem.badgeValue = "\(self.cartItemArray.count)"
                    }
                    
                    DispatchQueue.main.async {
                        self.providerId = self.cartItemArray[0]["provider_id"].stringValue
                        self.deliveryCharge = swiftyVarJson["delivery_charge"].floatValue
                        self.bottomView.isHidden = false
                        self.bottomHeight.constant = 140.0
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                        
                    }
                    
                    
                }else{
                    self.cartItemArray.removeAll()
                    self.blankImgView.isHidden = false
                    self.bottomView.isHidden = true
                    self.bottomHeight.constant = 0.0
                    self.tableView.isHidden = true
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeColor = .baseOrange
                        tabItem.badgeValue = "0"
                    }
                    UserDefaults.standard.setValue(0, forKey: "cart_count")
                    UserDefaults.standard.setProviderId(providerId: 0)
                    UserDefaults.standard.setProviderName(name: "")
                    
                    
                    
                    
                    
                }
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
                
                
            }
            
            
        }
        
        
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    func updateCartAPi(amount:String,quantity:String,dishId:String,discount:String){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.updateCart)"
        
        /*roles_id=2&dish_id
         user_id&amount=44.00&quantity=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(dishId)","amount":"\(amount)","quantity":"\(quantity)","discount_price":discount]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            //                            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
//                    if quantity == "0"{
//                    NotificationCenter.default.post(name: NSNotification.Name("removeCartCount"), object: nil)
//                    }
                    self.cartItemList()
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    
    func getAllAddress()  {
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getAllAddressURL)"
        
        /*roles_id
         user_id*/
        
        
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
        
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    self.addressArray = swiftyVarJson["address"].arrayValue
                    
                    self.addAdressBtn.isHidden = true
                    self.circleView.isHidden = false
                    self.addressType.isHidden  = false
                    self.changeAddressBtn.isHidden = false
                    self.nameLabel.isHidden = false
                    
                    self.addressLabel.isHidden = false
                    self.setDataToAddressView()
                    
                }else{
                    
                    self.addAdressBtn.isHidden = false
                    self.circleView.isHidden = true
                    self.addressType.isHidden  = true
                    self.changeAddressBtn.isHidden = true
                    self.nameLabel.isHidden = true
                    
                    self.addressLabel.isHidden = true
                    
                    
                    
                    
                }
                
                
                
                
            }else{
                //                MBProgressHUD.showToast(message: "No Internet", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    func emptyCart(){
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.emptyCart)"
        
        /*roles_id
         user_id*/
        
        
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            //                MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
//                    NotificationCenter.default.post(name: NSNotification.Name("deleteCartCount"), object: nil)
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[2]
                        tabItem.badgeColor = .baseOrange
                        tabItem.badgeValue = "0"
                    }
                    UserDefaults.standard.setValue(0, forKey: "cart_count")
                    let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        self.cartItemList()
                    }
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                    
                }else{
                    
                    
                    
                }
                
                
                
                
            }else{
                //                MBProgressHUD.showToast(message: "No Internet", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    
}

extension CartVC : BTViewControllerPresentingDelegate{
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true, completion: nil)
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    
}


extension CartVC:BTAppSwitchDelegate{
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    
}
