//
//  AddPopupVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 16/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController


protocol quantityUpdateDelegate {
    func updateQuantity(count:Int,amount:Float,status:Bool)
}

class AddPopupVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblDishName: UILabel!
    var dishName = String()
    var descArray = JSON()
    var totalAmount = Float()
    var customizedData = [JSON]()
    var previousAmountLeft = Float()
    var isPlusOrMinus = Bool()
    
    var delegate : quantityUpdateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        
        tableView.delegate = self
        tableView.dataSource = self
        
        lblDishName.text = "\(descArray["name"].stringValue)"
        
        
//        totalAmount = descArray["price"].floatValue
//        for item in descArray["customization"].arrayValue{
//
//            totalAmount = totalAmount + item["prices"].floatValue
//
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getCustomizedList()
    }
    
    
    
    @IBAction func stepDownAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! PopupCell
        var count: Int = Int(cell.lblQty.text ?? "") ?? 0
        
        isPlusOrMinus = false
        
        let prevAmount = totalAmount / Float(count)
        
        
        if count > 1{
            count = count  - 1
            cell.lblQty.text = "\(count)"
        }else{
            count = 0
            previousAmountLeft = prevAmount
        }
        
        
        
        
        let amount = prevAmount * Float(count)
        let dishID = descArray["id"].stringValue
        
        cell.amountLabel.text = "$\(amount)"
        let discount = (amount * descArray["discount"].floatValue)/100
        updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
        
        
    }
    
    
    @IBAction func stepUpAction(_ sender: UIButton) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! PopupCell
        var count: Int = Int(cell.lblQty.text ?? "") ?? 0
        
        if count <= 99{
        let prevAmount = totalAmount / Float(count)
        
        isPlusOrMinus = true
        
        count = count + 1
        
        cell.lblQty.text = "\(count)"
        
        let amount = prevAmount * Float(count)
        let dishID = descArray["id"].stringValue
        
        cell.amountLabel.text = "$\(amount)"
        let discount = (amount * descArray["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
        }else{
            self.customAlert(title: "", message: "You can not order more than 99 Quantity", icon: "checkmark3")
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCell", for: indexPath) as! PopupCell
        if customizedData.count > 0{
//        cell.steperView.layer.masksToBounds = true
//        cell.steperView.layer.cornerRadius = 5.0//dishcell.steperView.frame.height/2
//        cell.steperView.layer.borderColor = UIColor.init(red: 255/255.0, green: 75/255.0, blue: 0/255.0, alpha: 1.0).cgColor
//        cell.steperView.layer.borderWidth = 2
        
        cell.lblDish.text = "\(descArray["name"].stringValue)"
        cell.lblQty.text = customizedData[indexPath.row]["quantity"].stringValue
        cell.amountLabel.text = "$\(customizedData[indexPath.row]["amount"].stringValue)"
        let str = NSMutableString()
        for item in customizedData[indexPath.row]["customization"].arrayValue{
            str.appendFormat("%@ - %@ - $%.2f\n",item["customisation"].stringValue,item["subtypes"].stringValue,item["prices"].floatValue)
            
        }
        
        cell.lblDesc.text = "\(str)"
        }
        
        return cell
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func getCustomizedList(){
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getCustomizedList)"
        
        /*roles_id=2&dish_id
         user_id&amount=44.00&quantity=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(descArray["id"].stringValue)"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.bgView)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.bgView, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.tableView.isHidden = false
                    self.customizedData = swiftyVarJson["data"].arrayValue
                    self.totalAmount = self.customizedData[0]["amount"].floatValue
                    self.tableView.reloadData()
                    
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func updateCartAPi(amount:String,quantity:String,dishId:String,discount:String){
        
        if Reachability.isConnectedToNetwork(){
            
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.updateCart)"
        
        /*roles_id=2&dish_id
         user_id&amount=44.00&quantity=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(dishId)","amount":"\(amount)","quantity":"\(quantity)","discount_price":"\(discount)"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.bgView)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.bgView, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.totalAmount = swiftyVarJson["data"]["amount"].floatValue
                    
                    if quantity == "0"{
                        NotificationCenter.default.post(name: NSNotification.Name("removeCartCount"), object: nil)
                        self.delegate?.updateQuantity(count: swiftyVarJson["data"]["quantity"].intValue, amount: self.previousAmountLeft , status: self.isPlusOrMinus)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                    
                    self.delegate?.updateQuantity(count: swiftyVarJson["data"]["quantity"].intValue, amount: swiftyVarJson["data"]["amount"].floatValue , status: self.isPlusOrMinus)
                    }
                    
                }else{
                    
                   self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
