//
//  DishListVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 04/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class DishListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,customizationDelegate,quantityUpdateDelegate,ReplaceCartDelegate {
    
    
    func didReplaceCart(isCustomizable: Int) {
        if isCustomizable == 0{
            var dict = self.dishArray[selectedIndexPath.row]
            dict["is_addedToCart"] = ""
            dict["is_addedToCart"] = "1"
            self.dishArray[selectedIndexPath.row] = dict
            self.getCartAmount = self.dishArray[selectedIndexPath.row]["price"].floatValue
            self.getCartCount = 1
            let discount = (self.getCartAmount * self.dishArray[selectedIndexPath.row]["discount"].floatValue)/100
            addItemToCartApi(dishId: self.dishArray[selectedIndexPath.row]["id"].stringValue, amount: self.dishArray[selectedIndexPath.row]["price"].stringValue, discount: "\(discount)")
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
            vc.restaurantId = self.providerId
            vc.discountValue = self.dishArray[selectedIndexPath.row]["discount"].doubleValue
            vc.productId = self.dishArray[selectedIndexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.delegate = self
//            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func updateQuantity(count: Int,amount:Float,status:Bool) {
        
        let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
        if count == 0{
            
            var dict = self.dishArray[selectedIndexPath.row]
            dict["is_addedToCart"] = ""
            dict["is_addedToCart"] = "0"
            self.dishArray[selectedIndexPath.row] = dict
            cell.addBtn.isHidden = false
            cell.steperView.isHidden = true
            self.getCartCount = self.getCartCount - 1
            self.getCartAmount = self.getCartAmount - amount
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: self.getCartAmount)
            
        }else{
            
            if status{
                self.getCartAmount = self.getCartAmount + amount/Float(count)
                
            }else{
                self.getCartAmount = self.getCartAmount - amount/Float(count)
            }
            
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: self.getCartAmount)
            cell.countLabel.text = "\(count)"
            
            let floatCount = self.dishArray[selectedIndexPath.row]["quantity_added"].floatValue
            cell.lblAmount.text = String(format: "$%.2f",((self.dishArray[selectedIndexPath.row]["total_price"].floatValue/floatCount) * Float(count)))
           
            
           
        }
    }
    
    
    
    func updateCell(isAdded: Bool, amount:Float) {
        let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
        
        if isAdded{
            cell.addBtn.isHidden = true
            cell.steperView.isHidden = false
            self.getCartCount = self.getCartCount + 1
            self.getCartAmount = self.getCartAmount + amount
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: getCartAmount)
        }else{
            cell.addBtn.isHidden = false
            cell.steperView.isHidden = true
        }
    }
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var dishId = String()
    var providerId = String()
    var dishArray = [JSON]()
    var titleStr = String()
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cartStatusView: UIView!
    @IBOutlet weak var itemCountLabel: UILabel!
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    var selectedIndexPath = IndexPath()
    
    @IBOutlet weak var blankImgView: UIImageView!
    
    @IBOutlet weak var cartViewHeight: NSLayoutConstraint!
    var getCartCount = Int()
    var getCartAmount = Float()
    var restaurantName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        
        tableView.delegate = self
        tableView.dataSource = self
        
        bgView.layer.masksToBounds = true
        bgView.layer.cornerRadius = 5.0
        bgView.layer.shadowColor = UIColor.gray.cgColor
        bgView.layer.shadowOpacity = 0.3
        bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
        bgView.layer.shadowRadius = 6
        
        titleLabel.text = titleStr
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadPage), name: NSNotification.Name("reloadPage"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getMenuDishes()
    }
    
    @objc func reloadPage(){
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getMenuDishes()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateBottomView(getCount : String ,getPrice : Float){
        
        
        if getCount == "0"{
            cartStatusView.isHidden = true
            cartViewHeight.constant = 0
            UserDefaults.standard.setProviderId(providerId: 0)
            UserDefaults.standard.setProviderName(name: "")
        }else{
            cartStatusView.isHidden = false
            cartViewHeight.constant = 80
        }
        totalAmountLabel.text = String(format: "Total Amount : $ %.2f", getPrice)
        itemCountLabel.text = "\(getCount) Item(s)"
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dishArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as! MenuListCell
        
        blankImgView.isHidden = true
        
        cell.bgView.layer.shadowColor = UIColor.gray.cgColor
        cell.bgView.layer.shadowOpacity = 0.3
        cell.bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.bgView.layer.shadowRadius = 6
        cell.bgView.layer.cornerRadius = 10
        
        cell.storeImgView.layer.masksToBounds = true
        cell.storeImgView.layer.cornerRadius = 5
        
        let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(self.dishArray[indexPath.row]["img"].stringValue)")
        
        cell.storeImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        cell.lblStore.text = self.dishArray[indexPath.row]["name"].stringValue
        
        let description = self.dishArray[indexPath.row]["description"].stringValue
        cell.lblDescription.text = description.htmlToString
        
        
        
        let rating = String(format: "%.1f", self.dishArray[indexPath.row]["avg_rating"].floatValue)
        
        cell.ratingLabel.text = rating
        
        if let rate = Double("\(rating)"){
            cell.ratingView.rating = rate
        }
        
        
//        cell.steperView.layer.masksToBounds = true
//        cell.steperView.layer.cornerRadius = 5.0//dishcell.steperView.frame.height/2
//        cell.steperView.layer.borderColor = UIColor.init(red: 255/255.0, green: 75/255.0, blue: 0/255.0, alpha: 1.0).cgColor
//        cell.steperView.layer.borderWidth = 2
        
//        cell.addBtn.layer.masksToBounds = true
//        cell.addBtn.layer.cornerRadius = 5.0//dishcell.addBtn.frame.height/2
//        cell.addBtn.layer.borderColor = UIColor.init(red: 255/255.0, green: 75/255.0, blue: 0/255.0, alpha: 1.0).cgColor
//        cell.addBtn.layer.borderWidth = 2
        
        if self.dishArray[indexPath.row]["is_addedToCart"].intValue == 0{
            cell.addBtn.isHidden = false
            cell.steperView.isHidden = true
            cell.countLabel.text = "1"
            cell.lblAmount.text = String(format: "$%@",self.dishArray[indexPath.row]["price"].stringValue)
        }else{
            cell.addBtn.isHidden = true
            cell.steperView.isHidden = false
            cell.countLabel.text = self.dishArray[indexPath.row]["quantity_added"].stringValue
            cell.lblAmount.text = String(format: "$%@",self.dishArray[indexPath.row]["total_price"].stringValue)
        }
        
        if dishArray[indexPath.row]["discount"].floatValue == 0.0{
            cell.dishFlameImgView.isHidden = true
            cell.discount_label.isHidden = true
            cell.dishFlameHeight.constant = 0
           
        }else{
            cell.dishFlameImgView.isHidden = false
            cell.dishFlameHeight.constant = 40
            cell.discount_label.isHidden = false
            cell.discount_label.text = String(format: "%.1f%@", dishArray[indexPath.row]["discount"].floatValue,"% OFF")
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if self.dishArray[indexPath.row]["is_addedToCart"].intValue == 0{
//        
//        addToCart(indexPath: indexPath)
//        }
        
    }
    
    @IBAction func addButtonAction(_ sender: Any){
        
//        if UserDefaults.standard.getUserId() == ""{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
//            vc.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(vc, animated: true)
//        }else{
            
            let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
            let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
            
            
            self.selectedIndexPath = indexPath
            
             if UserDefaults.standard.getProviderId() == Int(self.providerId) ||  UserDefaults.standard.getProviderId() == 0{
            
            let isCustomizable = self.dishArray[indexPath.row]["is_customisable"].stringValue
            
            if isCustomizable == "0"{
                
                
                self.getCartCount = self.getCartCount + 1
                self.getCartAmount = self.getCartAmount + self.dishArray[indexPath.row]["price"].floatValue
                //                updateBottomView(getCount: "\(getCartCount)", getPrice: self.getCartAmount)
                
                var dict = self.dishArray[indexPath.row]
                dict["is_addedToCart"] = ""
                dict["is_addedToCart"] = "1"
                self.dishArray[indexPath.row] = dict
               let discount = (self.getCartAmount * self.dishArray[indexPath.row]["discount"].floatValue)/100
                addItemToCartApi(dishId: self.dishArray[indexPath.row]["id"].stringValue, amount: self.dishArray[indexPath.row]["price"].stringValue, discount: "\(discount)")
              
                
            }else{
                
                
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
                    vc.restaurantId = self.dishArray[indexPath.row]["added_by"].stringValue
                    vc.productId = self.dishArray[indexPath.row]["id"].stringValue
//                    vc.modalPresentationStyle = .overCurrentContext
//                    vc.delegate = self
                vc.providerName = self.restaurantName
                vc.discountValue = self.dishArray[indexPath.row]["discount"].doubleValue
                self.navigationController?.pushViewController(vc, animated: true)
//                    self.present(vc, animated: true, completion: nil)
               
            }
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReplacePopupVC") as! ReplacePopupVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.currentProviderName = self.restaurantName
                vc.delegate = self
                vc.isCustomization = self.dishArray[selectedIndexPath.row]["is_customisable"].intValue
                 self.present(vc, animated: true, completion: nil)
                
            }
            
//        }
    }
    
    
    func addToCart(indexPath:IndexPath){
        
        
        
        if UserDefaults.standard.getUserId() == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
//            self.customAlert(title: "", message: "Please login first!", icon: "checkmark3")
        }else{
            
//            let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
//            let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
//
//
            self.selectedIndexPath = indexPath
            
             if UserDefaults.standard.getProviderId() == Int(self.providerId) ||  UserDefaults.standard.getProviderId() == 0{
            
            let isCustomizable = self.dishArray[indexPath.row]["is_customisable"].stringValue
            
            if isCustomizable == "0"{
                
                
                self.getCartCount = self.getCartCount + 1
                self.getCartAmount = self.getCartAmount + self.dishArray[indexPath.row]["price"].floatValue
                //                updateBottomView(getCount: "\(getCartCount)", getPrice: self.getCartAmount)
                
                var dict = self.dishArray[indexPath.row]
                dict["is_addedToCart"] = ""
                dict["is_addedToCart"] = "1"
                self.dishArray[indexPath.row] = dict
                
                let discount = (self.getCartAmount * self.dishArray[indexPath.row]["discount"].floatValue)/100
                addItemToCartApi(dishId: self.dishArray[indexPath.row]["id"].stringValue, amount: self.dishArray[indexPath.row]["price"].stringValue, discount: "\(discount)")
              
                
            }else{
                
                
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
                    vc.restaurantId = self.dishArray[indexPath.row]["added_by"].stringValue
                    vc.productId = self.dishArray[indexPath.row]["id"].stringValue
//                    vc.modalPresentationStyle = .overCurrentContext
//                    vc.delegate = self
                vc.providerName = self.restaurantName
                vc.discountValue = self.dishArray[indexPath.row]["discount"].doubleValue
                self.navigationController?.pushViewController(vc, animated: true)
//                    self.present(vc, animated: true, completion: nil)
               
            }
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReplacePopupVC") as! ReplacePopupVC
                vc.modalPresentationStyle = .overCurrentContext
                vc.currentProviderName = self.restaurantName
                vc.delegate = self
                vc.isCustomization = self.dishArray[selectedIndexPath.row]["is_customisable"].intValue
                 self.present(vc, animated: true, completion: nil)
                
            }
            
        }
    }
    
    
    
    @IBAction func stepDownAction(_ sender: Any) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! MenuListCell
        var count: Int = Int(cell.countLabel.text ?? "") ?? 0
        
        self.selectedIndexPath = indexPath!
        
        let isCustomizable = self.dishArray[indexPath!.row]["is_customisable"].stringValue
        
        if isCustomizable == "0"{
            
            let prevAmount = dishArray[indexPath!.row]["price"].floatValue // Float(count)
            
            if count > 1{
                count = count  - 1
                cell.countLabel.text = "\(count)"
                
                self.getCartAmount = self.getCartAmount - prevAmount
            }else{
                count = 0
                cell.addBtn.isHidden = false
                cell.steperView.isHidden = true
                self.getCartCount = self.getCartCount - 1
                self.getCartAmount = self.getCartAmount - prevAmount
            }
            
            
            
            
            let amount = prevAmount * Float(count)
            let dishID = dishArray[indexPath!.row]["id"].stringValue
            
            //            self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
            
            if count == 0{
                var dict = self.dishArray[indexPath!.row]
                dict["is_addedToCart"] = ""
                dict["is_addedToCart"] = "0"
                self.dishArray[indexPath!.row] = dict
            }
            let discount = (amount * dishArray[indexPath!.row]["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
            
            
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPopupVC") as! AddPopupVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.delegate = self
            vc.descArray = self.dishArray[indexPath!.row]
            self.present(vc, animated: true, completion: nil)
            
        }
        
        
        
        
    }
    
    @IBAction func stepUpAction(_ sender: Any) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! MenuListCell
        var count: Int = Int(cell.countLabel.text ?? "") ?? 0
        
        if count <= 99{
        
        self.selectedIndexPath = indexPath!
        
        let isCustomizable = self.dishArray[indexPath!.row]["is_customisable"].stringValue
        
        if isCustomizable == "0"{
            let prevAmount = dishArray[indexPath!.row]["price"].floatValue // Float(count)
            count = count + 1
            
            cell.countLabel.text = "\(count)"
            
            let amount = prevAmount * Float(count)
            let dishID = dishArray[indexPath!.row]["id"].stringValue
            
            self.getCartAmount = self.getCartAmount + prevAmount
            
            //            self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
            let discount = (amount * dishArray[indexPath!.row]["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
            
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPopupVC") as! AddPopupVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.descArray = self.dishArray[indexPath!.row]
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            
        }
        }  else{
                self.customAlert(title: "", message: "You can not order more than 99 Quantity", icon: "checkmark3")
            }
        
        
    }
    
    @IBAction func placeOrderAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderVC") as! PlaceOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func viewCartBtnAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        self.navigationController?.pushViewController(vc, animated: true)
        tabBarController?.selectedIndex = 2
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func getMenuDishes(){
         if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getMenuDishesURL)"
        
        /*roles_id
         menu_id
         provider_id*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","provider_id":"\(providerId)","menu_id":"\(dishId)","user_id":"\(userId)"]
        print(params)
        //        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.dishArray = swiftyVarJson["dishes"].arrayValue
                    self.getCartCount = swiftyVarJson["cart_counts"].intValue
                    self.getCartAmount = swiftyVarJson["cart_price_counts"].floatValue
                    
                    self.updateBottomView(getCount: swiftyVarJson["cart_counts"].stringValue, getPrice: swiftyVarJson["cart_price_counts"].floatValue)
                    
                    
                    
                    self.tableView.reloadData()
                    
                }else{
                    
                    self.blankImgView.isHidden = false
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    func
        addItemToCartApi(dishId:String,amount:String,discount:String)  {
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.addToCart)"
        
        /*roles_id
         user_id
         dish_id
         quantity
         amount
         provider_id*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","amount":"\(amount)","quantity":"1","provider_id":"\(self.providerId)","dish_id":"\(dishId)","is_customizable":"0","discount_price":"\(discount)"]
        //        let params :[String:Any] = ["roles_id":"2","provider_id":"62","dish_id":"4"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    NotificationCenter.default.post(name: NSNotification.Name("updateCartCount"), object: nil)
                    let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
                    cell.addBtn.isHidden = true
                    cell.steperView.isHidden = false
                    
                    UserDefaults.standard.setProviderId(providerId:Int( self.providerId)!)
                    
                    UserDefaults.standard.setProviderName(name: self.restaurantName)
                    self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
                    
                    
                    let alert = self.createAlert(title: "", message: "Added to shopping cart.", icon: "cart-1")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
//                        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
//                        self.getMenuDishes()
                        self.dismiss(animated: true, completion: nil)
                    }

                    
                    alert.addAction(okAction)

                    self.present(alert, animated: true, completion: nil)
//
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func updateCartAPi(amount:String,quantity:String,dishId:String,discount:String){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.updateCart)"
        
        /*roles_id=2&dish_id
         user_id&amount=44.00&quantity=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(dishId)","amount":"\(amount)","quantity":"\(quantity)","discount_price":"\(discount)"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    if quantity == "0"{
                    NotificationCenter.default.post(name: NSNotification.Name("removeCartCount"), object: nil)
                    }
                    self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
                    
                }else{
                    
                   self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
