//
//  NewTrackOrderVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 12/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController


class NewTrackOrderVC: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var lblArrivalTime: UILabel!
    @IBOutlet weak var btnPlaced: UIButton!
    @IBOutlet weak var lblPlacedStatus: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblConfirmStatus: UILabel!
    @IBOutlet weak var btnProcessing: UIButton!
    @IBOutlet weak var lblProcessingStatus: UILabel!
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblConfirm: UILabel!
    @IBOutlet weak var lblProcess: UILabel!
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var btnDelivered: UIButton!
    @IBOutlet weak var lblDeliveredStatus: UILabel!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblVehicle: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnZoom: UIButton!
    @IBOutlet weak var viewLayoutConstraints: NSLayoutConstraint!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var pathLine1: UILabel!
    @IBOutlet weak var pathLine3: UILabel!
    @IBOutlet weak var pathLine2: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var profileViewHeight: NSLayoutConstraint!
    
    var googleMapApiKey = "AIzaSyDJl11mAojAFGlCjDrzrZx1LhXozqtA78Y"
    var locationManager = CLLocationManager()
    var isZoom:Bool = true
    var polyLine = GMSPolyline()
    let carMovement = SKCarMovement()
    var mymarker = GMSMarker()
    var sourceMarker = GMSMarker()
    var destinationMarker = GMSMarker()
    var trackData = JSON()
    var driverData = JSON()
    var pathh = GMSPath()
    
    var destinationLocation = CLLocationCoordinate2D()
    var sourceLocation = CLLocationCoordinate2D()
    var orderId = String()
    var driverContact = String()
    var timer = Timer()
    var isDrawPolyline : Bool = false
    var oldCoordinate: CLLocationCoordinate2D?
    var newCoordinate: CLLocationCoordinate2D?
    var isFirsttime = false
    var isNormalFirsttime = false
    var cameraZoom : Float = 0.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carMovement.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getOrderStatusFirst()
        
        timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(getOrderStatus), userInfo: nil, repeats: true)

        // Do any  setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        mapView.delegate = self
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer.invalidate()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func zoomButtonAction(_ sender: Any) {
        if isZoom{
            isZoom = false
            viewLayoutConstraints.constant = 0
            profileImgView.isHidden = true
            profileViewHeight.constant = 0
                
        }else{
            isZoom = true
            if trackData["order_status"].intValue > 2{
                viewLayoutConstraints.constant = 300.00
                profileImgView.isHidden = false
                profileViewHeight.constant = 80
            }else{
            viewLayoutConstraints.constant = 220.00
            }
        }
    }
    
    func getOrderStatusFirst() {
        if Reachability.isConnectedToNetwork(){
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.trackOrder)"
            let userId = UserDefaults.standard.getUserId()
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_id":"\(orderId)"]
            print(params)
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if  response.result.value != nil{
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        self.trackData = swiftyVarJson["order_data"]
                        self.driverData = swiftyVarJson["data"]
                        self.backView.isHidden = false
                        self.setDataOnView()
                    }
                    else{
                    }
                } else{
                }
            }
        }
        else{
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        }
    }
    
    @objc func getOrderStatus() {
        if Reachability.isConnectedToNetwork() {
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.trackOrder)"
            let userId = UserDefaults.standard.getUserId()
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_id":"\(orderId)"]
            print(params)
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                if  response.result.value != nil{
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        self.trackData = swiftyVarJson["order_data"]
                        self.driverData = swiftyVarJson["data"]
                        self.backView.isHidden = false
                        self.setDataOnView()
                    }
                    else{
                    }
                } else{
                }
            }
        }
        else{
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        }
    }
    
    func setDataOnView(){
        loadMapView()
        setPlaced()
        if trackData["order_status"].intValue == 1{
            
        }
        else if trackData["order_status"].intValue == 2{
            //            setPlaced()
            setAccepted()
        }
        else if trackData["order_status"].intValue == 3{
            //            setPlaced()
            setAccepted()
            setOutForDelivery()
        }
        else{
            //            setPlaced()
            setAccepted()
            setOutForDelivery()
            setDelivered()
        }
    }
    
    func loadMapView() {
        //        between Provider & User
        if trackData["order_status"].intValue == 1  || trackData["order_status"].intValue == 2 {
            if isNormalFirsttime == false {
                isNormalFirsttime = true
                destinationLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees(trackData["provider_lat"].doubleValue),longitude: CLLocationDegrees(trackData["provider_lng"].doubleValue))
                sourceLocation = CLLocationCoordinate2DMake(trackData["order_lat"].doubleValue, trackData["order_lng"].doubleValue)
                self.showRoutes(source: self.destinationLocation , destination:self.sourceLocation )
                
            }
            
            print("New_Destination -> \(destinationLocation)")
            print("New_Source -> \(sourceLocation)")
            
            lblArrivalTime.isHidden = true
            sourceMarker.position = CLLocationCoordinate2DMake(sourceLocation.latitude, sourceLocation.longitude)
            sourceMarker.icon = #imageLiteral(resourceName: "source")
            sourceMarker.map = mapView
            
            destinationMarker.position = CLLocationCoordinate2DMake(destinationLocation.latitude, destinationLocation.longitude)
            destinationMarker.icon = #imageLiteral(resourceName: "dest")
            destinationMarker.map = mapView
            
            let positionD = CLLocationCoordinate2DMake(CLLocationDegrees(destinationLocation.latitude), CLLocationDegrees(destinationLocation.longitude))
            let positionS = CLLocationCoordinate2DMake(CLLocationDegrees(sourceLocation.latitude), CLLocationDegrees(sourceLocation.longitude))
            
            let bounds = GMSCoordinateBounds(coordinate: positionS, coordinate: positionD)
            let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: UIScreen.main.bounds.height/6, left: 80, bottom: UIScreen.main.bounds.height/6, right: 80))
            self.mapView.animate(with: update)
        }
        if trackData["order_status"].intValue == 3 {
            
            //            destination = Driver
            destinationLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees(driverData["cur_lat"].doubleValue),longitude: CLLocationDegrees(driverData["cur_lng"].doubleValue))
            
            if isFirsttime == false {
                print("First Time")
                destinationMarker.icon = nil
                polyLine.map = nil
                
                //                oldcoordinate = Driver
                self.oldCoordinate = CLLocationCoordinate2DMake(CLLocationDegrees(destinationLocation.latitude), CLLocationDegrees(destinationLocation.longitude))
                //                sourceLocation = OrderLocation
                self.sourceLocation = CLLocationCoordinate2DMake(trackData["order_lat"].doubleValue, trackData["order_lng"].doubleValue)
                
                //                Source = User
                sourceMarker.position = CLLocationCoordinate2DMake(sourceLocation.latitude, sourceLocation.longitude)
                sourceMarker.icon = #imageLiteral(resourceName: "source")
                sourceMarker.map = mapView
                
                isFirsttime = true
                //                MyMarker = Driver
                mymarker.position = CLLocationCoordinate2DMake(destinationLocation.latitude, destinationLocation.longitude)
                mymarker.icon = #imageLiteral(resourceName: "delivery")
                mymarker.map = mapView
                
                mapView.camera = GMSCameraPosition(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude, zoom: 17.0)
                
                self.showRoutes(source:self.oldCoordinate!  , destination:self.sourceLocation )
            }
            else {
                print("Not First Time")
//                lblArrivalTime.isHidden = false
                self.newCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(destinationLocation.latitude),longitude: CLLocationDegrees(destinationLocation.longitude))
                
                self.carMovement.SKCarMovement(marker: self.mymarker, oldCoordinate: self.oldCoordinate!, newCoordinate: self.newCoordinate!, mapView: self.mapView, bearing: 0)
                self.oldCoordinate = self.newCoordinate
                                
                mapView.camera = GMSCameraPosition(latitude: destinationLocation.latitude, longitude: destinationLocation.longitude, zoom: 17.0)
                
                self.showRoutes(source:self.newCoordinate!  , destination:self.sourceLocation )
            }
            lblArrivalTime.isHidden = false
//            lblArrivalTime.text = String(format: "Estimated time for delivery   %d minutes", calculateDistance(storeLat: destinationLocation.latitude, storeLong: destinationLocation.longitude))
        }
    }
    
    func convertDate (dateStr:String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date : Date = dateFormatterGet.date(from: dateStr)!
        let orderDate = Date.getFormattedDate(date: date, format: "EEEE, dd/MM/yyy HH:mm a")
        return orderDate
    }
    
    func calculateDistance(storeLat: Double,storeLong:Double)->Int{
        let myLocation = CLLocation(latitude: sourceLocation.latitude, longitude: sourceLocation.longitude)
        let storeLocation = CLLocation(latitude: CLLocationDegrees(storeLat), longitude: CLLocationDegrees(storeLong))
        let distance = myLocation.distance(from: storeLocation) / 1000
        let time = Int(distance*60/30)
        return time
    }
    
    func setPlaced(){
        btnPlaced.isSelected = true
        lblOrder.textColor = .baseOrange
        lblPlacedStatus.text = convertDate(dateStr: trackData["created_at"].stringValue)
        pathLine1.backgroundColor = .baseOrange
    }
    
    func setAccepted(){
        btnConfirm.isSelected = true
        lblConfirm.textColor = .baseOrange
        lblConfirmStatus.text = convertDate(dateStr:trackData["accepted_date"].stringValue)
        pathLine2.backgroundColor = .baseOrange
    }
    
    func setOutForDelivery(){
        btnProcessing.isSelected = true
        lblProcess.textColor = .baseOrange
        lblProcessingStatus.text = convertDate(dateStr:trackData["out_for_delivery_date"].stringValue)
        pathLine3.backgroundColor = .baseOrange
        profileView.isHidden = false
        profileViewHeight.constant = 80
        lblClientName.text = driverData["name"].stringValue
        lblVehicle.text = driverData["vehicle_number"].stringValue
        driverContact = driverData["mobile"].stringValue
        
        profileImgView.layer.masksToBounds = true
        profileImgView.layer.cornerRadius = profileImgView.frame.height/2
        
        let imagename = "\(GlobalURL.imagebaseURL)\(driverData["user_image"].stringValue)"
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let imgURL = URL(string: urlString )
        profileImgView.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        profileImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "profile-3"), completed: nil)
    }
    func setDelivered(){
        btnDelivered.isSelected = true
        lblDelivered.textColor = .baseOrange
        lblDeliveredStatus.text = convertDate(dateStr:trackData["delivered_at"].stringValue)
    }
    
    func showRoutes(source : CLLocationCoordinate2D , destination: CLLocationCoordinate2D) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let source_String = "\(source.latitude),\(source.longitude)"
        let dest_String = "\(destination.latitude),\(destination.longitude)"
//        polyLine.map
        let url =  URL(string:"https://maps.googleapis.com/maps/api/directions/json?origin=\(source_String)&destination=\(dest_String)&sensor=false&mode=driving&key=\(googleMapApiKey)")!
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
            }
            else{
                do {
                    let dictonary:Dictionary<String, Any> = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                    DispatchQueue.main.async {
                        
                        self.drawRoute(routeDict: dictonary)
                    }
                } catch {
                    NSLog("One or more of the map styles failed to load. \(error)")
                }
                
            }
        })
        task.resume()
    }
    func drawRoute(routeDict: Dictionary<String, Any>) {
        let routesArray = routeDict["routes"] as! NSArray
        if (routesArray.count > 0) {
            self.isDrawPolyline = true
            let routeDict = routesArray[0] as! Dictionary<String, Any>
            let routeOverviewPolyline = routeDict["overview_polyline"] as! Dictionary<String, Any>
            let points = routeOverviewPolyline["points"]
            let timeArray = routeDict["legs"] as! NSArray
            let timeDict = timeArray[0] as! Dictionary<String, Any>
            let distDict = timeDict["duration"] as! Dictionary<String, Any>
            let time = distDict["text"] as! String
            
            lblArrivalTime.text = String(format: "Estimated time for delivery   %@", time)
            polyLine.map = nil
            pathh = GMSPath.init(fromEncodedPath: points as! String)!
            polyLine = GMSPolyline(path: pathh)
            polyLine.strokeWidth = 4.0
            polyLine.strokeColor = .blue
            polyLine.map = mapView
        }
    }
}

extension NewTrackOrderVC : SKCarMovementDelegate {
    func SKCarMovementMoved(_ Marker: GMSMarker) {
        mymarker = Marker
    }
}
