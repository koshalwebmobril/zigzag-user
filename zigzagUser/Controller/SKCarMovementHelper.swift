//
//  SKCarMovementHelper.swift
//  CIAL Rider
//
//  Created by Alivenet Solution on 07/01/19.
//  Copyright © 2019 COCHIN INTERNATIONAL AIRPORT LTD. All rights reserved.
//

import UIKit

import GoogleMaps

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

// MARK: - delegate protocol
@objc public protocol SKCarMovementDelegate {
    
    /**
     *  Tells the delegate that the specified marker will be move with animation.
     */
    func SKCarMovementMoved(_ Marker: GMSMarker)
}

@objcMembers public class SKCarMovement: NSObject {
    
    // MARK: Public properties
    public weak var delegate: SKCarMovementDelegate?
    public var duration: Float = 3.0
    
    public func SKCarMovement(marker: GMSMarker, oldCoordinate: CLLocationCoordinate2D, newCoordinate:CLLocationCoordinate2D, mapView: GMSMapView, bearing: Float) {
        
        let coordinate₀ = CLLocation(latitude: oldCoordinate.latitude, longitude: oldCoordinate.longitude)
        let coordinate₁ = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)

        let distanceInMeters = coordinate₀.distance(from: coordinate₁) // result is in meters

        if distanceInMeters < 1 {
            return
        }
        
        //calculate the bearing value from old and new coordinates
        //
        let calBearing: Float = getHeadingForDirection(fromCoordinate: oldCoordinate, toCoordinate: newCoordinate)
        marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        marker.rotation = CLLocationDegrees(calBearing); //found bearing value by calculation when marker add
        marker.position = oldCoordinate; //this can be old position to make car movement to new position
        
        //marker movement animation
        CATransaction.begin()
        CATransaction.setValue(duration, forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            marker.rotation = (Int(bearing) != 0) ? CLLocationDegrees(bearing) : CLLocationDegrees(calBearing)
        })
        
        // delegate method pass value
        //
        delegate?.SKCarMovementMoved(marker)
        
        marker.position = newCoordinate; //this can be new position after car moved from old position to new position with animation
        marker.map = mapView;
        marker.rotation = CLLocationDegrees(calBearing);
        CATransaction.commit()
    }
    
    private func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        return (degree >= 0) ? degree : (360 + degree)
    }
    
    func animateToNextCoord(marker: GMSMarker, oldCoordinate: CLLocationCoordinate2D, newCoordinate:CLLocationCoordinate2D) -> Void {
        
        let heading = GMSGeometryHeading(oldCoordinate, newCoordinate)
        let distance = GMSGeometryDistance(oldCoordinate, newCoordinate)
        
        CATransaction.begin()
        CATransaction.setValue((distance / (50 * 1000)), forKey: kCATransactionAnimationDuration)
        marker.position = newCoordinate
        CATransaction.commit()
        if marker.isFlat{
            marker.rotation = heading
        }
    }
}


