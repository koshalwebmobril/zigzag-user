//
//  Welcome1VC.swift
//  zigzagUser
//
//  Created by Webmobril on 02/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class Welcome1VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.patternColor
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func skip(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func next(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "Welcome2VC") as! Welcome2VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
