//
//  WebviewVc.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 26/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import WebKit
import JHTAlertController

class WebviewVc: UIViewController,WKUIDelegate,WKNavigationDelegate  {
    
    var pageId = String()

    @IBOutlet weak var titlaLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor

        let link = "\(GlobalURL.baseURL)page?page_id=\(pageId)"
        
        if pageId == "1"{
            
            self.titlaLabel.text = "Terms and Services"
            
        }else if pageId == "2"{
            self.titlaLabel.text = "About Us"
        }else{
            self.titlaLabel.text = "Terms and Services"
        }
        
        let url = URL(string: link)
        webView.navigationDelegate = self
        var urlrequest = URLRequest(url: url!)
        urlrequest.httpShouldHandleCookies = true
        webView.load(urlrequest)
        
    }
    

     func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
        }
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
        }
        
        @IBAction func backButtonAction(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
            
    
        }

}
