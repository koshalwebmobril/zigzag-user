//
//  AllShopList.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 24/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import JHTAlertController

class AllShopList: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    var favoriteArrray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getFavoriteList()
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteArrray.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavCell", for: indexPath) as! TableViewCell
        
        
        
        cell.nearbyview.layer.shadowColor = UIColor.gray.cgColor
        cell.nearbyview.layer.shadowOpacity = 0.3
        cell.nearbyview.layer.shadowOffset = CGSize(width: 2, height: 2)
        cell.nearbyview.layer.shadowRadius = 6
        cell.nearbyview.layer.cornerRadius = 10
        
        cell.nearbydistance.layer.cornerRadius = 10
        cell.nearbydistance.layer.masksToBounds = true
        
        let logoURL = URL(string:"\(GlobalURL.imagebaseURL)\(favoriteArrray[indexPath.row]["user_image"].stringValue)")
        
        cell.nearbyimg?.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "burger"),  completed: nil)
        
        cell.nearbyname.text = favoriteArrray[indexPath.row]["name"].stringValue
        cell.ratingLabel.text = favoriteArrray[indexPath.row]["address"].stringValue
        
        cell.ratingView.rating = favoriteArrray[indexPath.row]["avg_rating_provider"].doubleValue
        cell.nearbydistance.text =  String(format: "%.1f km", favoriteArrray[indexPath.row]["distance"].floatValue)
        
        cell.nearbyprice.text = String(format: "$ %.2f", favoriteArrray[indexPath.row]["starting_at"].floatValue)
        cell.removeBtn.layer.masksToBounds = true
        cell.removeBtn.layer.cornerRadius = 5.0
        
        if favoriteArrray[indexPath.row]["favourite"].intValue == 1{
            cell.removeBtn.setTitle("REMOVE", for: .normal)
        }else{
            cell.removeBtn.setTitle("ADD", for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        VC.restaurantId = self.favoriteArrray[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    
    @IBAction func addBtnAction(_ sender: UIButton) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        
        let providerId = self.favoriteArrray[indexPath.row]["id"].stringValue
        
        if sender.currentTitle == "ADD"{
            
            let alert = self.createAlert(title: "", message:"Do you want to add in favourite?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {
                _ in
                
                self.addFavoriteMethod(providerId: providerId)
                
                
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            
           let alert = self.createAlert(title: "", message:"Do you want to remove from favourites?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {
                _ in
                
                self.removeFavoriteMethod(providerId: providerId)
                
                
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
    }
    
    func getFavoriteList(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getAllStores)"
        
        let userId = UserDefaults.standard.getUserId()
        
        let lat = UserDefaults.standard.value(forKey: "lat") ?? ""
        let long = UserDefaults.standard.value(forKey: "long") ?? ""
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","lattitude":"\(lat)","longitude":"\(long)"]
        print(params)
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.tableView.isHidden = false
                    
                    self.favoriteArrray = swiftyVarJson["data"].arrayValue
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    
                    
                }else{
                    
                    
                    self.tableView.isHidden = true
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    
    func addFavoriteMethod(providerId : String){
        if Reachability.isConnectedToNetwork(){
            
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.makeFavoriteURL)"
        
        /*roles_id
         user_id
         provider_id
         */
        
        let userId = UserDefaults.standard.getUserId()
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(providerId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            //            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    let alert = self.createAlert(title: "", message: "Added to favourites", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                        
                        self.getFavoriteList()
                    }
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                    
                    
                }else{
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let alert = self.createAlert(title: "", message: "This store is already in your favourite list.", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                        
                        
                    }
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }
                
                
                
                
            }else{
                
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
     func removeFavoriteMethod(providerId : String){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.removeFavorite)"
            
            /*roles_id
            user_id
            provider_id
            */
            
            let userId = UserDefaults.standard.getUserId()
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(providerId)"]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
    //            MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        
                        let alert = self.createAlert(title: "", message: "Removed from favorites.", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "OK", style: .default) {
                            _ in self.getFavoriteList()}
                        
                        
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                        
                        
                    }else{
                        
//                        self.blankImgView.isHidden = false

                    }
                    
                    
                    
                    
                }else{
                
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        }
}
