//
//  OrderDetailVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 25/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController

class OrderDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    var detailArray = [JSON]()
    
    @IBOutlet weak var tableView: UITableView!
    
    var productDetails = JSON()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.patternColor
        
        //        tableView.estimatedRowHeight = 40
        //        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.layer.masksToBounds = true
        tableView.layer.cornerRadius = 10
        tableView.layer.borderColor = UIColor.black.cgColor
        tableView.layer.borderWidth = 1.5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetails["order_detail"].arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OrderDetailCell
        
        cell.productLabel.text = productDetails["order_detail"][indexPath.row]["product_name"].stringValue
        cell.unitLabel.text = productDetails["order_detail"][indexPath.row]["quantity"].stringValue
        cell.priceLabel.text = "$\(productDetails["order_detail"][indexPath.row]["order_price"].stringValue)"
        var subTypes = String()
        for item in productDetails["order_detail"][indexPath.row]["order_customisations"].arrayValue{
            subTypes.append(String(format: "%@ - %@ - $%.2f \n",  item["customisation"].stringValue,item["subtypes"].stringValue,item["prices"].floatValue))
        }
        cell.customization_label.text = subTypes
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Header") as! OrderDetailCell
        
        cell.logoImgView.layer.masksToBounds = true
        cell.logoImgView.layer.cornerRadius = cell.logoImgView.frame.height/2
        
        let imgURL:URL = URL(string: "\(GlobalURL.imagebaseURL)\(self.productDetails["provider"]["user_image"].stringValue)")!
        
        cell.logoImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        cell.providerLabel.text = self.productDetails["provider"]["name"].stringValue
        cell.orderNumber.text = "Order Number - \(self.productDetails["order_number"].stringValue)"
        let dateStr = self.productDetails["created_at"].stringValue
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let date : Date = dateFormatterGet.date(from: dateStr)!
        
        let orderDate = Date.getFormattedDate(date: date, format: "MMM d, yyyy h:mm a")
        cell.dateLabel.text = "Time : \(orderDate)"
        cell.addressLabel.text = productDetails["billing_address"].stringValue + " " + productDetails["billing_city"].stringValue + " " + productDetails["billing_state"].stringValue + " " + productDetails["billing_country"].stringValue + " " + productDetails["billing_zipcode"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 260.0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Footer") as! OrderDetailCell
        
        cell.trackBtn.layer.masksToBounds = true
        cell.trackBtn.layer.cornerRadius = cell.trackBtn.frame.height/2
        cell.trackBtn.layer.borderColor = UIColor.baseOrange.cgColor
        cell.trackBtn.layer.borderWidth = 1.5
        
        cell.cancelBtn.layer.masksToBounds = true
        cell.cancelBtn.layer.cornerRadius = cell.cancelBtn.frame.height/2
        cell.cancelBtn.layer.borderColor = UIColor.baseOrange.cgColor
        cell.cancelBtn.layer.borderWidth = 1.5
        
        cell.subTotalLabel.text = "$\(productDetails["order_amount"].stringValue)"
        cell.deliveryCharge.text = "$\(productDetails["delivery_charge"].stringValue)"
        cell.totalLabel.text = "$\(productDetails["final_order_amount"].stringValue)"
        cell.discount_label.text = "$\(productDetails["discount_amount"].stringValue)"
        
        return cell
    }
    
    
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func cancelOrderAction(_ sender: Any) {
        
        let id = productDetails["id"].stringValue
        let status = productDetails["order_status"].intValue
        
        if status >= 2{
            self.customAlert(title: "", message: "You can not cancel this order.", icon: "checkmark3")
            
            
        }else{
            
            let alert = self.createAlert(title: "", message:"Do you want to cancel this order?", icon: "checkmark3")
            
            let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
                self.cancelOrderApi(orderId: id)
            }
            alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
            
            alert.addAction(okAction)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    
    @IBAction func trackOrderAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewTrackOrderVC") as! NewTrackOrderVC
        vc.orderId = productDetails["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func cancelOrderApi(orderId:String){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.cancelOrder)"
        
        /*roles_id=2&user_id=5&card_id=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","order_id":"\(orderId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    
                    
                    let alert = self.createAlert(title: "", message: "Order has been cancelled successfully.", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request is timeout.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
}
