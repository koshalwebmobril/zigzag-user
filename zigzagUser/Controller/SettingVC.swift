//
//  SettingVC.swift
//  zigzagUser
//
//  Created by Webmobril on 09/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController

class SettingVC: UIViewController {
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.view.backgroundColor = UIColor.patternColor
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func language(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func myfavorites(_ sender: Any) {
    }
    @IBAction func contactus(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func termsofservice(_ sender: Any) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVc") as! WebviewVc
        vc.pageId = "1"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func aboutus(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVc") as! WebviewVc
        vc.pageId = "2"
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func logout(_ sender: Any) {
        
        let alert = self.createAlert(title: "", message:"Are you sure! Do you want to logout?", icon: "checkmark3")
        
        let okAction = JHTAlertAction(title: "Yes", style: .default) {(UIAction) in
            
            UserDefaults.standard.removeUserdefaultData()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.gotoLoginVC()
            
        }
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    func logout(){
        
        if Reachability.isConnectedToNetwork(){
        
        let logoutURL = "\(GlobalURL.baseURL)\(GlobalURL.logoutURL)"
        
        let userId = UserDefaults.standard.getUserId()
        
        let param :[String:Any] = ["user_id":"\(userId)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(logoutURL, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            MBProgressHUD.hideHUD()
            
            if response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                
                if swiftyVarJson["error"] == false{
                    
                    let alert = self.createAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {(UIAction) in
                        UserDefaults.standard.removeUserdefaultData()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.gotoLoginVC()
                        
                    }
                    
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }else{
                    
                    UserDefaults.standard.removeUserdefaultData()
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoLoginVC()
                    
                }
                
            }else{
                //                print(response.result.value!)
            }
            
        }
        
        
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
