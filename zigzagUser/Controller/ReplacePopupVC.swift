//
//  ReplacePopupVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 22/04/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController


protocol ReplaceCartDelegate {
    func didReplaceCart(isCustomizable:Int)
}

class ReplacePopupVC: UIViewController {
    
    
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var frontView: UIView!
    
    var delegate :ReplaceCartDelegate?
    
    var currentProviderName = String()
    var isCustomization = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
        colorView.addGestureRecognizer(tapgesture)
        
        messageLabel.text = "Your cart contains dishes from \(UserDefaults.standard.getProviderName()). Do you want to discard the selection and add dishes from \(currentProviderName)? "
    
    }
    
    
    @objc func dismissView(_ sender:UITapGestureRecognizer){
           self.dismiss(animated: true, completion: nil)
       }
       

    @IBAction func noAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func yesAction(_ sender: Any) {
        
        
        removeCartItem()
        
    }
    
    
    
    func removeCartItem(){
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.deletePreviousCart)"
        
        /*roles_id    user_id
        */
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    UserDefaults.standard.setValue(0, forKey: "cart_count")
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    UserDefaults.standard.setProviderName(name: "")
                    UserDefaults.standard.setProviderId(providerId: 0)
                    
                    self.delegate?.didReplaceCart(isCustomizable: self.isCustomization)
                                        
                }else{
                    
                  
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
}
