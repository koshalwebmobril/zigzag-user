//
//  StoreDetailVCViewController.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 07/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FloatRatingView
import CarbonKit
import JHTAlertController

class StoreDetailVC: UIViewController,CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuListVC") as! MenuListVC
        
        controller.providerId = restaurantId
        
        controller.providerName = detailDict["name"].stringValue

        controller.indexValue = Int(index) + 1
        return controller
        
    }
    

    @IBOutlet weak var detailView: UIView!
    

    @IBOutlet weak var favBtn: UIButton!
    
    var detailDict = JSON()
    var menuArray = [JSON]()
    var restaurantId = String()
    var ratingValue = Double()
    var favoriteStatus = Int()
    
    
    var titleArray:[String] = ["Menu","Business Details"]
    
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var avgSale: UILabel!
    @IBOutlet weak var ratinglabel: UILabel!
    @IBOutlet weak var restaurantDesc: UILabel!
    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var restaurantImg: UIImageView!
    @IBOutlet weak var menuView: UIView!
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.backgroundColor = UIColor.patternColor
        
        if #available(iOS 13.0, *) {
               let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
               
               statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
               UIApplication.shared.keyWindow?.addSubview(statusBar)
               } else {
                   
                   if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                       // my stuff
                       statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
                   }

               }
        
              

        getRestaurantDetails()
        
        self.favBtn.isUserInteractionEnabled = false
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    
    
    func updateDetailsUI(){
        
        self.detailView.isHidden = false
        
        self.restaurantImg.layer.masksToBounds = true
        self.restaurantImg.layer.cornerRadius = 5
        let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(detailDict["user_image"].stringValue)")
        self.restaurantImg.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
        
        self.restaurantName.text = detailDict["name"].stringValue
        self.restaurantDesc.text = detailDict["description"].stringValue
        self.ratinglabel.text = "\(ratingValue)"
        
        self.ratingView.rating = ratingValue
        
        
        if favoriteStatus == 1{
            self.favBtn.isSelected = true
        }else{
            self.favBtn.isSelected = false
        }
        
         addMenuListView()
    }
    
    fileprivate func addMenuListView() {
        let tabSwipe = CarbonTabSwipeNavigation(items: self.titleArray, delegate: self)
//        tabSwipe.setTabExtraWidth(40)
        tabSwipe.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 0)
        tabSwipe.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 1)
        tabSwipe.setIndicatorColor(UIColor.baseOrange)
        tabSwipe.setSelectedColor(UIColor.baseOrange)
        tabSwipe.setNormalColor(UIColor.lightGray)
        tabSwipe.view.frame = menuView.bounds
        
        addChild(tabSwipe)
        menuView.addSubview(tabSwipe.view)
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

    @IBAction func addFavAction(_ sender: UIButton) {
        if UserDefaults.standard.getUserId() == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        if sender.isSelected{
            removeFavoriteMethod(providerId: restaurantId)
        }else{
            
            self.addFavoriteMethod()
        }
    }
    }
    

}


extension StoreDetailVC {
    
    func getRestaurantDetails(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.getRestaurantDetailsURL)"
        
        /*roles_id
        user_id
        provider_id*/
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(restaurantId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    self.favBtn.isUserInteractionEnabled = true
                    self.detailDict = swiftyVarJson["restaurant"]
                    self.ratingValue = swiftyVarJson["rating"].doubleValue
                    self.menuArray = swiftyVarJson["menus"].arrayValue
                    self.favoriteStatus = swiftyVarJson["fav_key"].intValue
                    self.avgSale.text = String(format: "Monthly Sale: $%.2f", swiftyVarJson["monthly_sale"].floatValue)
                    
                    self.updateDetailsUI()
                    
                    
                    
                }else{
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    
                }
                
                
                
                
            }else{
              MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    func addFavoriteMethod(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.makeFavoriteURL)"
        
        /*roles_id
        user_id
        provider_id*/
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(self.restaurantId)"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    
                    self.favBtn.isSelected = true
                    
                   let alert = self.createAlert(title: "", message: "Added to favourites", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                       
                        self.dismiss(animated: true, completion: nil)
                    }

                    
                    alert.addAction(okAction)

                    self.present(alert, animated: true, completion: nil)
                    
                    
                }else{
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                   
                    
                }
                
                
                
                
            }else{
              MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func removeFavoriteMethod(providerId : String){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.removeFavorite)"
            
            /*roles_id
            user_id
            provider_id
            */
            
            let userId = UserDefaults.standard.getUserId()
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(providerId)"]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
               
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    
                    
                    
                    if swiftyVarJson["error"] == false{
                        
                        self.favBtn.isSelected = false
                        
                        let alert = self.createAlert(title: "", message: "Removed from favourites", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                           
                            self.dismiss(animated: true, completion: nil)
                        }

                        
                        alert.addAction(okAction)

                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }else{
                        
                        
                       self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                  MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        }
    
    
    
    
    
}
