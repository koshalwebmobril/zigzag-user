//
//  MenuListVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 07/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController

class MenuListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,customizationDelegate,quantityUpdateDelegate ,ReplaceCartDelegate {
    func didReplaceCart(isCustomizable: Int) {
        if isCustomizable == 0{
            var dict = self.productArray[selectedIndexPath.row]
            dict["is_addedToCart"] = ""
            dict["is_addedToCart"] = "1"
            self.productArray[selectedIndexPath.row] = dict
            self.getCartAmount = self.productArray[selectedIndexPath.row]["price"].floatValue
            let discount = (self.getCartAmount * self.productArray[selectedIndexPath.row]["discount"].floatValue)/10
            self.getCartCount = 1
            addItemToCartApi(dishId: self.productArray[selectedIndexPath.row]["id"].stringValue, amount: self.productArray[selectedIndexPath.row]["price"].stringValue, discount: "\(discount)")
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
            vc.restaurantId = self.providerId
            vc.discountValue = self.productArray[selectedIndexPath.row]["discount"].doubleValue
            vc.productId = self.productArray[selectedIndexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.delegate = self
//            self.present(vc, animated: true, completion: nil)
        }
    }
    


   
    func updateQuantity(count: Int,amount:Float,status:Bool) {
        
        let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
        if count == 0{
            var dict = self.productArray[selectedIndexPath.row]
            dict["is_addedToCart"] = ""
            dict["is_addedToCart"] = "0"
            self.productArray[selectedIndexPath.row] = dict
            cell.addBtn.isHidden = false
            cell.steperView.isHidden = true
            self.getCartCount = self.getCartCount - 1
            self.getCartAmount = self.getCartAmount - amount
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: self.getCartAmount)
            
        }else{
            
            if status{
                self.getCartAmount = self.getCartAmount + amount/Float(count)
                
            }else{
                self.getCartAmount = self.getCartAmount - amount/Float(count)
            }
           
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: self.getCartAmount)
        cell.countLabel.text = "\(count)"
        }
    }
    
    
    
    func updateCell(isAdded: Bool, amount:Float) {
        let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
        
        if isAdded{
            cell.addBtn.isHidden = true
            cell.steperView.isHidden = false
            self.getCartCount = self.getCartCount + 1
            self.getCartAmount = self.getCartAmount + amount
            self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: getCartAmount)
        }else{
            cell.addBtn.isHidden = false
            cell.steperView.isHidden = true
        }
    }
    
    
    
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeight: NSLayoutConstraint!
    var senderController : UINavigationController!
    
    var indexValue = Int()
    var providerName = String()
    var productArray = [JSON]()
    var providerId = String()
    
    var businessDetails = JSON()
    var selectedIndexPath = IndexPath()
    
    @IBOutlet weak var blankImgView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var getCartCount = Int()
    var getCartAmount = Float()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.view.backgroundColor = UIColor.patternColor
        
        
        
        bgView.layer.masksToBounds = true
        bgView.layer.cornerRadius =  5.0
        if indexValue == 2{
            indexValue = indexValue + 1
        }
        
         NotificationCenter.default.addObserver(self, selector: #selector(reloadPage), name: NSNotification.Name("reloadPage"), object: nil)
        
    }
    
    
    @objc func reloadPage(){
        
        getRestaurantMenu()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if indexValue == 2{
//            if getCartCount > 0{
//                bottomView.isHidden = false
//                bottomViewHeight.constant = 80
//            }
//
//        }else{
//            bottomView.isHidden = true
//            bottomViewHeight.constant = 0
//        }
        
        getRestaurantMenu()
    }
    
    func updateBottomView(getCount : String ,getPrice : Float){
        if getCount == "0"{
            bottomView.isHidden = true
         bottomViewHeight.constant = 0
            
            UserDefaults.standard.setProviderId(providerId: 0)
            UserDefaults.standard.setProviderName(name: "")
            
        }else{
            bottomView.isHidden = false
            bottomViewHeight.constant = 80
        }
        priceLabel.text = String(format: "Total Amount : $ %.2f", getPrice)
        countLabel.text = "\(getCount) Item(s)"
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if indexValue == 3{
            return 1
        }else{
            return productArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexValue == 1{
            return 100
        }else if indexValue == 2{
            return 180
        }else{
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        blankImgView.isHidden = true
        
        var cell = UITableViewCell()
        if indexValue == 1{
            let menucell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell1", for: indexPath) as! MenuListCell
            
            menucell.menuBackView.layer.shadowColor = UIColor.gray.cgColor
            menucell.menuBackView.layer.shadowOpacity = 0.3
            menucell.menuBackView.layer.shadowOffset = CGSize(width: 2, height: 2)
            menucell.menuBackView.layer.shadowRadius = 6
            menucell.menuBackView.layer.cornerRadius = 10
            
            menucell.menuLabel.text = productArray[indexPath.row]["menu_name"].stringValue
            
            let imgURL:URL = URL(string: "\(GlobalURL.imagebaseURL)\(self.productArray[indexPath.row]["menu_icon"].stringValue)")!
            
            menucell.menuLogoImg.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)

            if productArray[indexPath.row]["hot_deal"].intValue == 0{
                menucell.flameImgView.isHidden = true
                menucell.flameWidth.constant = 0
            }else{
                menucell.flameImgView.isHidden = false
                menucell.flameWidth.constant = 120
            }
            
            cell = menucell
            
        }else if indexValue == 2 {
            let dishcell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as! MenuListCell
            dishcell.bgView.layer.shadowColor = UIColor.gray.cgColor
            dishcell.bgView.layer.shadowOpacity = 0.3
            dishcell.bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
            dishcell.bgView.layer.shadowRadius = 6
            dishcell.bgView.layer.cornerRadius = 10
            
            dishcell.storeImgView.layer.masksToBounds = true
            dishcell.storeImgView.layer.cornerRadius = 5
            
            let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(self.productArray[indexPath.row]["img"].stringValue)")
            
            dishcell.storeImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
            
            dishcell.lblStore.text = self.productArray[indexPath.row]["name"].stringValue
            
            let description = self.productArray[indexPath.row]["description"].stringValue
            dishcell.lblDescription.text = description.htmlToString
            
            dishcell.lblAmount.text = String(format: "$%@",self.productArray[indexPath.row]["price"].stringValue)
            
            let rating = String(format: "%.1f", self.productArray[indexPath.row]["avg_rating"].floatValue)
            
            dishcell.ratingLabel.text = rating
            
            if let rate = Double("\(rating)"){
                dishcell.ratingView.rating = rate
            }
            
            
//            dishcell.steperView.layer.masksToBounds = true
//            dishcell.steperView.layer.cornerRadius = 5.0//dishcell.steperView.frame.height/2
//            dishcell.steperView.layer.borderColor = UIColor.init(red: 255/255.0, green: 75/255.0, blue: 0/255.0, alpha: 1.0).cgColor
//            dishcell.steperView.layer.borderWidth = 2
            
//            dishcell.addBtn.layer.masksToBounds = true
//            dishcell.addBtn.layer.cornerRadius = 5.0//dishcell.addBtn.frame.height/2
//            dishcell.addBtn.layer.borderColor = UIColor.init(red: 255/255.0, green: 75/255.0, blue: 0/255.0, alpha: 1.0).cgColor
//            dishcell.addBtn.layer.borderWidth = 2
            
            if self.productArray[indexPath.row]["is_addedToCart"].intValue == 0{
                dishcell.addBtn.isHidden = false
                dishcell.steperView.isHidden = true
                dishcell.countLabel.text = "1"
            }else{
                dishcell.addBtn.isHidden = true
                dishcell.steperView.isHidden = false
                dishcell.countLabel.text = self.productArray[indexPath.row]["quantity_added"].stringValue
            }
            
            if productArray[indexPath.row]["discount"].floatValue == 0.0{
                dishcell.dishFlameImgView.isHidden = true
                dishcell.discount_label.isHidden = true
                dishcell.dishFlameHeight.constant = 0
               
            }else{
                dishcell.dishFlameImgView.isHidden = false
                dishcell.dishFlameHeight.constant = 40
                dishcell.discount_label.isHidden = false
                dishcell.discount_label.text = String(format: "%.1f%@", productArray[indexPath.row]["discount"].floatValue,"% OFF")
            }
            
            cell = dishcell
            
        }else {
            
            let businesscell = tableView.dequeueReusableCell(withIdentifier: "BusinessCell", for: indexPath) as! MenuListCell
            
            businesscell.storeImgView.layer.masksToBounds = true
            businesscell.storeImgView.layer.cornerRadius = 5
            
            let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(self.businessDetails["user_image"].stringValue)")
            
            businesscell.storeImgView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "burger"), completed: nil)
            
            businesscell.lblStore.text = self.businessDetails["name"].stringValue
            
            let businassDesc = self.businessDetails["description"].stringValue
            businesscell.lblDescription.text = businassDesc.htmlToString
            
            
            
            
            cell = businesscell
            
        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexValue == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DishListVC") as! DishListVC
            vc.providerId = self.providerId
            vc.dishId = self.productArray[indexPath.row]["id"].stringValue
            vc.restaurantName = self.providerName
            vc.titleStr = self.productArray[indexPath.row]["menu_name"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexValue == 2 {
            if self.productArray[indexPath.row]["is_addedToCart"].intValue == 0{
            
            addToCart(indexPath: indexPath)
            }
        }
    }
    
    
    
    @IBAction func addButtonAction(_ sender: Any) {
        
        if UserDefaults.standard.getUserId() == ""{
            self.customAlert(title: "", message: "Please login first!", icon: "checkmark3")
            
            
            
            
        }else{
            
            let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
            let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
            
            self.selectedIndexPath = indexPath
            
             if UserDefaults.standard.getProviderId() == Int(self.providerId) ||  UserDefaults.standard.getProviderId() == 0{
            
            let isCustomizable = self.productArray[indexPath.row]["is_customisable"].stringValue
            
            if isCustomizable == "0"{
                
                
                self.getCartCount = self.getCartCount + 1
                self.getCartAmount = self.getCartAmount + self.productArray[indexPath.row]["price"].floatValue
//                self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
              
                   var dict = self.productArray[indexPath.row]
                   dict["is_addedToCart"] = ""
                   dict["is_addedToCart"] = "1"
                   self.productArray[indexPath.row] = dict
              
                let discount = (self.getCartAmount * self.productArray[indexPath.row]["discount"].floatValue)/100
                    
                addItemToCartApi(dishId: self.productArray[indexPath.row]["id"].stringValue, amount: self.productArray[indexPath.row]["price"].stringValue, discount: "\(discount)")
               
                
                
            }else{
                
                
               
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
                    vc.restaurantId = self.providerId
                    vc.productId = self.productArray[indexPath.row]["id"].stringValue
                vc.providerName = self.providerName
                vc.discountValue = self.productArray[indexPath.row]["discount"].doubleValue
                self.navigationController?.pushViewController(vc, animated: true)
//                    vc.modalPresentationStyle = .overCurrentContext
//                    vc.delegate = self
//
//                    self.present(vc, animated: true, completion: nil)
               
            }
            }else{
                  let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReplacePopupVC") as! ReplacePopupVC
                        vc.modalPresentationStyle = .overCurrentContext
                vc.delegate = self
                vc.currentProviderName = self.providerName
                vc.isCustomization = self.productArray[selectedIndexPath.row]["is_customisable"].intValue
                self.present(vc, animated: true, completion: nil)
                
            }
        }
        
    }
    
    
    
    func addToCart(indexPath:IndexPath){
            
            if UserDefaults.standard.getUserId() == ""{
                self.customAlert(title: "", message: "Please login first!", icon: "checkmark3")
                
               
                
            }else{
                
//                let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
//                let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
                
                self.selectedIndexPath = indexPath
                
                 if UserDefaults.standard.getProviderId() == Int(self.providerId) ||  UserDefaults.standard.getProviderId() == 0{
                
                let isCustomizable = self.productArray[indexPath.row]["is_customisable"].stringValue
                
                if isCustomizable == "0"{
                    
                    
                    self.getCartCount = self.getCartCount + 1
                    self.getCartAmount = self.getCartAmount + self.productArray[indexPath.row]["price"].floatValue
    //                self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
                   
                        var dict = self.productArray[indexPath.row]
                        dict["is_addedToCart"] = ""
                        dict["is_addedToCart"] = "1"
                        self.productArray[indexPath.row] = dict
                        let discount = (self.getCartAmount * self.productArray[indexPath.row]["discount"].floatValue)/100
                    addItemToCartApi(dishId: self.productArray[indexPath.row]["id"].stringValue, amount: self.productArray[indexPath.row]["price"].stringValue, discount: "\(discount)")
                   
                    
                    
                }else{
                    
                    
                   
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomizationVC") as! CustomizationVC
                        vc.restaurantId = self.providerId
                        vc.productId = self.productArray[indexPath.row]["id"].stringValue
//                        vc.modalPresentationStyle = .overCurrentContext
//                        vc.delegate = self
                    vc.providerName = self.providerName
                    vc.discountValue = self.productArray[indexPath.row]["discount"].doubleValue
                    self.navigationController?.pushViewController(vc, animated: true)
//                        self.present(vc, animated: true, completion: nil)
                   
                }
                }else{
                      let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReplacePopupVC") as! ReplacePopupVC
                            vc.modalPresentationStyle = .overCurrentContext
                    vc.delegate = self
                    vc.currentProviderName = self.providerName
                    vc.isCustomization = self.productArray[selectedIndexPath.row]["is_customisable"].intValue
                             self.present(vc, animated: true, completion: nil)
                    
                }
            }
            
        }
    
    
    @IBAction func stepDownAction(_ sender: Any) {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! MenuListCell
        var count: Int = Int(cell.countLabel.text ?? "") ?? 0
        
        
        self.selectedIndexPath = indexPath!
        let isCustomizable = self.productArray[indexPath!.row]["is_customisable"].stringValue
        
        if isCustomizable == "0"{
            
            let prevAmount = productArray[indexPath!.row]["price"].floatValue
            
            if count > 1{
                count = count  - 1
                cell.countLabel.text = "\(count)"
               self.getCartAmount = self.getCartAmount - prevAmount
                
            }else{
                count = 0
                cell.addBtn.isHidden = false
                cell.steperView.isHidden = true
                self.getCartCount = self.getCartCount - 1
                self.getCartAmount = self.getCartAmount - prevAmount
            }
            
            
            
            
            let amount = prevAmount * Float(count)
            let dishID = productArray[indexPath!.row]["id"].stringValue
            
            if count == 0{
                var dict = self.productArray[indexPath!.row]
                dict["is_addedToCart"] = ""
                dict["is_addedToCart"] = "0"
                self.productArray[indexPath!.row] = dict
            }
//            self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
            let discount = (amount * productArray[indexPath!.row]["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
            
            
        }else{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPopupVC") as! AddPopupVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.descArray = self.productArray[indexPath!.row]
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            
        }
        
        
        
        
    }
    
    @IBAction func stepUpAction(_ sender: Any) {
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let cell = self.tableView.cellForRow(at: indexPath!) as! MenuListCell
        var count: Int = Int(cell.countLabel.text ?? "") ?? 0
        
        if count <= 99 {
        self.selectedIndexPath = indexPath!
        let isCustomizable = self.productArray[indexPath!.row]["is_customisable"].stringValue
        
        if isCustomizable == "0"{
            let prevAmount = productArray[indexPath!.row]["price"].floatValue
            count = count + 1
            
            cell.countLabel.text = "\(count)"
            
            let amount = prevAmount * Float(count)
            let dishID = productArray[indexPath!.row]["id"].stringValue
            
            self.getCartAmount = self.getCartAmount + prevAmount
            
//            self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
            let discount = (amount * productArray[indexPath!.row]["discount"].floatValue)/100
            updateCartAPi(amount: "\(amount)", quantity: "\(count)", dishId: "\(dishID)", discount: "\(discount)")
            
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPopupVC") as! AddPopupVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.descArray = self.productArray[indexPath!.row]
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            
        }
        }else{
            self.customAlert(title: "", message: "You can not order more than 99 Quantity", icon: "checkmark3")
        }
        
        
    }
    
    
    
    
    func updateCountLabel(count:String){
        
    }
    
    
    @IBAction func placeOrderAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderVC") as! PlaceOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}


extension MenuListVC {
    
    func getRestaurantMenu(){
        
        if Reachability.isConnectedToNetwork(){
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.restaurantTabURL)"
        
        /*roles_id
         provider_id
         tab=1,tab=2,tab=3*/
        
        let userId = UserDefaults.standard.getUserId()
        let params :[String:Any] = ["roles_id":"2","provider_id":"\(providerId)","tab":"\(indexValue)","user_id":"\(userId)"]
        print(params)
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                
                
                
                if swiftyVarJson["error"] == false{
                    
                   
                    
                    if self.indexValue == 3{
                        self.businessDetails = swiftyVarJson["data"]
                        
                    }else if self.indexValue == 2{
                            
                        self.productArray = swiftyVarJson["dishes"].arrayValue
                        self.getCartCount = swiftyVarJson["cart_counts"].intValue
                        self.getCartAmount = swiftyVarJson["cart_price_counts"].floatValue
                        
                            self.updateBottomView(getCount: swiftyVarJson["cart_counts"].stringValue, getPrice: swiftyVarJson["cart_price_counts"].floatValue)
                        } else{
                        self.productArray = swiftyVarJson["data"].arrayValue
                      
                    }
                    if self.productArray.count > 0 || self.businessDetails.count > 0 {
                         self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }else{
                        self.blankImgView.isHidden = false
                        self.tableView.reloadData()
                    }
                    
                    
                }else{
                    self.blankImgView.isHidden = false
                    self.tableView.isHidden = true
                    self.bottomView.isHidden = true
                    self.bottomViewHeight.constant = 0
//
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func addItemToCartApi(dishId:String,amount:String,discount:String)  {
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.addToCart)"
        
        /*roles_id
         user_id
         dish_id
         quantity
         amount
         provider_id,is_customizable*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","amount":"\(amount)","quantity":"1","provider_id":"\(self.providerId)","dish_id":"\(dishId)","is_customizable":"0","discount_price":"\(discount)"]
        //        let params :[String:Any] = ["roles_id":"2","provider_id":"62","dish_id":"4"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    NotificationCenter.default.post(name: NSNotification.Name("updateCartCount"), object: nil)
                    
                    let alert = self.createAlert(title: "", message: "Added to shopping cart.", icon: "cart-1")
                     
                    let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                        
                    let cell = self.tableView.cellForRow(at: self.selectedIndexPath) as! MenuListCell
                    cell.addBtn.isHidden = true
                    cell.steperView.isHidden = false
                    
                    UserDefaults.standard.setProviderId(providerId: Int(self.providerId)!)
                    UserDefaults.standard.setProviderName(name: self.providerName)
                        
                        self.updateBottomView(getCount: "\(self.getCartCount)", getPrice: self.getCartAmount)
                     }
                     alert.addAction(okAction)
                      self.present(alert, animated: true, completion: nil)
                   
                    
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    
                }
                
                
                
                
            }else{
                MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func updateCartAPi(amount:String,quantity:String,dishId:String,discount:String){
        if Reachability.isConnectedToNetwork(){
            
            
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.updateCart)"
        
        /*roles_id=2&dish_id
         user_id&amount=44.00&quantity=2*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","dish_id":"\(dishId)","amount":"\(amount)","quantity":"\(quantity)","discount_price":"\(discount)"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                                       MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    if quantity == "0"{
                    NotificationCenter.default.post(name: NSNotification.Name("removeCartCount"), object: nil)
                    }
                    self.updateBottomView(getCount:"\(self.getCartCount)" , getPrice:self.getCartAmount )
                    
//                    if quantity == "0"{
//
//
//
//                    }
                    
                    
                    
                }else{
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                   
                    
                }
                
                
                
                
            }else{
             MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
}
