//
//  ExpressVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 24/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import JHTAlertController
import iOSDropDown
import Alamofire
import SwiftyJSON
import CoreLocation


class ExpressVC: UIViewController ,SetAddressDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,setExpressAddressDelegate{
    
    func setExpressAddress(address: String,lattitude: Double,longitude:Double,name:String,mobile:String,addressline2:String,landmark:String,city:String,state:String,country:String) {
        if addressType == "pickup"{
            self.lblPickup.text = "\(address)" + " \(addressline2)," + " " + " City:\(city)," + " " + " Country:\(country)"
            pickupLat = lattitude
            pickupLong = longitude
            fromName = name
            fromMobile = mobile
            self.lblfromName.text = name
            self.lblfromMobile.text = String(format: "Mobile No: %@", mobile)
            billingCity = city
            billingState = state
            billingCountry = country
            billingLandmark = landmark
            toLandmark.text = "Pickup"
            issetPickup = true
            pickupDict = ["name":name,"phone":mobile,"address":address,"line2":addressline2,"landmark":landmark,"city":city,"state":state,"country":country]
            
        }else{
            self.lblDrop.text = "\(address)" + " \(addressline2),"  + " " + " City:\(city),"  + " " + " Country:\(country)"
            dropLat = lattitude
            dropLong = longitude
            toName = name
            toMobile = mobile
            lbltoName.text = name
            self.lbltoMobile.text = String(format: "Mobile No: %@", mobile)
            issetDrop = true
            
            destCity = city
            destState = state
            destCountry = country
            destLandmark = landmark
            fromLandmark.text = "Drop"
            dropDict = ["name":name,"phone":mobile,"address":address,"line2":addressline2,"landmark":landmark,"city":city,"state":state,"country":country]
        }
    }
    
    func setAddress(address: String,lattitude: Double,longitude:Double,name:String,mobile:String,addressline2:String,landmark:String,city:String,state:String,country:String) {
        if addressType == "pickup"{
            self.lblPickup.text = "\(address),"  + " " + " City:\(city)," + " " + " State:\(state)," + " " + " Country:\(country)"
            pickupLat = lattitude
            pickupLong = longitude
            fromName = name
            fromMobile = mobile
            self.lblfromName.text = name
            self.lblfromMobile.text = String(format: "Mobile No: %@", mobile)
            billingCity = city
            billingState = state
            billingCountry = country
            billingLandmark = landmark
            toLandmark.text = "Landmark: \(landmark)"
            issetPickup = true
            pickupDict = ["name":name,"phone":mobile,"address":address,"line2":addressline2,"landmark":landmark,"city":city,"state":state,"country":country]
            
        }else{
            self.lblDrop.text = "\(address),"  + " " + " City:\(city)," + " " + " State:\(state)," + " " + " Country:\(country)"
            dropLat = lattitude
            dropLong = longitude
            toName = name
            toMobile = mobile
            lbltoName.text = name
            self.lbltoMobile.text = String(format: "Mobile No: %@", mobile)
            issetDrop = true
            
            destCity = city
            destState = state
            destCountry = country
            destLandmark = landmark
            fromLandmark.text = "Landmark: \(landmark)"
            dropDict = ["name":name,"phone":mobile,"address":address,"line2":addressline2,"landmark":landmark,"city":city,"state":state,"country":country]
        }
    }
    
    
    @IBOutlet weak var dateHeight: NSLayoutConstraint!
    @IBOutlet weak var selectDateBtn: UIButton!
    @IBOutlet weak var pickupBtn: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    var datePicker = UIDatePicker()
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var lblDrop: UILabel!
    @IBOutlet weak var toLandmark: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var packBtn: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var fromLandmark: UILabel!
    @IBOutlet weak var docBtn: UIButton!
    @IBOutlet weak var lbltoName: UILabel!
    @IBOutlet weak var lblfromName: UILabel!
    @IBOutlet weak var txtDetail: UITextField!
    @IBOutlet weak var txtPackValue: UITextField!
    @IBOutlet weak var lblPickup: UILabel!
    
    @IBOutlet weak var continueBtn: UIButton!
    
    @IBOutlet weak var txtWeight: DropDown!
    @IBOutlet var views: [UIView]!
    
    var pickupDict = [String:String]()
    var dropDict = [String:String]()
    
    var image = UIImage()
    
    var pickupLat = Double()
    var pickupLong = Double()
    var dropLat = Double()
    var dropLong = Double()
    var isAcceptTerms = "0"
    var issetPickup = false
    var issetDrop = false
    var expressId = ""
    
    @IBOutlet weak var lblfromMobile: UILabel!
    @IBOutlet weak var lbltoMobile: UILabel!
    var addressType = String()
    var fromName = String()
    var fromMobile = String()
    var toName = String()
    var toMobile = String()
    var shipmentType = String()
    
    
    var billingLandmark = String()
    var billingCity = String()
    var billingState = String()
    var billingCountry = String()
    var billingAddressLine2 = String()
    
    var destLandmark = String()
    var destCity = String()
    var destState = String()
    var destCountry = String()
    var destAddressLine2 = String()
    
    
    var senderTag = Int()
    var imagePicker = UIImagePickerController()
    var weightArray = ["0.5","1.0","1.5","2.0","2.5","3.0","3.5","4.0","4.5","5.0","5.5","6.0","6.5","7.0","7.5","8.0","8.5","9.0","9.5","10.0","10.5","11.0","11.5","12.0","12.5","13.0","13.5","14.0","14.5","15.0"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        
        setUI()
        
    }
    
    func setUI(){
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Accept Terms and Services.", attributes: underlineAttribute)
        termsLabel.attributedText = underlineAttributedString
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(termsAction))
        termsLabel.addGestureRecognizer(gesture)
        
        pickupBtn.isSelected = true
        selectDateBtn.isSelected = false
        dateHeight.constant = 0
        txtDate.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        txtDate.text = formatter.string(from: Date())
        
        docBtn.isSelected = true
        packBtn.isSelected = false
        shipmentType = "1"
        
        imagePicker.delegate = self
        
        continueBtn.layer.masksToBounds = true
        continueBtn.layer.cornerRadius = 5.0
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let button1 = UIButton(frame: CGRect(x: 0, y: 5, width: 30, height: view1.frame.height - 10))
        button1.setImage(UIImage(named: "cal"), for: .normal)
        
        view1.addSubview(button1)
        txtDate.rightViewMode = .always
        txtDate.rightView = view1
        button1.addTarget(self, action: #selector(dateShow), for: .touchUpInside)
        
        for item in views{
            item.layer.cornerRadius = 10
            item.layer.shadowColor = UIColor.gray.cgColor
            item.layer.shadowOpacity = 1
            item.layer.shadowOffset = CGSize.zero
            item.layer.shadowRadius = 2.5
            
        }
        
        txtWeight.optionArray = weightArray
        
        let pickupTap = UITapGestureRecognizer(target: self, action: #selector(showPickupAddress))
        let dropTap = UITapGestureRecognizer(target: self, action: #selector(showDropAddress))
        
        lblPickup.addGestureRecognizer(pickupTap)
        lblDrop.addGestureRecognizer(dropTap)
        
        showDatePicker()
        
        
    }
    
    func showDatePicker(){
        //Formate Date
        
        
        //        datePicker.minimumDate =
        datePicker.minimumDate = NSDate() as Date
        datePicker.datePickerMode = .date
        
        
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDate.inputAccessoryView = toolbar
        txtDate.inputView = datePicker
        
    }
    @objc func dateShow(){
        txtDate.becomeFirstResponder()
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        txtDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func documentAction(_ sender: UIButton) {
        txtDetail.text = ""
        txtPackValue.text = ""
        txtWeight.text = ""
        
        txtDetail.placeholder = "Document Details"
        txtPackValue.placeholder = "Document Value"
        
        valueLabel.text = "Document Value"
        detailLabel.text = "Document Details"
        
        docBtn.isSelected = true
        packBtn.isSelected = false
        shipmentType = "1"
        
        
    }
    
    @IBAction func packageAction(_ sender: UIButton) {
        txtDetail.text = ""
        txtPackValue.text = ""
        txtWeight.text = ""
        
        txtDetail.placeholder = "Package Details"
        txtPackValue.placeholder = "Package Value"
        
        valueLabel.text = "Package Value"
        detailLabel.text = "Package Details"
        
        docBtn.isSelected = false
        packBtn.isSelected = true
        shipmentType = "2"
    }
    
    @objc func termsAction() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebviewVc") as! WebviewVc
        vc.pageId = "3"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func pickupnowAction(_ sender: Any) {
        
        pickupBtn.isSelected = true
        selectDateBtn.isSelected = false
        dateHeight.constant = 0
        txtDate.isHidden = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        txtDate.text = formatter.string(from: Date())
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtWeight{
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func selectDateAction(_ sender: UIButton) {
        
        if !sender.isSelected {
            
            txtDate.text = ""
        }
        
        pickupBtn.isSelected = false
        selectDateBtn.isSelected = true
        dateHeight.constant = 50
        txtDate.isHidden = false
        
    }
    
    @objc func showPickupAddress(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        vc.delegate = self
        self.addressType = "pickup"
        vc.isFromExpress = true
        //        vc.data = pickupDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func showDropAddress(){
        
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetAddressVC") as! SetAddressVC
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        vc.delegate = self
        self.addressType = "drop"
        vc.isFromExpress = true
        //        vc.data = dropDict
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func pickupAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        vc.delegate = self
        self.addressType = "pickup"
        vc.isFromExpress = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func dropAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressListVC") as! AddressListVC
        vc.delegate = self
        self.addressType = "drop"
        vc.isFromExpress = true
        //        vc.data = dropDict
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func checkTermAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected{
            isAcceptTerms = "1"
            
        }else{
            isAcceptTerms = "0"
        }
        
    }
    
    
    
    
    
    
    func isValidate()->Bool{
        
        if issetPickup == false{
            self.customAlert(title: "", message: "Enter pick up address.", icon: "checkmark3")
            return false
        }else  if issetDrop == false{
            self.customAlert(title: "", message: "Enter drop address.", icon: "checkmark3")
            return false
        }else  if !(txtWeight.text?.isValidTextField())!{
            if shipmentType == "1"{
                self.customAlert(title: "", message: "Select Document weight.", icon: "checkmark3")
                return false
            }else{
                self.customAlert(title: "", message: "Select Package weight.", icon: "checkmark3")
                return false
            }
        }else if !(txtPackValue.text?.isValidTextField())!{
            self.customAlert(title: "", message: "Enter Package value.", icon: "checkmark3")
            return false
        }else if !(txtDetail.text?.isValidTextField())!{
            self.customAlert(title: "", message: "Enter Document Detail.", icon: "checkmark3")
            return false
        }else  if isAcceptTerms == "0"{
            self.customAlert(title: "", message: "Pleas accept terms and services.", icon: "checkmark3")
            return false
        }
        
        return true
    }
    
    
    
    @IBAction func continueAction(_ sender: Any) {
        if UserDefaults.standard.getUserId() == ""{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            if isValidate(){
                saveProductDetails()
            }
        }
    }
    
    
    
    func calculateDistance(lat1: Double,long1:Double,lat2:Double,long2:Double)->Double{
        //My location
        let myLocation = CLLocation(latitude: lat1, longitude: long1)
        
        //My buddy's location
        let myBuddysLocation = CLLocation(latitude: lat2, longitude: long2)
        
        //Measuring my distance to my buddy's (in km)
        let distance = myLocation.distance(from: myBuddysLocation) / 1000
        return distance
    }
    
    func saveProductDetails(){
        
        if Reachability.isConnectedToNetwork(){
            
            let distanceValue =  self.calculateDistance(lat1: self.pickupLat, long1: self.pickupLong, lat2: self.dropLat, long2: self.dropLong)
            
            //            if distanceValue > 15.00 {
            //
            //                let alert = self.createAlert(title: "", message: "We provide Express Service delivery within 15 KM distance.", icon: "checkmark3")
            //
            //                let okAction = JHTAlertAction(title: "Ok", style: .cancel, handler: nil)
            //
            //                alert.addAction(okAction)
            //
            //                self.present(alert, animated: true, completion: nil)
            //
            //
            //            }else{
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.expressURL)"
            
            /*roles_id=2&user_id=241&delivery_date=2020-05-07&billing_address=noida
             
             
             billing_landmark, billing_city, billing_state, billing_country
             destination_landmark, destination_city, destination_state, destination_country
             */
            
            let userId = UserDefaults.standard.getUserId()
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","delivery_date":txtDate.text ?? "","billing_address":"\(lblPickup.text ?? "")","destination_address":"\(lblDrop.text ?? "")","order_lat":"\(pickupLat)","order_lng":"\(pickupLong)","to_lat":"\(dropLat)","to_long":"\(dropLong)","package_size":txtWeight.text ?? "" ,"package_value":txtPackValue.text ?? "" ,"package_detail":txtDetail.text ?? "","from_name":fromName,"to_name":toName,"from_mobile":fromMobile,"to_mobile":toMobile,"shipment_type":shipmentType,"billing_landmark":billingLandmark,"billing_city":billingCity,"billing_state":billingState,"billing_country":billingCountry,"destination_landmark":destLandmark,"destination_city":destCity,"destination_state":destState,"destination_country":destCountry,"line_2_billing":billingAddressLine2,"line_2_destination":destAddressLine2]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        
                        self.expressId = swiftyVarJson["data"]["id"].stringValue
                        
                        let alert = self.createAlert(title: "", message: "Express service driver will pick up your package very soon.", icon: "express")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContinueVC") as! ContinueVC
                            vc.expressId = self.expressId
                            vc.shipmentType = self.shipmentType
                            //                        vc.amount = 20.00
                            vc.packageSize = self.txtWeight.text ?? ""
                            vc.distance = String(format: "%.2f", self.calculateDistance(lat1: self.pickupLat, long1: self.pickupLong, lat2: self.dropLat, long2: self.dropLong))
                            vc.frommName = self.fromName
                            vc.fromMobile = self.fromMobile
                            vc.toName = self.toName
                            vc.toMobile = self.toMobile
                            vc.fromAddress = self.lblPickup.text ?? ""
                            vc.toAddress = self.lblDrop.text ?? ""
                            vc.toLandmark = self.billingLandmark
                            vc.fromLandmarkvalue = self.destLandmark
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                            self.dismiss(animated: true, completion: {
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContinueVC") as! ContinueVC
                            vc.expressId = self.expressId
                            vc.shipmentType = self.shipmentType
                            //                        vc.amount = 20.00
                            vc.packageSize = self.txtWeight.text ?? ""
                            vc.distance = String(format: "%.2f", self.calculateDistance(lat1: self.pickupLat, long1: self.pickupLong, lat2: self.dropLat, long2: self.dropLong))
                            vc.frommName = self.fromName
                            vc.fromMobile = self.fromMobile
                            vc.toName = self.toName
                            vc.toMobile = self.toMobile
                            vc.fromAddress = self.lblPickup.text ?? ""
                            vc.toAddress = self.lblDrop.text ?? ""
                            vc.toLandmark = self.billingLandmark
                            vc.fromLandmarkvalue = self.destLandmark
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            })
                        }
                        
                    }else{
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let alert = self.createAlert(title: "", message: "Data can't be saved", icon: "checkmark3")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            
                        }
                        alert.addAction(okAction)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        
                    }
                }else{
                    MBProgressHUD.hide(for: self.view, animated: true)
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
            //            }
            
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
}
