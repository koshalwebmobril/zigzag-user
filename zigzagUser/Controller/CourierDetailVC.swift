//
//  CourierDetailVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 06/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON

class CourierDetailVC: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    
    
    
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    
    
    @IBOutlet weak var btnReceived: UIButton!
    @IBOutlet weak var btnConfirmed: UIButton!
    
    @IBOutlet weak var btnWay: UIButton!
    @IBOutlet weak var btnDelivered: UIButton!
    
    @IBOutlet weak var deliveredLabel: UILabel!
    @IBOutlet weak var wayLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var receivedLabel: UILabel!
    var data = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()

       setUI()
    }
    
    func setUI(){
        
        
            view1.layer.masksToBounds = true
            view1.layer.cornerRadius = view1.frame.height/2
        
        view2.layer.masksToBounds = true
        view2.layer.cornerRadius = view2.frame.height/2
        
        view3.layer.masksToBounds = true
        view3.layer.cornerRadius = view3.frame.height/2
        
        view4.layer.masksToBounds = true
        view4.layer.cornerRadius = view4.frame.height/2
            
        orderLabel.text = data["id"].stringValue
        timeLabel.text = "1 Week"
       
        
        if data["order_status"].intValue == 1{
            btnReceived.isSelected = true
            view1.backgroundColor = .baseOrange
            receivedLabel.backgroundColor = .baseOrange
        }else if data["order_status"].intValue == 2{
            btnReceived.isSelected = true
            btnWay.isSelected = true
            
            view1.backgroundColor = .baseOrange
            view2.backgroundColor = .baseOrange
            receivedLabel.backgroundColor = .baseOrange
            confirmLabel.backgroundColor = .baseOrange
            
        }else if data["order_status"].intValue == 3{
            btnReceived.isSelected = true
            btnWay.isSelected = true
            btnConfirmed.isSelected = true
            view1.backgroundColor = .baseOrange
            view2.backgroundColor = .baseOrange
            view3.backgroundColor = .baseOrange
            receivedLabel.backgroundColor = .baseOrange
            confirmLabel.backgroundColor = .baseOrange
            wayLabel.backgroundColor = .baseOrange
            
        }else if data["order_status"].intValue == 4{
            btnReceived.isSelected = true
            btnWay.isSelected = true
            btnConfirmed.isSelected = true
            btnDelivered.isSelected = true
            view1.backgroundColor = .baseOrange
            view2.backgroundColor = .baseOrange
            view3.backgroundColor = .baseOrange
            view4.backgroundColor = .baseOrange
            receivedLabel.backgroundColor = .baseOrange
            confirmLabel.backgroundColor = .baseOrange
            wayLabel.backgroundColor = .baseOrange
            deliveredLabel.backgroundColor = .baseOrange
            
        }
        
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
