//
//  CheckoutVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 20/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import JHTAlertController
import Alamofire
import SwiftyJSON

class CheckoutVC: UIViewController{
    
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var proceedBtn: UIButton!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var cardBtn: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    
    var clientNotes = String()
    var discount = 0.0
    var oderAmount = String()
    var deliveryCharge = String()
    var restaurantId = String()
    var finalAmount = Float()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }
        
        setUI()

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }
    }
    
    
    func setUI(){
        proceedBtn.layer.masksToBounds = true
        proceedBtn.layer.cornerRadius = 5.0
        
        amountLabel.text = String(format: "$%.2f", finalAmount)
        view2.layer.masksToBounds = true
        view2.layer.borderWidth = 1.5
        view2.layer.borderColor = UIColor.gray.cgColor
       
        view2.layer.cornerRadius = 10
        
        cashBtn.isSelected = true
        
    }
    
    @IBAction func backAtion(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func codAction(_ sender: Any) {
        cashBtn.isSelected = true
        cardBtn.isSelected = false
        
    }
    
    @IBAction func cardAction(_ sender: Any) {
        
         self.customAlert(title: "", message:"Available Soon...", icon: "maintenance")
        
    }
    
    
    @IBAction func proceedAction(_ sender: Any) {
        
        let str = UserDefaults.standard.value(forKey: "order") as? String ?? ""
                if   str == "" {
                    let alert = self.createAlert(title: "", message:"Are you sure! Do you want to place order?", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in

                        self.paymentSuccessApi()
                        
                    }
                    alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    let alert = self.createAlert(title: "", message:"Are you sure! Do you want to place order?", icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in

                        self.expPayment()
                        
                    }
                    alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
        
        
        
        
        
       
    }
    
    
    func paymentSuccessApi(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.paymentApi)"
        
        /*roles_id=2&user_id=14&provider_id=62&order_amt=553.00&delivery_charge=0.0*/
        
        let userId = UserDefaults.standard.getUserId()
        
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","provider_id":"\(self.restaurantId)","order_amt":"\(self.oderAmount)","delivery_charge":"\(self.deliveryCharge)","client_note":"\(clientNotes)","payment_type":"1","discount_amount":"\(discount)"]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
                    UserDefaults.standard.setProviderId(providerId: 0)
                                           UserDefaults.standard.setProviderName(name: "")
                     let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
                    VC.modalPresentationStyle = .fullScreen
                    self.present(VC, animated: true)

//                    self.bgView.isHidden = false
                       

                    
                }else{
                    
                    let alert = self.createAlert(title: "", message:swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                   
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func expPayment(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.expressPaymentURL)"
        
        /*roles_id=2&user_id=14&provider_id=62&order_amt=553.00&delivery_charge=0.0*/
        
        let userId = UserDefaults.standard.getUserId()
        
        let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","express_id":"\(self.restaurantId)","order_amount":self.oderAmount]
        
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    
 
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSuccessVC") as! PaymentSuccessVC
                    VC.modalPresentationStyle = .fullScreen
                    self.present(VC, animated: true)
                    
                        

                    
                }else{
                    
                    let alert = self.createAlert(title: "", message:swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                    let okAction = JHTAlertAction(title: "OK", style: .default) {_ in
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                   
                    
                    alert.addAction(okAction)
                    
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    

}



