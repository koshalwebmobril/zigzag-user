//
//  ContinueVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 17/07/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JHTAlertController

class ContinueVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var buttonView: UIStackView!
    @IBOutlet weak var buttonView2: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lbltoAddress: UILabel!
    @IBOutlet weak var lblfromAddress: UILabel!
    @IBOutlet weak var lbltoName: UILabel!
    @IBOutlet weak var lblfromName: UILabel!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var btnViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var fromLandmark: UILabel!
    @IBOutlet weak var frommobilelbl: UILabel!
    @IBOutlet weak var landmarklbl: UILabel!
    @IBOutlet weak var toMobilelbl: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var btnViewHeight2: NSLayoutConstraint!
    var shipmentType = String()
    var senderTag = Int()
     var image = UIImage()
    var expressId = String()
    var frommName = String()
    var fromMobile = String()
    var fromAddress = String()
    var toName = String()
    var toMobile = String()
     var toAddress = String()
    var packageSize = String()
    var distance = String()
    var amount = Float()
    
    var fromLandmarkvalue = String()
    var toLandmark = String()

    var isImageUpload = false
    
     var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }

        calculatePriceMethod()
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }
    }
    
    
    func setUI(){
        self.scrollView.isHidden = false
        imagePicker.delegate = self
        
        continueBtn.layer.masksToBounds = true
        continueBtn.layer.cornerRadius = 5.0
        
        
            topView.layer.cornerRadius = 10
            topView.layer.shadowColor = UIColor.gray.cgColor
            topView.layer.shadowOpacity = 1
            topView.layer.shadowOffset = CGSize.zero
            topView.layer.shadowRadius = 2.5

//        if shipmentType == "1"{
//            buttonView.isHidden = true
//            buttonView2.isHidden = true
//            btnViewHeight.constant = 0
//            btnViewHeight2.constant = 0
//
//        }else{
            buttonView.isHidden = false
            buttonView2.isHidden = false
            btnViewHeight.constant = 60
            btnViewHeight2.constant = 60
//        }
        
//        amountLabel.text = String(format: "$%.2f", amount)
        lblfromName.text = frommName
        lblfromAddress.text = String(format: "%@",fromAddress)
        lbltoName.text = toName
        lbltoAddress.text = String(format: "%@",toAddress)
        
        toMobilelbl.text = fromMobile
        landmarklbl.text = "Pickup Address" //toLandmark
        
        frommobilelbl.text = toMobile
        fromLandmark.text = "Drop Address" //fromLandmarkvalue
        
       
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func commonAction(_ sender: UIButton) {
           
           self.senderTag = sender.tag
           
          
           
           let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
           alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
           }))
           alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
               
           }))
           alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
           
           
           switch UIDevice.current.userInterfaceIdiom {
           case .pad:
               alert.popoverPresentationController?.sourceView = sender as? UIView
               alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
               alert.popoverPresentationController?.permittedArrowDirections = .up
           default:
               break
           }
           
           self.present(alert, animated: true, completion: nil)
           
       }
       
       func openCamera()
       {
           if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
           {
               imagePicker.sourceType = UIImagePickerController.SourceType.camera
               imagePicker.allowsEditing = true
               self.present(imagePicker, animated: true, completion: nil)
           }
           else
           {
               
               self.customAlert(title: "", message: "You don't have camera", icon: "checkmark3")
               
           }
       }
       
       func openGallary()
       {
           imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
           imagePicker.allowsEditing = true
           imagePicker.modalPresentationStyle = .fullScreen
           self.present(imagePicker, animated: true, completion: nil)
           
       }
       
       
       @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           self.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        self.isImageUpload = true
           
               addImage(tagg: self.senderTag)
           
           self.dismiss(animated: true, completion: nil)
           
       }

    
    @IBAction func continueAction(_ sender: Any) {
        
        let alert = self.createAlert(title: "", message:"Do you want to continue Shipping?", icon: "checkmark3")
        let okAction = JHTAlertAction(title: "Yes", style: .default) {_ in
            if self.shipmentType == "1"{
                UserDefaults.standard.set("yes", forKey: "order")
                
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
                
                VC.oderAmount = "\(self.amount)"
                //        VC.deliveryCharge = self.amount
                VC.restaurantId = self.expressId
                VC.clientNotes = ""
                VC.finalAmount = self.amount
                VC.modalPresentationStyle = .fullScreen
                self.present(VC, animated: true)
            }else{
                if self.isImageUpload == false{
                    self.customAlert(title: "", message: "Please add images for Package.", icon: "checkmark3")
                }else{
                    UserDefaults.standard.set("yes", forKey: "order")
                    
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
                    
                    VC.oderAmount = "\(self.amount)"
                    //        VC.deliveryCharge = self.amount
                    VC.restaurantId = self.expressId
                    VC.clientNotes = ""
                    VC.finalAmount = self.amount
                    VC.modalPresentationStyle = .fullScreen
                    self.present(VC, animated: true)
                }
            }
            
        }
        alert.addAction(JHTAlertAction(title: "No", style: .cancel, handler: nil))
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func calculatePriceMethod(){
                
        if Reachability.isConnectedToNetwork(){
                
                
                let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.calculatePriceURL)"
                
                /*keyword*/
                
        let params = ["package_size":self.packageSize,"distance":self.distance]
                print(params)
               
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
              
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    if  response.result.value != nil{
                        
                        let swiftyVarJson = JSON(response.result.value!)
                        print(swiftyVarJson)
                        if swiftyVarJson["error"] == false{
                            self.amount = swiftyVarJson["value"].floatValue
                            self.amountLabel.text = String(format: "$%.2f", swiftyVarJson["value"].floatValue)
                            self.setUI()
                            
                        }else{
                            
                            
                        }
                        
                        
                        
                        
                    }else{
                        
                         MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    }
                    
                }
            }else{
                
                self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
                
                
            }
            }
    
    
    
    
    func addImage(tagg:Int){
        
        if Reachability.isConnectedToNetwork(){
                
                let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.uploadProductURL)"
                
                print(baseURL)
                /*roles_id=2&user_id=241&express_id=53
         voice_message
         img_1
         img_2
         img_3
         img_4
         img_5
         img_6


         */
        
        var imageName = String()
        
        if tagg == 101{
            imageName = "img_1"
            btn1.setImage(image, for: .normal)
        }else if tagg == 102{
            imageName = "img_2"
            btn2.setImage(image, for: .normal)
        }else if tagg == 103{
            imageName = "img_3"
            btn3.setImage(image, for: .normal)
        }else if tagg == 104{
            imageName = "img_4"
            btn4.setImage(image, for: .normal)
        }else if tagg == 105{
            imageName = "img_5"
            btn5.setImage(image, for: .normal)
        }else if tagg == 106{
            imageName = "img_6"
            btn6.setImage(image, for: .normal)
        }
                
                let userId = UserDefaults.standard.getUserId()
                
        let params = ["roles_id":"2","user_id":"\(userId)","express_id":"\(self.expressId)"]
                
                let imgData = self.image.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
    //                DispatchQueue.main.async {
                    multipartFormData.append(imgData!, withName: imageName,fileName: "\(imageName).jpg", mimeType: "image/jpg")
    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                               
                                if swiftyJsonVar["error"] == false {
                                    self.customAlert(title: "", message: "Image uploaded successfully.", icon: "checkmark3")
                                    
                                } else {
                                    
                                    self.customAlert(title: "", message: swiftyJsonVar["message"].stringValue, icon: "checkmark3")
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            }else{
                
                self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
                
                
            }
            }
    
}
