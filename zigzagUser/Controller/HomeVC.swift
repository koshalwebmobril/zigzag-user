//
//  Home1VC.swift
//  zigzagUser
//
//  Created by Webmobril on 02/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import MarqueeLabel
import FSPagerView
import CoreLocation
import CarbonKit
import JHTAlertController



class HomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,FSPagerViewDelegate,FSPagerViewDataSource,CLLocationManagerDelegate ,CarbonTabSwipeNavigationDelegate{
    
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "NearbyRestaurentVC") as! NearbyRestaurentVC
        
        controller.indexValue = index
        return controller
    }
    
    
    
    
    
    var titleArray:[String] = ["Near by Restaurant","By Sales"]
    
    var locManager = CLLocationManager()
    var latitudeVal = Double()
    var longitudeVal = Double()
    var bannerArray = [JSON]()
    var categoryArray = [JSON]()
    var offerArray = [JSON]()
    var restaurantArray = [JSON]()
    var searchArray = [JSON]()
    
    @IBOutlet weak var blankImgView: UIImageView!
    var refreshControl = UIRefreshControl()
    
    var isApiRunning :Bool = false
    var marqueeText = String()
    
    var isSearchEnabled : Bool = false
    
    @IBOutlet weak var searchBar: UITextField!
    
    
    //    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(popupNotification(_ :)), name: NSNotification.Name("Notifiaction"), object: nil)
        
        
        
        //        searchBar.updateHeight(height: 50)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        searchBar.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("Search for shops and items", comment: "Search for shops and items"),attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .blue
        
        self.navigationController?.isNavigationBarHidden = true
        
        searchBar.layer.masksToBounds = true
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = 5.0
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: searchBar.frame.height))
        searchBar.leftView = paddingView
        let searchView = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: searchBar.frame.height))
        searchView.backgroundColor = UIColor.baseOrange
        searchView.layer.masksToBounds = true
        searchView.layer.borderColor = UIColor.white.cgColor
        searchView.layer.borderWidth = 1
        searchView.layer.cornerRadius = 5.0
        searchView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        let button = UIButton(frame:CGRect(x: 10, y: 0, width: 40 , height: 40))
        button.addTarget(self, action: #selector(searchBarSearchButtonClicked), for: .touchUpInside)
        button.setImage(UIImage(named: "search_white"), for: .normal)
        searchView.addSubview(button)
        searchBar.rightViewMode = .always
        searchBar.rightView = searchView
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            
            statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }
            
        }
        locationManagerMethod()
        NotificationCenter.default.addObserver(self, selector: #selector(updateCartCount), name: NSNotification.Name("updateCartCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCartCount), name: NSNotification.Name("deleteCartCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeCartCount), name: NSNotification.Name("removeCartCount"), object: nil)
        
        //        updateCartCount()
        
    }
    
    @objc func refresh() {
        isApiRunning = false
        self.homeServiceMethod()
    }
    
    @objc func deleteCartCount(){
        if let tabItems = tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            UserDefaults.standard.set(0, forKey: "cart_count")
            tabItem.badgeColor = .baseOrange
            tabItem.badgeValue = "\(UserDefaults.standard.value(forKey: "cart_count") as? Int ?? 0)"
        }
    }
    
    @objc func updateCartCount(){
        if let tabItems = tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            tabItem.badgeColor = .baseOrange
            let count = UserDefaults.standard.value(forKey: "cart_count") as? Int ?? 0
            UserDefaults.standard.set(count + 1, forKey: "cart_count")
            tabItem.badgeValue = "\(UserDefaults.standard.value(forKey: "cart_count") as? Int ?? 0)"
        }
    }
    
    @objc func removeCartCount(){
        if let tabItems = tabBarController?.tabBar.items {
            let tabItem = tabItems[2]
            tabItem.badgeColor = .baseOrange
            let count = UserDefaults.standard.value(forKey: "cart_count") as? Int ?? 0
            UserDefaults.standard.set(count - 1, forKey: "cart_count")
            tabItem.badgeValue = "\(UserDefaults.standard.value(forKey: "cart_count") as? Int ?? 0)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isApiRunning = false
        
    }
    
    
    //    @objc func popupNotification(_ sender:Notification){
    //
    //        UserDefaults.standard.removeObject(forKey: "Notification")
    //
    //        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DriverRatingVC") as! DriverRatingVC
    //
    //        popvc.popupDict = (UserDefaults.standard.value(forKey: "data") as? [String:Any]) ?? [:]
    //        print( popvc.popupDict)
    //        popvc.modalPresentationStyle = .overCurrentContext
    //
    //        self.present(popvc, animated: true, completion: nil)
    //
    //    }
    
    fileprivate func locationManagerMethod() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.distanceFilter = 100
        locManager.startUpdatingLocation()
        
        //        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
        //            CLLocationManager.authorizationStatus() == .authorizedAlways){
        //
        //
        //            print("location authorized......")
        //
        //        }else{
        //
        //            let alert = UIAlertController(title: "Allow Location Access", message: "Zig-Zag needs access to your location to show restaurants nearby you. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
        //
        //
        //            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
        //                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
        //                    return
        //                }
        //                if UIApplication.shared.canOpenURL(settingsUrl) {
        //                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
        //                        print("Settings opened: \(success)")
        //                    })
        //                }
        //            }))
        //            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        //            self.present(alert, animated: true, completion: nil)
        //        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
        
        //        let location = locations.last!
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        latitudeVal = locValue.latitude
        longitudeVal = locValue.longitude
        
        UserDefaults.standard.set(latitudeVal, forKey: "lat")
        UserDefaults.standard.set(longitudeVal, forKey: "long")
        
        self.homeServiceMethod()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways,.authorizedWhenInUse:
            
            break
        case .denied,.restricted:
            let alert = UIAlertController(title: "Allow Location Access", message: "Zig-Zag needs access to your location to show restaurants nearby you. Turn on Location Services in your device settings.", preferredStyle: UIAlertController.Style.alert)
            
            
            alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func searchBtn(_ sender: Any) {
    }
    
    
    
    
    @IBAction func callToSearchAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        let contactNumber = restaurantArray[indexPath.row]["mobile"].stringValue
        
        if contactNumber.isEmpty{
            
            self.customAlert(title: "", message: "Phone number not available", icon: "checkmark3")
            
            
            
        }else{
            let tempCall = String(format:"%@",contactNumber)
            if let url = URL(string: "tel://\(tempCall)"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                
            }
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count : Int!
        
        if collectionView.tag == 5000{
            count = categoryArray.count
        }
        else if collectionView.tag == 3000{
            count = offerArray.count
        }
        else if collectionView.tag == 4000{
            count = offerArray.count
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 5000{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "servicesCell", for: indexPath) as! CollectionViewCell
            
            let serviceImgURL = URL(string: "\(GlobalURL.imagebaseURL)\(categoryArray[indexPath.row]["cat_img"].stringValue)")
            
            cell.serviceimg?.sd_setImage(with: serviceImgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)
            
            
            cell.servicename.text = categoryArray[indexPath.row]["name"].stringValue
            
            cell.serviceimg.layer.masksToBounds = false
            cell.serviceimg.layer.shadowColor = UIColor.gray.cgColor
            cell.serviceimg.layer.shadowOpacity = 0.5
            cell.serviceimg.layer.shadowOffset = .zero
            
            
            return cell
        }
        else if collectionView.tag == 3000{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addCell", for: indexPath) as! CollectionViewCell
            
            let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(offerArray[indexPath.row]["img"].stringValue)")
            print(imgURL!)
            
            cell.addimg?.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)
            
            
            cell.addimg?.contentMode = .scaleToFill
            cell.addimg?.clipsToBounds = true
            
            return cell
        }
        else if collectionView.tag == 4000{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "anotherServicesCell", for: indexPath) as! CollectionViewCell
            let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(offerArray[indexPath.row]["img"].stringValue)")
            print(imgURL!)
            
            cell.anotherserviceimg?.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)
            
            
            cell.anotherserviceimg?.contentMode = .scaleToFill
            cell.anotherserviceimg?.clipsToBounds = true
            
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height : CGSize!
        if collectionView.tag == 5000{
            height = CGSize(width: (collectionView.frame.size.width-40)/4, height: collectionView.frame.size.height/2.1)
            
            
            
        }
        else if collectionView.tag == 3000{
            height = CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
        else if collectionView.tag == 4000{
            height = CGSize(width: collectionView.frame.size.width/2 - 10, height: collectionView.frame.size.height)
        }
        
        return height
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 5000{
            
            if indexPath.row == 6{
                if UserDefaults.standard.getUserId() == ""{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier:"RegisterVC") as! RegisterVC
                    vc.hidesBottomBarWhenPushed = true
                    vc.isFromRoot = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: "ExpressVC") as! ExpressVC
                    self.navigationController?.pushViewController(VC, animated: true)
                }
                
            }else{
                
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryProviderVC") as! CategoryProviderVC
                VC.categoryId = self.categoryArray[indexPath.row]["id"].stringValue
                VC.titleName = self.categoryArray[indexPath.row]["name"].stringValue
                VC.tabIndex = indexPath.item
                self.navigationController?.pushViewController(VC, animated: true)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearchEnabled ? searchArray.count : 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return isSearchEnabled ? 120 : 1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var mainCell = UITableViewCell()
        
        if isSearchEnabled{
            
            let searchCell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
            
            searchCell.bgView.layer.shadowColor = UIColor.gray.cgColor
            searchCell.bgView.layer.shadowOpacity = 0.3
            searchCell.bgView.layer.shadowOffset = CGSize(width: 2, height: 2)
            searchCell.bgView.layer.shadowRadius = 6
            searchCell.bgView.layer.cornerRadius = 10
            
            searchCell.logoImgView.layer.masksToBounds = true
            searchCell.logoImgView.layer.cornerRadius = 5.0
            let imgURL:URL = URL(string: "\(GlobalURL.imagebaseURL)\(searchArray[indexPath.row]["user_image"].stringValue)")! 
            
            searchCell.logoImgView.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)
            searchCell.nameLabel.text = searchArray[indexPath.row]["name"].stringValue
            searchCell.ratingView.rating = searchArray[indexPath.row]["avg_rating"].doubleValue
            searchCell.rateLabel.text = searchArray[indexPath.row]["address"].stringValue
            
            searchCell.distanceLabel.layer.masksToBounds = true
            searchCell.distanceLabel.layer.cornerRadius = 5.0
            
            searchCell.distanceLabel.text = "\( searchArray[indexPath.row]["distance"].stringValue)"
            
            
            mainCell = searchCell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
            
            
            cell.marqueeLabel.type = .continuous
            cell.marqueeLabel.speed = .duration(15.0)
            cell.marqueeLabel.animationCurve = .easeInOut
            cell.marqueeLabel.fadeLength = 0.0
            cell.marqueeLabel.leadingBuffer = 10.0
            cell.marqueeLabel.trailingBuffer = 10.0
            cell.marqueeLabel.text = marqueeText
            
            //        controllerArray.removeAll()
            
            let tabSwipe = CarbonTabSwipeNavigation(items: self.titleArray, delegate: self)
            //            tabSwipe.setTabExtraWidth(0)
            tabSwipe.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 0)
            tabSwipe.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 1)
            //            tabSwipe.carbonSegmentedControl?.setWidth(self.view.frame.width/2, forSegmentAt: 2)
            tabSwipe.setIndicatorColor(UIColor.baseOrange)
            tabSwipe.setSelectedColor(UIColor.baseOrange)
            tabSwipe.setNormalColor(UIColor.lightGray)
            tabSwipe.view.frame = cell.pageMenuView.bounds
            //            tabSwipe.pagesScrollView?.bounces = false
            tabSwipe.pagesScrollView?.isScrollEnabled = true
            addChild(tabSwipe)
            cell.pageMenuView.addSubview(tabSwipe.view)
            
            
            cell.pagerView.automaticSlidingInterval = 5.0
            cell.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            cell.pagerView.layer.masksToBounds = true
            cell.pagerView.layer.cornerRadius = 10.0
            cell.pagerView.delegate = self
            cell.pagerView.dataSource = self
            cell.pagerView.isInfinite = true
            cell.pagerView.itemSize = FSPagerView.automaticSize
            cell.pagerView.transformer = FSPagerViewTransformer(type: .zoomOut)
            cell.pageControl.numberOfPages = self.bannerArray.count
            //                       cell.pageControl.setPath(UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 14, height: 14)), for: .normal)
            //                        cell.pageControl.setPath(UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 14, height: 14)), for: .selected)
            //            cell.pageControl.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 16, height: 16), cornerRadius: 8), for: .normal)
            //            cell.pageControl.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 16, height: 16), cornerRadius: 8), for: .selected)
            //            cell.pageControl.setStrokeColor(UIColor.baseOrange, for: .normal)
            cell.pageControl.itemSpacing = 10
            cell.pageControl.backgroundColor = UIColor.clear
            //                        cell.pageControl.setStrokeColor(.baseOrange, for: .normal)
            cell.pageControl.setImage(UIImage(named:"blank_dot2"), for: .normal)
            cell.pageControl.setImage(UIImage(named:"fill_dot"), for: .selected)
            
            
            
            
            cell.addCollectionView.tag = 3000
            cell.addCollectionView.layer.shadowColor = UIColor.gray.cgColor
            cell.addCollectionView.layer.shadowOpacity = 0.3
            cell.addCollectionView.layer.shadowOffset = CGSize(width: 2, height: 2)
            cell.addCollectionView.layer.shadowRadius = 6
            cell.addCollectionView.delegate = self
            cell.addCollectionView.dataSource = self
            
            
            cell.anotherServicesCollectionView.tag = 4000
            cell.anotherServicesCollectionView.layer.shadowColor = UIColor.gray.cgColor
            cell.anotherServicesCollectionView.layer.shadowOpacity = 0.3
            cell.anotherServicesCollectionView.layer.shadowOffset = CGSize(width: 2, height: 2)
            cell.anotherServicesCollectionView.layer.shadowRadius = 6
            cell.anotherServicesCollectionView.delegate = self
            cell.anotherServicesCollectionView.dataSource = self
            //        cell.anotherServicesCollectionView.layer.cornerRadius = 10
            
            cell.servicecollectionview.tag = 5000
            cell.servicecollectionview.layer.shadowColor = UIColor.gray.cgColor
            cell.servicecollectionview.layer.shadowOpacity = 0.3
            cell.servicecollectionview.layer.shadowOffset = CGSize(width: 2, height: 2)
            cell.servicecollectionview.layer.shadowRadius = 6
            cell.servicecollectionview.delegate = self
            cell.servicecollectionview.dataSource = self
            //        cell.servicecollectionview.layer.cornerRadius = 10
            mainCell = cell
        }
        
        return mainCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchEnabled{
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
            VC.restaurantId = self.searchArray[indexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return bannerArray.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if bannerArray.count > 0{
            
            let imgURL = URL(string: "\(GlobalURL.imagebaseURL)\(bannerArray[index]["img"].stringValue)")
            //            print(imgURL!)
            
            cell.imageView?.sd_setImage(with: imgURL, placeholderImage: #imageLiteral(resourceName: "burger"),  completed: nil)
            
            
            cell.imageView?.contentMode = .scaleToFill
            cell.imageView?.clipsToBounds = true
            
        }
        return cell
        
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetailVC") as! StoreDetailVC
        VC.restaurantId = self.bannerArray[index]["provider_id"].stringValue
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! HomeCell
        cell.pageControl.currentPage = targetIndex
        
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        if isSearchEnabled == false{
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! HomeCell
            cell.pageControl.currentPage = pagerView.currentIndex
        }
        
    }
}


extension HomeVC {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            self.isSearchEnabled = false
            searchBar.resignFirstResponder()
            self.tableView.reloadData()
        }else{
            self.isSearchEnabled = true
            self.searchMethod(keyword: searchBar.text!)
        }
        return true
    }
    
    @objc func searchBarSearchButtonClicked() {
        
        searchBar.resignFirstResponder()
        if searchBar.text == "" {
            self.isSearchEnabled = false
            searchBar.resignFirstResponder()
            self.tableView.reloadData()
        }else{
            self.isSearchEnabled = true
            self.searchMethod(keyword: searchBar.text!)
        }
    }
    //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    //        if searchText == "" {
    //            searchBar.resignFirstResponder()
    //            self.view.endEditing(true)
    //            self.searchBar.searchTextField.resignFirstResponder()
    //            self.isSearchEnabled = false
    //            self.tableView.isHidden = false
    //            self.tableView.reloadData()
    //        }else{
    //        }
    //    }
    //    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    //        searchBar.resignFirstResponder()
    //        self.isSearchEnabled = false
    //        self.tableView.isHidden = false
    //        self.tableView.reloadData()
    //    }
    
    
    
    
    
    
    func homeServiceMethod(){
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.homeURL)"
            
            /*user_id
             roles_id,lattitude,longitude*/
            let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["user_id":userId,"roles_id":"2","lattitude":"\(latitudeVal)","longitude":"\(longitudeVal)"]
            print(params)
            if !isApiRunning{
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                isApiRunning = true
                Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                    self.refreshControl.endRefreshing()
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if  response.result.value != nil{
                        let swiftyVarJson = JSON(response.result.value!)
                        print(swiftyVarJson)
                        if swiftyVarJson["error"] == false{
                            self.marqueeText = swiftyVarJson["floating_text"].stringValue
                            self.bannerArray = swiftyVarJson["banners"].arrayValue
                            self.categoryArray = swiftyVarJson["categories"].arrayValue
                            self.offerArray = swiftyVarJson["offers"].arrayValue
                            self.restaurantArray = swiftyVarJson["restaurants"].arrayValue
                            UserDefaults.standard.setValue(swiftyVarJson["cart_count"].intValue, forKey: "cart_count")
                            if let tabItems = self.tabBarController?.tabBar.items {
                                let tabItem = tabItems[2]
                                tabItem.badgeColor = .baseOrange
                                tabItem.badgeValue = "\(UserDefaults.standard.value(forKey: "cart_count") ?? 0)"
                            }
                            UserDefaults.standard.setProviderId(providerId: swiftyVarJson["provider_id"].intValue)
                            UserDefaults.standard.setProviderName(name: swiftyVarJson["provider_name"].stringValue)
                            self.tableView.isHidden = false
                            DispatchQueue.main.async {
                                let indexPath = IndexPath(row: 0, section: 0)
                                let cell = self.tableView.cellForRow(at: indexPath) as! HomeCell
                                cell.addCollectionView.reloadData()
                                cell.anotherServicesCollectionView.reloadData()
                                cell.servicecollectionview.reloadData()
                                cell.pagerView.reloadData()
                                cell.pageControl.numberOfPages = self.bannerArray.count
                                cell.pageControl.currentPage = 0
                                cell.marqueeLabel.text = swiftyVarJson["floating_text"].stringValue
                            }
                        }else{
                            
                            self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        }
                    }else{
                        
                        MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                    }
                    
                }
            }
            
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
    }
    
    
    
    func searchMethod(keyword:String){
        
        
        if Reachability.isConnectedToNetwork(){
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.searchApi)"
            let params :[String:Any] = ["keyword":"\(keyword)","lat":"\(latitudeVal)","long":"\(longitudeVal)"]
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        self.tableView.isHidden = false
                        self.searchArray = swiftyVarJson["data"].arrayValue
                        if self.searchArray.count > 0{
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }else{
                            self.tableView.isHidden = true
                        }
                    }else{
                        self.searchBar.resignFirstResponder()
                        self.isSearchEnabled = false
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    }
                }else{
                    
                    MBProgressHUD.showToast(message: "The request timed out.", name: "Zag-Zag", PPView: self.view)
                }
            }
        }else{
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        }
    }
}


extension UISearchBar {
    func updateHeight(height: CGFloat, radius: CGFloat = 8.0) {
        let image: UIImage? = UIImage.imageWithColor(color: UIColor.clear, size: CGSize(width: 1, height: height))
        setSearchFieldBackgroundImage(image, for: .normal)
        for subview in self.subviews {
            for subSubViews in subview.subviews {
                if #available(iOS 13.0, *) {
                    for child in subSubViews.subviews {
                        if let textField = child as? UISearchTextField {
                            textField.layer.cornerRadius = radius
                            textField.clipsToBounds = true
                        }
                    }
                    continue
                }
                if let textField = subSubViews as? UITextField {
                    textField.layer.cornerRadius = radius
                    textField.clipsToBounds = true
                }
            }
        }
    }
}

private extension UIImage {
    static func imageWithColor(color: UIColor, size: CGSize) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        guard let image: UIImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        return image
    }
}
