//
//  DishDetailVC.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 29/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import JHTAlertController



protocol customizationDelegate {
    func updateCell(isAdded : Bool , amount:Float)
}

class DishDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var menuHeaderArray = [JSON]()
    var menuOptionArray = [JSON]()
    var productDetails = JSON()
    
    @IBOutlet weak var steperView: UIView!
    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var amountlabel: UILabel!
    
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    
    var delegate : customizationDelegate?
    
    @IBOutlet weak var colorView: UIView!
    
    var productId = String()
    var restaurantId = String()
    var providerName = String()
    var finalAmount = String()
    var customizedArray = [String]()
    
    var subMenuArray = [[String:[Int]]]()
    
    var selectedIndex = [IndexPath]()
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.patternColor
        tableView.delegate = self
        tableView.dataSource = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewAction(_ :)))
        
        self.colorView.addGestureRecognizer(tapGesture)
        

        bgView.layer.masksToBounds = true
        bgView.roundCorners(cornerRadius: 10.0)
       
    }
    
    @objc func dismissViewAction(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProductDetails()
    }
    
    func updateBottomView(){
        amountlabel.text = "$\(productDetails["price"].stringValue)"
        
        finalAmount = productDetails["price"].stringValue
        
        let title = "Add $\(productDetails["price"].stringValue)"
        
        self.addToCart.setTitle(title, for: .normal)
    }
    

    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func minusBtnAction(_ sender: Any) {
        
        var count: Int = Int(self.quantityLabel.text ?? "") ?? 0
        
        let fullName = self.amountlabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        let getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        
        let finalAmount = getPrevAmount / Float(count)
        
        
        
        if count > 1{
        count = count - 1
        }
        
        
        self.quantityLabel.text = "\(count)"
        
        
        let title =  "Add $\(finalAmount*Float(count))"
        
        self.amountlabel.text = "$\(finalAmount*Float(count))"
        
        self.addToCart.setTitle(title, for: .normal)
        
       
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func plusBtnAction(_ sender: Any) {
        
        var count: Int = Int(self.quantityLabel.text ?? "") ?? 0
        
        let fullName = self.amountlabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        let getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        
        let baseAmount = getPrevAmount/Float(count)
        
        count = count  + 1
        
        self.quantityLabel.text = "\(count)"
        
        
        
        let finalAmount = baseAmount * Float(count)
        
        let title =  "Add $\(finalAmount)"
        self.amountlabel.text = "$\(finalAmount)"
        
        self.addToCart.setTitle(title, for: .normal)
        
    }
    
    
    @IBAction func addToCartAction(_ sender: Any) {
        

        var strSet = Set<String>()
        var subStrArray = [String]()
        
        for (index,value) in subMenuArray.enumerated(){
            print(value)
            print(index)
            var subStr = String()
            for (_,data) in value.enumerated(){
                
                print(data)
                if data.value.count > 0 {
                 subStr = (data.value.map{String($0)}).joined(separator: "@")
                    
                    strSet.insert(data.key)
                    subStrArray.append(subStr)
            

                    
                }
              
            }


            
        }
        
       let customizationStr = strSet.map({$0}).joined(separator: ",")
        
        let submenStr = subStrArray.map({$0}).joined(separator: ",")
        
       
        addToCartApi(customization: customizationStr, subType: submenStr)

    }
}


extension DishDetailVC{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if menuHeaderArray.count > 0{
            return menuHeaderArray.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
           self.menuOptionArray = menuHeaderArray[section-1]["all_sub_types"].arrayValue
            return self.menuOptionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
        return UITableView.automaticDimension
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if indexPath.section == 0{
            let detailCell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailCell", for: indexPath) as! ProductDetailCell
            detailCell.selectionStyle = .none
            
            detailCell.lblName.text = productDetails["name"].stringValue
            
            let desc = productDetails["description"].stringValue
            detailCell.lblDesc.text = desc.htmlToString
            
            if menuHeaderArray.count > 0 {
                detailCell.lblChooseHieght.constant = 30
            }else{
                detailCell.lblChooseHieght.constant = 0
            }
            
            cell = detailCell
        }else{
            self.menuOptionArray = menuHeaderArray[indexPath.section-1]["all_sub_types"].arrayValue
            let optionCell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailCell2", for: indexPath) as! ProductDetailCell
                        

            optionCell.lblOption.text = menuOptionArray[indexPath.row]["type"].stringValue
            optionCell.priceLabel.text = "$\( menuOptionArray[indexPath.row]["prices"].stringValue)"
            
            cell = optionCell
            
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
        return 50.0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section > 0 {
            return menuHeaderArray[section-1]["name"].stringValue
        }
       return ""
    }
    
    @IBAction func checkButton(_ sender:UIButton){
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)!
        let cell = self.tableView.cellForRow(at: indexPath) as! ProductDetailCell
        sender.isSelected = !sender.isSelected
        
        self.menuOptionArray = menuHeaderArray[indexPath.section-1]["all_sub_types"].arrayValue
        
        
        

        
        let keyData = subMenuArray.flatMap({$0.filter({$0.key == menuHeaderArray[indexPath.section-1]["id"].stringValue})})
        var addData = keyData[0].value
        let getValue = [menuHeaderArray[indexPath.section-1]["id"].stringValue : addData]
        let position = self.subMenuArray.index(of: getValue)
        
        let fullName = self.amountlabel.text
        let fullStr = fullName!.split{$0 == "$"}.map(String.init)
        
        var getPrevAmount: Float = Float(fullStr[0] ) ?? 0.0
        
        
        
        if sender.isSelected{
            cell.checkBtn.setImage(UIImage(named: "checkBox"), for: .normal)
            let addValue = self.menuOptionArray[indexPath.row]["prices"].floatValue
            getPrevAmount = getPrevAmount + (addValue )
            
            selectedIndex.append(indexPath)
            
            addData.append(self.menuOptionArray[indexPath.row]["id"].intValue)
            print(selectedIndex)

            
        }else{
            cell.checkBtn.setImage(UIImage(named: "uncheckBox"), for: .normal)
            let addValue = self.menuOptionArray[indexPath.row]["prices"].floatValue
            getPrevAmount = getPrevAmount - (addValue )
            

            for (index,item) in addData.enumerated() {
                if item == self.menuOptionArray[indexPath.row]["id"].intValue {
                    addData.remove(at: index)
                }
            }


            print(selectedIndex)

        }
        

        subMenuArray[position!] = ([menuHeaderArray[indexPath.section-1]["id"].stringValue : addData])
        
        self.amountlabel.text =  "$\(getPrevAmount)"
        
        finalAmount = "\(getPrevAmount)"
        
        let title =  "Add $\(getPrevAmount)"
        
        self.addToCart.setTitle(title, for: .normal)
        
       
    }
    
    
    
    

    
    
    
    func getProductDetails(){
        
        if Reachability.isConnectedToNetwork(){
        
        let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.dishDetailsURL)"
        
        /*roles_id
        dish_id
        provider_id*/
        
        let params :[String:Any] = ["roles_id":"2","provider_id":"\(self.restaurantId)","dish_id":"\(self.productId)"]
//        let params :[String:Any] = ["roles_id":"2","provider_id":"62","dish_id":"4"]
        print(params)
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if  response.result.value != nil{
                
                let swiftyVarJson = JSON(response.result.value!)
                print(swiftyVarJson)
                if swiftyVarJson["error"] == false{
                    //dish
                    
                    self.productDetails = swiftyVarJson["dish"]
                    self.menuHeaderArray = swiftyVarJson["customization"].arrayValue

                    
                    for value in self.menuHeaderArray{
//                        print("This is val \(value)")
                        let newValue: [String: [Int]] = [value["id"].stringValue : []]
                        self.subMenuArray.append(newValue)
                    }
//                      print("This is finally \(self.subMenuArray)")
                    self.tableView.reloadData()
                   self.updateBottomView()
                    
                    
                }else{
                    
                    self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                    
                }
                
                
                
                
            }else{
               MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
            }
            
        }
    }else{
        
        self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
        
        
    }
    }
    
    
    func addToCartApi(customization:String,subType:String)  {
        
        if Reachability.isConnectedToNetwork(){
            
            let baseURL = "\(GlobalURL.baseURL)\(GlobalURL.addToCart)"
            
            /*roles_id
            user_id
            dish_id
            quantity
            amount
            provider_id customisation=1,2,3&sub_types=2@3,4@6@7,9@12    */
        
        var strCustomization = String()
        
        if customization.count > 0{
            strCustomization = "1"
        }else{
            strCustomization = "0"
        }
        
        let userId = UserDefaults.standard.getUserId()
            
            let params :[String:Any] = ["roles_id":"2","user_id":"\(userId)","amount":"\(finalAmount)","quantity":"1","provider_id":"\(self.restaurantId)","dish_id":"\(self.productId)","is_customizable":"\(strCustomization)","sub_types":"\(subType)","customisations":"\(customization)","discount_price":"10"]
   
            print(params)
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseURL, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                if  response.result.value != nil{
                    
                    let swiftyVarJson = JSON(response.result.value!)
                    print(swiftyVarJson)
                    if swiftyVarJson["error"] == false{
                        NotificationCenter.default.post(name: NSNotification.Name("updateCartCount"), object: nil)
                        self.delegate?.updateCell(isAdded: true, amount: swiftyVarJson["data"]["amount"].floatValue)
                        
                        UserDefaults.standard.setProviderId(providerId: Int(self.restaurantId)!)
                        
                        UserDefaults.standard.setProviderName(name: self.providerName)
                        
                        let alert = self.createAlert(title: "", message: "Item added to your cart.", icon: "cart-1")
                        
                        let okAction = JHTAlertAction(title: "Ok", style: .default) { _ in
                            
                            NotificationCenter.default.post(name: NSNotification.Name("reloadPage"), object: nil)
                           
                            self.dismiss(animated: true, completion: nil)
                        }

                        
                        alert.addAction(okAction)

                        self.present(alert, animated: true, completion: nil)
                        

                        
                    }else{
                        self.delegate?.updateCell(isAdded: false, amount: 0.0)
                        
                        self.customAlert(title: "", message: swiftyVarJson["message"].stringValue, icon: "checkmark3")
                        
                    }
                    
                    
                    
                    
                }else{
                  MBProgressHUD.showToast(message: "The network connection was lost.", name: "Zag-Zag", PPView: self.view)
                    
                }
                
            }
        }else{
            
            self.customAlert(title: "", message: "No internet connection.", icon: "checkmark3")
            
            
        }
        }
    
    
}
