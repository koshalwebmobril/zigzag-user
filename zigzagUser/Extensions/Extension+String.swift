//
//  Extension+String.swift
//  Plabook
//
//  Created by Mac on 11/15/19.
//  Copyright © 2019 mobappssolutions.com. All rights reserved.
//

import Foundation
import JHTAlertController

let REGEX_USER_NAME_LIMIT="^.{3,15}$"
let REGEX_USER_NAME="[A-Za-z0-9]{1,50}"
let REGEX_USER_EMAIL="[A-Z0-9a-z._%+-]{2,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"
let REGEX_USER_PASSWORD_LIMIT="^.{6,20}$"
let REGEX_USER_PASSWORD="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,}$"
let REGEX_USER_PHONE_PATTERN="[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
let REGEX_USER_PHONE="^[0-9]{7,15}$"
let REGEX_ONLY_NUMBER="[0-9]{1,10}"
let REGEX_RANGE_PATTERN="[0-9]{1,3}\\-[0-9]{1,3}"
let REGEX_ZIPCODE="^[0-9]{3,6}$"

extension String {
    
    func isValidEmail() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_EMAIL,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidPassword() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_PASSWORD,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidPhoneNumber() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_PHONE,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    func isValidTextField() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_NAME,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidZipcode() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_ZIPCODE,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    
    
    func localizableString(loc:String) -> String{
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}


extension Date {
    static func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
}

extension UIViewController{
    func createAlert(title:String,message:String, icon:String)->JHTAlertController{
        
        let alertController = JHTAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.titleViewBackgroundColor = UIColor.clear
        alertController.titleTextColor = .green
        alertController.titleImage = UIImage(named: icon)
         alertController.alertBackgroundColor = .secondarySystemBackground
        alertController.messageTextColor = .black
        alertController.dividerColor = .lightGray
        alertController.setButtonTextColorFor(.default, to: .baseOrange)
        alertController.setButtonTextColorFor(.cancel, to: .baseOrange)
        alertController.setAllButtonBackgroundColors(to: .secondarySystemBackground)
        return alertController
    }
    
    
    func customAlert(title:String,message:String, icon:String){
        
        let alertController = JHTAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.titleViewBackgroundColor = UIColor.clear
        alertController.titleTextColor = .green
        alertController.titleImage = UIImage(named: icon)
         alertController.alertBackgroundColor = .secondarySystemBackground
        alertController.messageTextColor = .black
        alertController.dividerColor = .lightGray
        alertController.setButtonTextColorFor(.default, to: .baseOrange)
        alertController.setAllButtonBackgroundColors(to: .secondarySystemBackground)
        alertController.addAction(JHTAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
}

