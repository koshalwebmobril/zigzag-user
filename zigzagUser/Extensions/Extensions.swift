//
//  Extensions.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 24/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    public class var baseOrange: UIColor {
        return UIColor(red: 254/255, green: 89/255, blue: 0/255, alpha: 1.0)
    }
    public class var baseBackground: UIColor {
        return UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1.0)
    }
    public class var patternColor: UIColor {
        return UIColor(red: 251/255, green: 246/255, blue: 243/255, alpha: 1.0)
    }
}



extension UserDefaults{
    
    func setPhone(token:String)  {
        
        self.set(token, forKey: "phone")
        
    }
    
    func getPhone() -> String {
        return self.value(forKey: "phone") as? String ?? "92929229292"
    }
    
    func setDeviceToken(token:String)  {
        
        self.set(token, forKey: "device_token")
        
    }
    
    func getDeviceToken() -> String {
        return self.value(forKey: "device_token") as? String ?? "12345"
    }
    
    func setUserId(userId:String)  {
        
        self.set(userId, forKey: "user_id")
        
    }
    
    func getUserId() -> String {
        
        
        return self.value(forKey: "user_id") as? String ?? ""
    }
    
    func setLoginFlag(loginFlag:Int)  {
        
        self.set(loginFlag, forKey: "login_flag")
        
    }
    
    func getLoginFlag() -> Int {
        return self.value(forKey: "login_flag") as? Int ?? 0
    }
    
    func setProviderId(providerId:Int)  {
          
          self.set(providerId, forKey: "providerId")
          
      }
      
      func getProviderId() -> Int {
          return self.value(forKey: "providerId") as? Int ?? 0
      }
    
    
    func setProviderName(name:String)  {
        
        self.set(name, forKey: "provider_name")
    }
    
    func getProviderName() -> String {
        
        
        return self.value(forKey: "provider_name") as? String ?? ""
    }
    
    func setFCMToken(fcm_token:String)  {
        
        self.set(fcm_token, forKey: "fcm_token")
        
    }
    
    func getFCMToken() -> String {
        
        
        return self.value(forKey: "fcm_token") as? String ?? ""
    }
    
    func removeUserdefaultData()  {
        
        self.removeObject(forKey: "user_id")
        self.removeObject(forKey: "login_flag")
        
    }
    
    func setLanguage(lang:String)  {
           
           self.set(lang, forKey: "lang")
           
       }
       
       func getLanguage() -> String {
           
           
           return self.value(forKey: "lang") as? String ?? ""
       }
    
    
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}



extension UIView {
  
    func roundCorners(cornerRadius: Double) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
   
}
