//
//  AppDelegate.swift
//  zigzagUser
//
//  Created by Webmobril on 26/11/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import GooglePlaces
import GoogleMaps
import Braintree
import IQKeyboardManager
import Firebase
import Messages
import FirebaseMessaging
import FirebaseCore


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        BTAppSwitch.setReturnURLScheme("com.demo.zigzagUser.payments")
        
        registerForPushNotifications()
//        AIzaSyDJl11mAojAFGlCjDrzrZx1LhXozqtA78Y
        GMSPlacesClient.provideAPIKey("AIzaSyDJl11mAojAFGlCjDrzrZx1LhXozqtA78Y")
        GMSServices.provideAPIKey("AIzaSyDJl11mAojAFGlCjDrzrZx1LhXozqtA78Y")
        
    
//AIzaSyDL87Sxo7OD0L-BmC198x-wJpmbYCalvyw
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentSandbox:"AbyL4cK0CWozaJmgrEQ6_6R-bUt8OKU809tzj3__XR0Z8p2o3au7XWwbmLwyZBSwDTLYEegC85HxYJZA"])
        
//        Thread.sleep(forTimeInterval: 2)
        
        if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        
        statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
        UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            
            if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
                // my stuff
                statusBar.backgroundColor = #colorLiteral(red: 0.9605992436, green: 0.2029439211, blue: 0.6319147348, alpha: 1)
            }

        }
       
//         if UserDefaults.standard.getUserId() == "" {
//            gotoLoginVC()
//        }else{
             gotoHomeVC()
//        }
        return true
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
       
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
       if url.scheme?.localizedCaseInsensitiveCompare("com.demo.zigzagUser.payments") == .orderedSame{
            return BTAppSwitch.handleOpen(url,options: options)
        }
        
        return false
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        UserDefaults.standard.setValue("FromNotification", forKey: "Notification")
        
        if let jsonString = userInfo[AnyHashable("gcm.notification.data")] as? String,

            let data = jsonString.data(using: .utf8) {

            do {
                if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String : Any] {
                    
                    var finalDict = [String:Any]()
                    if json["result"] as? [String:Any] != nil{
                        finalDict = json["result"] as! [String : Any]
                        
                        UserDefaults.standard.setValue(finalDict["data"], forKey: "data")
                        
                    }
                }
                
            }catch{

            }
        }
        

//        NotificationCenter.default.post(name: NSNotification.Name("Notifiaction"), object: nil, userInfo: userInfo)

        
        
    }
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "zigzagUser")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

   

}


extension AppDelegate{
    

    
    
    
    func gotoLoginVC(){
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        let navigationController = UINavigationController(rootViewController: initialViewController)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    func gotoHomeVC(){
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        initialViewController.selectedIndex = 0
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.isNavigationBarHidden = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
   
   
        
}

extension AppDelegate{
    
    
       func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            
            let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
            let temptoken = tokenParts.joined()
    
            UserDefaults.standard.setDeviceToken(token: temptoken)
            UserDefaults.standard.synchronize()
            
        }
        
        
        func getNotificationSettings() {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
   
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        
        
        func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
            print("FCM token: \(fcmToken)")
            
            let fToken = fcmToken
            UserDefaults.standard.setFCMToken(fcm_token: fToken)
            UserDefaults.standard.synchronize()
        }
        
        
        
        func registerForPushNotifications() {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
    //            print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        }
    
        
}
