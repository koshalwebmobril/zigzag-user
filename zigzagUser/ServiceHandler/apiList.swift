//
//  apiList.swift
//  Ziphawk_Child
//
//  Created by Sushant on 17/07/19.
//  Copyright © 2019 WebMobril. All rights reserved.
//

import Foundation

struct GlobalURL {
    //https://webmobril.org/dev/zig-zag/

    static let baseURL = "http://52.54.1.108/zigzag/api/"
   
    static let imagebaseURL = "http://52.54.1.108/zigzag/"
    
    static let loginURL = "login"
    static let verifyOtpURL = "verify_otp"
    static let homeURL = "home_page"
    static let updateProfileURL = "update_profile"
    static let logoutURL = "logout"
    static let ratingRestaurantURL = "rate_restaurant"
    static let getRestaurantListURL = "view_restaurants"
    static let getRestaurantDetailsURL = "restaurant_detail"
    static let makeFavoriteURL = "make_fav_restaurant"
    static let rateDishURL = "rate_dish"
    static let dishDetailsURL = "dish_detail"
    static let restaurantTabURL = "restaurant_profile_tabs"
    static let contactAdminURL = "contact_admin"
    static let getMenuDishesURL = "get_menu_dishes"
    static let addAddressURL = "add_address"
    static let getAllAddressURL = "get_all_address"
    static let editAddressURL = "edit_address"
    static let makeDefualtAddressURL = "make_default_address"
    static let deleteAddressURL = "delete_address"
    static let getCartItemList = "get_cart_items"
    static let addToCart = "add_to_cart"
    static let updateCart = "updateCart"
    static let savedCardList = "saved_card_list"
    static let saveNewCard = "add_card"
    static let removeCard = "delete_card"
    static let editCard = "edit_card"
    static let getFavoriteList = "get_favourite_providers"
    static let getProfileData = "get_profile"
    static let getCustomizedList = "get_user_cart_customise_items"
    static let removeFavorite = "removeFavourite_Restaurant"
    static let paymentApi = "payment"
    static let orderHistory = "order_history"
    static let searchApi = "search"
    static let cancelOrder = "cancel_order"
    static let trackOrder = "track_order"
    static let categoryProvider = "get_category_based_providers"
    static let deletePreviousCart = "delete_previous_cart_items"
    static let rateDriver = "rate_driver"
    static let emptyCart = "delete_all_cart_items"
    static let getAllStores = "allstores"
    
    static let uploadProductURL = "upload_express_doc"
    static let expressURL = "express_service"
    static let expressPaymentURL = "make_express_payment"
    static let calculatePriceURL = "calculate_price"
    static let expressServiceHistoryURL = "order_history_express"

    
}
