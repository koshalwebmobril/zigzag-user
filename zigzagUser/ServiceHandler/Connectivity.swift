//
//  Connectivity.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 19/06/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import Foundation

import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
