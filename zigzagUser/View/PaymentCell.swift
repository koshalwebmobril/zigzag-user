//
//  PaymentCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 13/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var payNowBtn: UIButton!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var cvvView: UIView!
    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardlabel: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
    
    
    @IBOutlet weak var txtNewCardCvv: UITextField!
    @IBOutlet weak var txtCard: UITextField!
    @IBOutlet weak var addCardBgView: UIView!
    
    @IBOutlet weak var txtExpDate: UITextField!
    @IBOutlet weak var newCardPayBtn: UIButton!
    
    @IBOutlet weak var saveCardBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
