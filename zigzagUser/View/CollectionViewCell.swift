//
//  CollectionViewCell.swift
//  zigzagUser
//
//  Created by Webmobril on 02/12/19.
//  Copyright © 2019 Webmobril. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    //image collection view cell
    
    @IBOutlet weak var bannersImg: UIImageView!
    
    //services cell
    
    @IBOutlet weak var serviceimg: UIImageView!
    
    @IBOutlet weak var servicename: UILabel!
    
    //Add cell
    
    @IBOutlet weak var addimg: UIImageView!
    
    //Another services cell
    
    @IBOutlet weak var anotherserviceimg: UIImageView!
}
