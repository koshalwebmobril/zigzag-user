//
//  CourierCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 06/08/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class CourierCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var trackId: UILabel!
    
    @IBOutlet weak var billingAddress: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DestinationAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
