//
//  HomeCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 10/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import MarqueeLabel
import FSPagerView

class HomeCell: UITableViewCell {
    
    @IBOutlet weak var pageControl: FSPageControl!{
        didSet {
           
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.setFillColor(UIColor.white, for: .normal)
            self.pageControl.setFillColor(UIColor.red, for: .selected)
            
        }
    }
    
    @IBOutlet weak var servicecollectionview: UICollectionView!
    @IBOutlet weak var addCollectionView: UICollectionView!
    @IBOutlet weak var anotherServicesCollectionView: UICollectionView!
   
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var marqueeLabel: MarqueeLabel!
    @IBOutlet weak var secMainView: UIView!
    
  
    @IBOutlet weak var pageMenuView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
