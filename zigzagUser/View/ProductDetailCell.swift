//
//  ProductDetailCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 02/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class ProductDetailCell: UITableViewCell {
    
    
    @IBOutlet weak var productImg: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblChooseHieght: NSLayoutConstraint!
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
