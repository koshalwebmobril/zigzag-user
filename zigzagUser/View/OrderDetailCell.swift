//
//  OrderDetailCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 20/04/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {
    
    
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var providerLabel: UILabel!
    
    @IBOutlet weak var customization_label: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var discount_label: UILabel!
    
    @IBOutlet weak var radioMenuPrice: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var trackBtn: UIButton!
    @IBOutlet weak var deliveryCharge: UILabel!
    
    @IBOutlet weak var checkPriceLabel: UILabel!
    @IBOutlet weak var uncheckBtn: UIButton!
    @IBOutlet weak var checkBtn: UIButton!
    
    @IBOutlet weak var checkMenuLabel: UILabel!
    
    @IBOutlet weak var productImView: UIImageView!
    
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var productTotal: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var itemCountLabel: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
