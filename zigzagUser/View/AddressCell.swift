//
//  AddressCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 05/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblAddressNumber: UILabel!
    @IBOutlet weak var pointBtn: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var defaultBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var lbladdress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
