//
//  MenuListCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 07/02/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import FloatRatingView

class MenuListCell: UITableViewCell {
    
    @IBOutlet weak var discount_label: UILabel!
    @IBOutlet weak var dishFlameImgView: UIImageView!
    
    @IBOutlet weak var dishFlameHeight: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var storeImgView: UIImageView!
    
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var steperView: UIView!
    
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var menuBackView: UIView!
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var menuLogoImg: UIImageView!
    @IBOutlet weak var flameImgView: UIImageView!
    @IBOutlet weak var flameWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
