//
//  PopupCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 16/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class PopupCell: UITableViewCell {
    
    
    @IBOutlet weak var steperView: UIView!
    @IBOutlet weak var lblDish: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
