//
//  SearchCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 24/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import FloatRatingView

class SearchCell: UITableViewCell {
    
    
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
