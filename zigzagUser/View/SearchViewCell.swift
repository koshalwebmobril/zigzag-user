//
//  SearchViewCell.swift
//  EndCashBusiness
//
//  Created by Vijay Kunal on 28/11/18.
//  Copyright © 2018 Priyanka Jaiswal. All rights reserved.
//

import UIKit

class SearchViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var lblSearch: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
