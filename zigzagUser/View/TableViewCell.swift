//
//  TableViewCell.swift
//  zigzagUser
//
//  Created by Webmobril on 29/01/20.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit
import FloatRatingView

class TableViewCell: UITableViewCell {

    //nearbycell
    
    @IBOutlet weak var nearbyview: UIView!
    @IBOutlet weak var nearbyimg: UIImageView!
    @IBOutlet weak var nearbyname: UILabel!
    @IBOutlet weak var nearbyprice: UILabel!
    
    @IBOutlet weak var address_label: UILabel!
    @IBOutlet weak var desc_label: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var nearbydistance: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
