//
//  CartCell.swift
//  zigzagUser
//
//  Created by Sandeep Kumar on 03/03/2020.
//  Copyright © 2020 Webmobril. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var minusBtn: UIButton!
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var txtClientNote: CustomTextField!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    
    @IBOutlet weak var discount_label: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var steperView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
